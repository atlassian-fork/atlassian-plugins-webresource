define(["wr!com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path"], function(wrContextPath) {
    return wrContextPath.WRM.contextPath;
});
