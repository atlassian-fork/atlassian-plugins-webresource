// soy! loader, needed to require LESS from AMD module, returns the null as the result.
define("soy", {
    load: function (name, req, onload, config) {
        onload(req(name + ".soy"));
    }
});