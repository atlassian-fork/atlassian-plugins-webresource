module('wrm/i18n');

asyncTest("should be same as global", function() {
    require(["wrm/i18n"], function(I18n){
        equal(I18n, window.WRM.I18n);
        QUnit.start()
    });
});

asyncTest("the format function should match the 'wrm/format' module", function() {
    require(["wrm/format", "wrm/i18n"], function(format, I18n){
        equal(I18n.format, format);
        QUnit.start()
    });
});

asyncTest("should expose the I18n object with a getText function", function() {
    require(["wrm/i18n"], function(I18n){
        equal(I18n.getText('foo'), 'foo');
        QUnit.start()
    });
});

asyncTest("should warn about using getText directly", function() {
    require(["wrm/i18n"], function(I18n){
        var consoleWarnSpy = sinon.spy(console, 'warn');
        I18n.getText('foo');

        sinon.assert.calledWith(
            consoleWarnSpy,
            'Call to "getText" function was not replaced with either raw translation or a call to the "format" function. Have you included the "jsI18n" transformation in your webresource configuration?'
        );

        QUnit.start()
    });
});


asyncTest("should forward the formatting options to 'format' function", function() {
    require(["wrm/i18n"], function(I18n){
        sinon.spy(I18n, "format");

        I18n.getText('foo', 'bar', 'moo', 123);
        sinon.assert.calledWith(I18n.format, 'foo', 'bar', 'moo', 123);

        QUnit.start()
    });
});
