module('wrm/context-path');

asyncTest("should be same as global", function() {
    require(["wrm/context-path"], function(contextPath){
        equal(contextPath, window.WRM.contextPath);
        QUnit.start()
    });
});
