// Assembling AMD modules for tests.
({
    optimize: "none",
    baseUrl: "../../../main/resources/js/modules",
    include: [
        "wrm/data",
        "wrm/context-path",
        "wrm/format",
        "wrm/i18n"
    ],
    paths: {
        wr: "../web-resources/wr-loader",
        css: "../web-resources/css-loader",
        less: "../web-resources/less-loader",
        soy: "../web-resources/soy-loader"
    },
    out: "../../../../target/modules-test-build.js"
})
