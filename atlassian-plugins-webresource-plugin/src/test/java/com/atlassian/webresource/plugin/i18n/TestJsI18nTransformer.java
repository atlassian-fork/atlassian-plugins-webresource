package com.atlassian.webresource.plugin.i18n;

import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.plugin.webresource.util.HashBuilder.buildHash;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJsI18nTransformer {
    JsI18nTransformer transformerFactory;
    WebResourceIntegration webResourceIntegration;

    private final Map<String, Optional<String>> stubbedKeys = new HashMap<>();
    private static final Locale MOCK_LOCALE_OBJECT = new Locale("en", "gb");
    private static final String MOCK_LOCALE = LocaleUtils.serialize(MOCK_LOCALE_OBJECT);
    private static final List<Locale> LOCALES = asList(
            MOCK_LOCALE_OBJECT, new Locale("en", "us"), new Locale("fr", "FR"),
            new Locale("de", "DE"), new Locale("ja", "JP"), new Locale("es", "ES")
    );

    @Before
    public void setUp() throws Exception {
        webResourceIntegration = mock(WebResourceIntegration.class);
        stubI18nResolution();
        transformerFactory = new JsI18nTransformer(webResourceIntegration);
    }

    @Test
    public void testLocale() {
        String hashValue = "doge_s0-H4sh-WoWW";
        when(webResourceIntegration.getLocale()).thenReturn(MOCK_LOCALE_OBJECT);
        when(webResourceIntegration.getI18nStateHash()).thenReturn(hashValue);
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();

        transformerFactory.makeUrlBuilder(null).addToUrl(urlBuilder);

        assertEquals(MOCK_LOCALE, urlBuilder.buildParams().get("locale"));
        assertEquals(buildHash(hashValue), urlBuilder.buildHash());
    }

    @Test
    public void testSimple() {
        String javascript = "var label = WRM.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var label = \"bar\";", transform(javascript));
    }

    @Test
    public void testSimpleLocaleCaseInsensitive() {
        String javascript = "var label = WRM.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var label = \"bar\";", transform(javascript, QueryParams.of(ImmutableMap.of("locale", "en-gb"))));
        assertEquals("var label = \"bar\";", transform(javascript, QueryParams.of(ImmutableMap.of("locale", "en-GB"))));
    }

    @Test
    public void testMultipleLines() {
        String key = "foo.bar";
        String key2 = "awesome.label";
        String function = "var someFunction = function() { return 0; };";

        String javascript = "var label = WRM.I18n.getText(\"" + key + "\");\n" +
                "var anotherLabel = WRM.I18n.getText(\"" + key2 + "\");\n" + function;

        stubTranslation(key, "Foo Bar");
        stubTranslation(key2, "Awesome");
        assertEquals("var label = \"Foo Bar\";\nvar anotherLabel = \"Awesome\";\n" + function, transform(javascript));
    }

    @Test
    public void testMissingKey() {
        String javascript = "var t = WRM.I18n.getText(\"blah\");";

        assertEquals("var t = \"blah\";", transform(javascript));
    }

    @Test
    public void testMissingKeyNull() {
        String javascript = "var t = WRM.I18n.getText(\"blah\");";
        stubNullTranslation("blah");

        assertEquals("var t = \"blah\";", transform(javascript));
    }

    @Test
    public void testMissingRawKeyNull() {
        String javascript = "var t = WRM.I18n.getText(\"blah\", yada);";
        stubNullTranslation("blah");

        assertEquals("var t = WRM.format(\"blah\", yada);", transform(javascript));
    }

    @Test
    public void testKeyWithSingleQuotes() {
        String javascript = "var t = WRM.I18n.getText('blah');";
        stubTranslation("blah", "Blah");

        assertEquals("var t = \"Blah\";", transform(javascript));
    }

    @Test
    public void testKeyWithoutDots() {
        String javascript = "var t = AJS.I18n.getText(\"blah\");";
        stubTranslation("blah", "Blah");

        assertEquals("var t = \"Blah\";", transform(javascript));
    }

    @Test
    public void testKeyWithHyphens() {
        String key = "foo-bar";
        String javascript = "var str = AJS.I18n.getText(\"" + key + "\");";
        stubTranslation(key, "Foo Bar");
        assertEquals("var str = \"Foo Bar\";", transform(javascript));
    }

    @Test
    public void testValueGetsEscaped() {
        String key = "apos.key";
        String javascript = "var str = AJS.I18n.getText(\"" + key + "\");";
        stubTranslation(key, "That''s Awesome! \"Woot!\"");
        assertEquals("var str = \"That\\'s Awesome! \\\"Woot!\\\"\";", transform(javascript));
    }

    @Test
    public void testNonMatchingString() {
        String javascript = "var s = 0; var t = AJS.I18n;, var u = AJSI18ngetText(\"foo\")";
        assertEquals(javascript, transform(javascript));

        // mismatched quotes
        javascript = "var str = AJS.I18n.getText('apos.key\");";
        assertEquals(javascript, transform(javascript));
        javascript = "var str = AJS.I18n.getText(\"apos.key');";
        assertEquals(javascript, transform(javascript));
    }

    @Test
    public void testWhitespaceBetweenArgs() {
        stubTranslation("blah", "Blah");

        String javascript = "var t = AJS.I18n.getText( 'blah');";
        assertEquals("var t = \"Blah\";", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah' );";
        assertEquals("var t = \"Blah\";", transform(javascript));

        javascript = "var t = AJS.I18n.getText( 'blah' );";
        assertEquals("var t = \"Blah\";", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah',1,2);";
        assertEquals("var t = AJS.format(\"Blah\",1,2);", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah', 1,2);";
        assertEquals("var t = AJS.format(\"Blah\", 1,2);", transform(javascript));

        javascript = "var t = AJS.I18n.getText('blah' , 1,2);";
        assertEquals("var t = AJS.format(\"Blah\", 1,2);", transform(javascript));
    }

    @Test
    public void testMessageWithQuotes() {
        String key = "key.with.quotes";
        String translation = "Could not find file ''{0}''.";
        String jsEscapedTranslation = "Could not find file \\'\\'{0}\\'\\'.";
        String javascript = "var t = WRM.I18n.getText(\"" + key + "\", results, total);";
        stubTranslation(key, translation);

        assertEquals("var t = WRM.format(\"" + jsEscapedTranslation + "\", results, total);", transform(javascript));
    }

    @Test
    public void testFormattedMessageWithArgs() {
        String key = "key.with.format";
        String translation = "Found {0} out of {1}";
        String javascript = "var t = WRM.I18n.getText(\"" + key + "\", results, total);";
        stubTranslation(key, translation);

        assertEquals("var t = WRM.format(\"" + translation + "\", results, total);", transform(javascript));
    }

    @Test
    public void testFormattedMessageNoArgs() {
        String key = "key.with.format";
        String translation = "open-curly-zero '{0}' open-curley-one '{1}'";
        String javascript = "var t = WRM.I18n.getText(\"" + key + "\");";
        stubTranslation(key, translation);

        assertEquals("var t = \"open-curly-zero {0} open-curley-one {1}\";", transform(javascript));
    }

    @Test
    public void testFormattedWithoutLocaleInQueryKey() {
        when(webResourceIntegration.getLocale()).thenReturn(MOCK_LOCALE_OBJECT);
        String javascript = "var t = WRM.I18n.getText('drink');";
        stubTranslation("en-GB", "drink", "tea");
        stubTranslation("en-US", "drink", "cawfee");

        assertEquals("var t = \"tea\";", transform(javascript, QueryParams.of(ImmutableMap.of("locale", "en-GB"))));
        assertEquals("var t = \"cawfee\";", transform(javascript, QueryParams.of(ImmutableMap.of("locale", "en-US"))));
        assertEquals("var t = \"cawfee\";", transform(javascript, QueryParams.of(emptyMap())));
    }

    @Test
    public void testLegacyAjsNamespaceNoArgs() {
        String javascript = "var t = AJS.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testLegacyAjsNamespaceWithArgs() {
        String key = "key.with.format";
        String translation = "Found {0} out of {1}";
        String javascript = "var t = AJS.I18n.getText(\"" + key + "\", results, total);";
        stubTranslation(key, translation);

        assertEquals("var t = AJS.format(\"" + translation + "\", results, total);", transform(javascript));
    }

    @Test
    public void testRenamedAjsNamespace() {
        String javascript = "var t = a.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testRenamedAjsNamespaceWithFormat() {
        String javascript = "var t = HiMum.I18n.getText(\"foo\", results, total);";

        stubTranslation("foo", "bar");
        assertEquals("var t = HiMum.format(\"bar\", results, total);", transform(javascript));
    }

    @Test
    public void testBabeledAjsNamespace() {
        String javascript = "var t = _AJS[\"default\"].I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testBabeledAjsNamespaceWithFormat() {
        String javascript = "var t = _AJS[\"default\"].I18n.getText(\"foo\", results, total);";

        stubTranslation("foo", "bar");
        assertEquals("var t = _AJS[\"default\"].format(\"bar\", results, total);", transform(javascript));
    }

    @Test
    public void testTranspiledOutputFromBabel7WithWebpack4() {
        String javascript = "var t = ANY_MODULE['I18n'].getText('foo');";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"bar\";", transform(javascript));
    }

    @Test
    public void testTranspiledOutputFromBabel7WithWebpack4WithFormat() {
        String javascript = "var t = ANY_MODULE[\"I18n\"].getText(\"foo\", results, total);";

        stubTranslation("foo", "bar");
        assertEquals("var t = ANY_MODULE.format(\"bar\", results, total);", transform(javascript));
    }

    @Test
    public void testNoPrecedingWhitespaceAjsNamespace() {
        String javascript = "var t = \"str\"+AJS.I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals("var t = \"str\"+\"bar\";", transform(javascript));
    }

    @Test
    public void testWrappedAjsNamespace() {
        String javascript = "var t = [].join(AJS.I18n.getText(\"foo\"));";

        stubTranslation("foo", "bar");
        assertEquals("var t = [].join(\"bar\");", transform(javascript));
    }

    @Test
    public void testUsedInArrayReference() {
        String javascript = "var t = mymap[AJS.I18n.getText(\"foo\")];";

        stubTranslation("foo", "bar");
        assertEquals("var t = mymap[\"bar\"];", transform(javascript));
    }

    @Test
    public void testOtherNamespace1() {
        String javascript = "var label = I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals(javascript, transform(javascript));
    }

    @Test
    public void testOtherNamespace2() {
        String javascript = "var label = .I18n.getText(\"foo\");";

        stubTranslation("foo", "bar");
        assertEquals(javascript, transform(javascript));
    }

    @Test
    public void testPrebakeDimensions() {
        Dimensions result = transformerFactory.computeDimensions();
        Dimensions expected = Dimensions.empty()
                .andExactly("locale", "en-GB", "en-US", "fr-FR", "de-DE", "ja-JP", "es-ES");

        assertEquals(expected, result);
    }


    // See PLUGWEB-397 for details.
    @Test
    public void testIsNotSucceptableToStackOverflow() throws IOException {
        String js = "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC" +
                "AgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/iDFhJQ0NfUF" +
                "JPRklMRQABAQAADEhMaW5vAhAAAG1u;";
        stubTranslation("a", "b");
        transform(js, QueryParams.of(ImmutableMap.of("locale", "en-gb")));
    }

    @Test
    public void testAddToUrlWithCoordinates() {
        String hashValue = "doge_s0-H4sh-WoWW";
        when(webResourceIntegration.getI18nStateHash()).thenReturn(hashValue);
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();

        Coordinate coord = Dimensions.empty().andExactly("locale", "en-GB").cartesianProduct().findFirst().get();
        transformerFactory.makeUrlBuilder(null).addToUrl(urlBuilder, coord);

        assertEquals(MOCK_LOCALE, urlBuilder.buildParams().get("locale"));
        assertEquals(buildHash(hashValue), urlBuilder.buildHash());
    }

    private void stubTranslation(final String key, final String raw) {
        stubTranslation(MOCK_LOCALE, key, raw);
    }

    private void stubTranslation(final String locale, final String key, final String raw) {
        stubbedKeys.put(locale + "-" + key, Optional.of(raw));
    }

    private void stubNullTranslation(final String key) {
        stubbedKeys.put(MOCK_LOCALE + "-" + key, Optional.empty());
    }

    private void stubI18nResolution() {
        when(webResourceIntegration.getI18nRawText(any(Locale.class), anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                Locale locale = (Locale) invocation.getArguments()[0];
                String key = (String) invocation.getArguments()[1];
                String fullKey = LocaleUtils.serialize(locale) + "-" + key;
                if (stubbedKeys.containsKey(fullKey)) {
                    Optional<String> value = stubbedKeys.get(fullKey);
                    return value.isPresent() ? value.get() : null;
                } else {
                    return key;
                }
            }
        });
        when(webResourceIntegration.getI18nText(any(Locale.class), anyString())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Locale locale = (Locale) invocation.getArguments()[0];
                String key = (String) invocation.getArguments()[1];
                String fullKey = LocaleUtils.serialize(locale) + "-" + key;
                if (stubbedKeys.containsKey(fullKey)) {
                    Optional<String> value = stubbedKeys.get(fullKey);
                    return value.isPresent() ? MessageFormat.format(value.get(), new Object[0]) : null;
                } else {
                    return key;
                }
            }
        });

        when(webResourceIntegration.getSupportedLocales()).thenReturn(LOCALES);
    }

    private String transform(String javascript) {
        return transform(javascript, QueryParams.of(ImmutableMap.of("locale", MOCK_LOCALE)));
    }

    private String transform(String javascript, QueryParams queryParams) {
        try {
            TransformableResource origResource = new TransformableResource(null, null, new StringDownloadableResource(javascript));
            DownloadableResource resource = transformerFactory.makeResourceTransformer(null).transform(origResource, queryParams);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            resource.streamResource(out);
            return out.toString("UTF-8");
        } catch (DownloadException ex) {
            throw new RuntimeException(ex);
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    private class StringDownloadableResource implements DownloadableResource {
        private final String content;

        StringDownloadableResource(final String content) {
            this.content = content;
        }

        public void streamResource(final OutputStream out) throws DownloadException {
            try {
                out.write(content.getBytes());
            } catch (final IOException e) {
                throw new DownloadException(e);
            }
        }

        public void serveResource(final HttpServletRequest request, final HttpServletResponse response) throws DownloadException {
        }

        public boolean isResourceModified(final HttpServletRequest request, final HttpServletResponse response) {
            return false;
        }

        public String getContentType() {
            return null;
        }

    }
}
