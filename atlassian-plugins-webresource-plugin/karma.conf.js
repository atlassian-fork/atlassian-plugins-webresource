/* eslint-env node */
var path = require('path');

module.exports = function(config) {
    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: 'src',

        // frameworks to use
        frameworks: ['qunit', 'sinon'],

        plugins: [
            'karma-qunit',
            'karma-sinon',
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher'
        ],

        // list of files / patterns to load in the browser
        files: [
            require.resolve('sinon-qunit'),
            // deps
            'test/resources/js/ajs-mock.js',
            'test/resources/js/wrm-mock.js',
            // data and context path
            'main/resources/js/data/data.js',
            'main/resources/js/data/context-path.js',
            'main/resources/js/data/format.js',
            'main/resources/js/data/i18n.js',
            // in-order batches loader
            'main/resources/js/in-order-loader.js',
            // amd
            require.resolve('almond/almond.js'),
            '../target/modules-test-build.js',
            'main/resources/js/web-resources/atlassian-module.js',
            // tests
            'test/resources/js/**/*-test.js'
        ],


        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: path.resolve(
                __dirname,
                'target/surefire-reports/',
                'karma-results.xml'
            ),
            classNameFormatter: function(browser) {
                var parts = [];
                parts.push(browser.name);
                parts.push('WRM Plugin');
                return parts.map(function(part) {
                    return part.replace(/[\s.]/g, '_');
                }).join('.');
            },
            suite: 'WRM Plugin'
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        //browsers: ['Chrome', 'Safari', 'Firefox', 'Opera', 'IE11 - Win7', 'IE10 - Win7', 'IE9 - Win7'],
        browsers: ['PhantomJS'],


        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,


        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });
};
