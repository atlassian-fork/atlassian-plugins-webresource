package com.atlassian.webresource.compiler;

import com.atlassian.webresource.spi.CompilerEntry;
import com.atlassian.webresource.spi.CompilerUtil;
import com.atlassian.webresource.spi.ResourceCompiler;
import com.google.javascript.jscomp.AbstractCommandLineRunner;
import com.google.javascript.jscomp.CompilationLevel;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Testing GCC.
 *
 * @since v3.5.10
 */
public class TestGoogleClosureCompiler {

    private List<String> paths;
    private List<CompilerEntry> entries;
    private File cliCompiled;

    @Before
    public void setUp() throws IOException {
        paths = asList(
                Paths.get("src", "test", "resources", "js", "logger.js").toString(),
                Paths.get("src", "test", "resources", "js", "veryVeryLongNameForObject.js").toString(),
                Paths.get("src", "test", "resources", "js", "dostuff.js").toString());
        entries = paths
                .stream()
                .map(s -> {
                    try (InputStream is = new FileInputStream(s)) {
                        return CompilerEntry.ofKeyValue(s.substring(s.lastIndexOf("/") + 1), IOUtils.toString(is));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }).collect(Collectors.toList());
        cliCompiled = File.createTempFile("temp-compiled-file", ".js");
        cliCompiled.deleteOnExit();
    }

    @Test
    public void testSizeIsReduced() throws IOException {
        ResourceCompiler compiler = new GCCResourceCompiler();
        compiler.compile(entries.stream());
        try (FileInputStream in = new FileInputStream(paths.get(1))) {
            String notCompiledContent = IOUtils.toString(in);
            String justCompiledContent = compiler.content(entries.get(1).key());
            assertThat(notCompiledContent.length(), greaterThan(justCompiledContent.length()));
        }
    }

    @Test
    public void testPathCompilation() throws IOException {
        ResourceCompiler compiler = new GCCResourceCompiler();
        compiler.compile(entries.stream());
        String veryVeryLongNameForObject = entries.get(1).key();
        String justCompiledContent = compiler.content(veryVeryLongNameForObject);

        // change to veryVeryLongNameForObject-min.js after APDEX-1028
        String expectedCompiled = Paths.get("src", "test", "resources", "js", "veryVeryLongNameForObject-small.js").toString();
        try (FileInputStream in = new FileInputStream(expectedCompiled)) {
            String expectedCompiledContent = IOUtils.toString(in);
            assertEquals(
                    format("Just minified code from [%s] not equals to contents of [%s]",
                            veryVeryLongNameForObject, expectedCompiled), expectedCompiledContent, justCompiledContent);
        }
    }

    @Test
    public void testCompilerUtil() throws IOException {
        ResourceCompiler compiler = new GCCResourceCompiler();
        compiler.compile(entries.stream());
        String veryVeryLongNameForObject = entries.get(1).key();

        // change to veryVeryLongNameForObject-min.js after APDEX-1028
        String expectedCompiled = Paths.get("src", "test", "resources", "js", "veryVeryLongNameForObject-small.js").toString();
        try (InputStream compiledInputStream = CompilerUtil.toInputStream(compiler, veryVeryLongNameForObject);
             FileInputStream actualInputStream = new FileInputStream(expectedCompiled)) {

            assertNotNull("Compiled InputStream is null", compiledInputStream);

            String expectedCompiledContent = IOUtils.toString(actualInputStream, CompilerUtil.CHARSET);
            String justCompiledContent = IOUtils.toString(compiledInputStream, CompilerUtil.CHARSET);

            assertEquals("Expected and compiled InputStreams have different number of bytes",
                    compiledInputStream.available(), actualInputStream.available());
            assertEquals(
                    format("Just minified code from [%s] not equals to contents of [%s]",
                            veryVeryLongNameForObject, expectedCompiled), expectedCompiledContent, justCompiledContent);
        }
    }

    @Test
    @Ignore("APDEX-1028")
    public void testProgrammaticAndCLICompilation() throws IOException, AbstractCommandLineRunner.FlagUsageException {
        // Perform CLI compilation
        WRMCommandLineRunner runner = new WRMCommandLineRunner(new String[]{
                "--js", paths.get(1),
                "--js_output_file", cliCompiled.getAbsolutePath(),
                "--compilation_level", CompilationLevel.ADVANCED_OPTIMIZATIONS.name()});
        runner.performRun();

        try (FileInputStream in = new FileInputStream(cliCompiled)) {
            String cliCompiledContent = IOUtils.toString(in);
            // Compiler#toSource() adds ";\n" at the end of minified file, whilst Compiler#toSource(Node n) is not
            cliCompiledContent = cliCompiledContent.substring(0, cliCompiledContent.lastIndexOf(";\n"));

            // Perform programmatic compilation
            ResourceCompiler compiler = new GCCResourceCompiler();
            compiler.compile(Stream.of(entries.get(1)));
            String justCompiledContent = compiler.content(entries.get(1).key());

            assertEquals("CLI minified code not equals to programmatically minified code",
                    cliCompiledContent, justCompiledContent);
        }
    }

}
