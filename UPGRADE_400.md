# Host application upgrade notes

Version 4.0.0 of the WRM includes an upgrade to platform version 5, as well as backwards-incompatible changes
to default behaviours when requiring and draining resources.

## Resource draining changes

### Internet Explorer compatibility

Previously, the WRM included the ability to output `web-resources` to specific versions of Internet Explorer:

```xml
<resource type="download" name="something-for-ie.js" location="js/something-for-ie.js">
  <param name="ieonly" value="true" />
  <param name="conditionalComment" value="lt ie 8" />
</resource>
```

This ability was implemented using [Internet Explorer's conditional comments feature][ccie].
This feature stopped working in Internet Explorer 10, meaning that resources tagged with these parameters would
not be loaded by IE 10 or higher.

Today, WRM only supports Internet Explorer 11 and higher. As a simplification, we have removed the rendering of
conditional comments from the WRM. Use of the `ieonly` and `conditionalComment` parameters is deprecated.

To prevent unwanted rendering in modern browsers, resources tagged with the `ieonly` or `conditionalComment` params
will be dropped from resource sets and never rendered.

Developers should audit their web-resources and remove any `<resource>`s that use these params, as future versions
of the WRM may remove the special handling of these parameters entirely.

### Superbatch draining

Previously, the superbatch had a buggy behaviour: it would be automatically excluded from all resource resolutions
based on the assumption that the superbatch would always be provided with the first call to
`webResourceAssembler.assembled().drainIncludedResources()`.

However, you can configure a WebResourceAssembler to omit the superbatch by calling
`WebResourceAssemblerBuilder#includeSuperbatchResources(true)`, which means the superbatch will not automatically
be added to drain calls.

In this version of the WRM, this automatic exclusion of the superbatch is gone. This allows resources declared in it
to be drained in subsequent drain calls, regardless of whether the whole superbatch was served or not.

If you need to emulate the original behaviour, you can do so by explicitly adding the `Config#SUPERBATCH_KEY`
in to the excluded resources list after the first drain call:

```
WebResourceSet set = webResourceAssembler.assembled().drainIncludedResources();
webResourceAssembler.resources().exclude(Config.SUPERBATCH_KEY);
// any resources referenced via the superbatch will not be served, regardless of
// whether they are explicitly required or not.
```
