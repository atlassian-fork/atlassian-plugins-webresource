package com.atlassian.webresource.spi;

import javax.annotation.Nonnull;
import java.util.stream.Stream;

/**
 * SPI for resource compiler (minifier). Being used by atlassian-plugins-webresource MinificationTransformer.
 *
 * @since v3.5.10
 */
public interface ResourceCompiler {

    /**
     * Compiles/minifies provided {@link Stream} of resources' entries.
     *
     * @param entries - resources entries to minify.
     */
    void compile(@Nonnull final Stream<CompilerEntry> entries);

    /**
     * Provides compiled/minified contents of the resource under provided key.
     *
     * @param key - wanted resource's ID (can be a path).
     * @return compiled code
     */
    String content(@Nonnull final String key);

}
