# Host application upgrade notes

In this release, we extended the integration for the translation mechanism. 

## Translations

In version 3.6.0 we introduced the `WRM.format` function and `wrm/format` module. In this release, we have added the
`WRM.I18n.getText` function and `wrm/i18n` module to our runtime. The new function and module can be used to provide
translation in JavaScript code.

Previously, you would have to use the `AJS.I18n.getText()` function to use the translations mechanism.
Now, you can use the `WRM.I18n.getText` or even better, require the module directly in your code:

```js
// Require the AMD module
require(['wrm/i18n'], function(I18n) {
    var myParam = 1234;

    console.log(I18n.getText('my.translation.key', myParam));
});
```


Add the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n` web-resource to the superbatch.
It must be added before any other web-resources that make use of the `I18nJsTransformer`.
