# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- The "root" web-resource `key` was changed so as to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.

## [4.2.0]

### Added
- `WRM.I18n.getText` JavaScript function and `wrm/i18n` modules available from
  the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n` web-resource
  (resolves PLUGWEB-456).

## [4.1.3]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request and their
  callbacks will execute immediately.
- The "root" web-resource `key` was changed so as to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4)
  (fixes PLUGWEB-454).
- Deleted resource temp files no longer cause permanent resource compilation (fixes PLUGWEB-453).

## [4.1.2]

### Changed
- resources can now be flagged with "allow-public-use" param when you want them served with a CORS header

### Fixed
- Fixed a problem where queued callbacks for `WRM.require` would occasionally resolve before their
  required scripts had been loaded.


## [4.1.0]

### Changed
 - Bunch of APIs are not experimental anymore, for example
 `UrlReadingWebResourceTransformer`, `UrlReadingContentTransformer`, `QueryParams`, `UrlBuilder`, `UrlMode`.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request and their
  callbacks will execute immediately.

## [4.0.8]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request and their
  callbacks will execute immediately.
- The "root" web-resource `key` was changed so as to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.

## [4.0.7]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

## [4.0.6]

### Changed
- resources can now be flagged with "allow-public-use" param when you want them served with a CORS header

## [4.0.3]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.

## [4.0.2]

### Changed
- Tomcat's `ClientAbortException`, thrown when the end-user closes connection abruptly,
  will be swallowed by the plugin and not propagated to the main application.

## [4.0.1]
- `WRM.require` will continue to work if the version of jQuery used does not return a `jQuery.Deferred` object
  in its `jQuery.ajax` method.

## [4.0.0]

### Changed
- Adds support for Java 11.
- Now depends on Platform 5 being provided by the host product.

### Fixed
- The superbatch is only excluded from requests automatically if it was drained previously.
- Async requests for resources can now pull in resources if they are referenced through the superbatch.

### Removed
- The WRM has removed support for IE 10 and older.
- The ability to conditionally output web-resources to Internet Explorer has been removed.
  `<resource>` blocks with either an `ieOnly` or `conditionalComment` param will not be rendered at all.

## [3.7.3]

### Changed
- Bad requests to the `/resources` REST endpoint no longer output a full stack trace.

### Fixed
- The "root" web-resource `key` was changed so as to avoid find-and-replace logic that treats
  double-underscores as the start of a variable name.

## [3.7.2]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

## [3.7.1]

### Fixed
- Fixed a problem where queued callbacks for `WRM.require` would occasionally resolve before their
  required scripts had been loaded.

## [3.7.0]

### Changed
- Deprecation log events will now only log at WARN level if the atlassian.dev.mode property is
  set, otherwise they will be logged at DEBUG level.

### Fixed
- Callbacks for `WRM.require` calls to unrelated resources will resolve correctly once all resources for that
  specific require call have been loaded.
- `WRM.require` calls that result in an empty set of resources will no longer make a remote request and their
  callbacks will execute immediately.

## [3.6.6]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).

## [3.6.5]

### Changed
- Tomcat's `ClientAbortException`, thrown when the end-user closes connection abruptly,
  will be swallowed by the plugin and not propagated to the main application.
  
## [3.6.4]

### Fixed
- `WRM.require` will continue to work if the version of jQuery used does not return a `jQuery.Deferred` object
  in its `jQuery.ajax` method.

## [3.6.3]

### Changed
- Replaced usage of jQuery for DOM element creation with native equivalents.
- Use of `jQuery.Deferred#then` was replaced with `jQuery.Deferred#done` and `jQuery.Deferred#fail`, so that
  behaviour will remain synchronous irrespective of what version of jQuery is used.

## [3.6.1]

### Fixed

- The change to `JsI18nTransformer` in [3.6.0] was reverted;
  translation substitutions will preserve the original variable where the `I18n` object was found.
    - This allows unit tests that stub or mock the original variable to continue working.
- Usages of `AJS.log` have been replaced with `console.log`.

## [3.6.0]

### Added
- `web-resource` module descriptors can be marked as deprecated by adding a `<deprecated/>` child element to them.
  The following attributes and values are accepted:
  `<deprecation since="version" remove="version" alternative="webresource:key">a detailed reason</deprecation>`
- `WRM.format` JavaScript function available from
  the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:format` web-resource

### Changed
- `JsI18nTransformer` will always rewrite translations with substitutions
  to use `WRM.format` -- for instance, `AJS.I18n.getText("foo.bar", 1, 2, 3)` will become
  `WRM.format("value of 'foo.bar' translation key", 1, 2)`.
- Requiring the `com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager`
  web-resource no longer pulls in the `com.atlassian.auiplugin:aui-core` web-resource from AUI.

### Fixed
- The WRM consumes `jQuery` via `window` instead of via `AJS.$`.

## [3.5.44]

### Fixed
- The `JsI18nTransformer` will find and transform javascript translation calls in the format of
  `variableName["I18n"].getText(...)` (this format is typically output by Babel 7 and Webpack 4).
