package com.atlassian.plugin.webresource;

import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.cache.filecache.impl.PassThroughCache;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Controller;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformers;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.webresource.spi.NoOpResourceCompiler;
import com.atlassian.webresource.spi.ResourceCompiler;

import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.helpers.Helpers.asDownloadableResource;

/**
 * Default implementation of {@link PluginResourceLocator}.
 *
 * @since 2.2
 * @deprecated since 3.3.2
 */
@Deprecated
public class PluginResourceLocatorImpl implements PluginResourceLocator {
    private volatile Globals globals;

    public PluginResourceLocatorImpl(final WebResourceIntegration webResourceIntegration,
                                     final ServletContextFactory servletContextFactory, final WebResourceUrlProvider webResourceUrlProvider,
                                     final PluginEventManager pluginEventManager, final ResourceCompiler resourceCompiler) {
        this(webResourceIntegration, servletContextFactory, webResourceUrlProvider,
                new DefaultResourceBatchingConfiguration(), pluginEventManager, resourceCompiler);
    }

    public PluginResourceLocatorImpl(final WebResourceIntegration webResourceIntegration,
                                     final ServletContextFactory servletContextFactory, final WebResourceUrlProvider webResourceUrlProvider,
                                     final ResourceBatchingConfiguration batchingConfiguration, final PluginEventManager pluginEventManager,
                                     final ResourceCompiler resourceCompiler) {
        Config config = new Config(
                batchingConfiguration,
                webResourceIntegration,
                webResourceUrlProvider,
                servletContextFactory,
                new TransformerCache(pluginEventManager, webResourceIntegration.getPluginAccessor()),
                resourceCompiler
        );
        StaticTransformers staticTransformers = new DefaultStaticTransformers(new DefaultStaticTransformersSupplier(
                webResourceIntegration, webResourceUrlProvider, config.getCdnResourceUrlTransformer()));
        config.setStaticTransformers(staticTransformers);
        initialize(pluginEventManager, config);
    }

    public PluginResourceLocatorImpl(final WebResourceIntegration webResourceIntegration,
                                     final ServletContextFactory servletContextFactory, final WebResourceUrlProvider webResourceUrlProvider,
                                     final ResourceBatchingConfiguration batchingConfiguration,
                                     final PluginEventManager pluginEventManager, final StaticTransformers staticTransformers,
                                     final ResourceCompiler resourceCompiler) {
        Config config = new Config(
                batchingConfiguration,
                webResourceIntegration,
                webResourceUrlProvider,
                servletContextFactory,
                new TransformerCache(pluginEventManager, webResourceIntegration.getPluginAccessor()),
                resourceCompiler
        );
        config.setStaticTransformers(staticTransformers);
        initialize(pluginEventManager, config);
    }


    public PluginResourceLocatorImpl(PluginEventManager pluginEventManager, Config config) {
        initialize(pluginEventManager, config);
    }

    // Constructors with NoOpResourceCompiler for backwards compatibility
    public PluginResourceLocatorImpl(final WebResourceIntegration webResourceIntegration,
                                     final ServletContextFactory servletContextFactory, final WebResourceUrlProvider webResourceUrlProvider,
                                     final PluginEventManager pluginEventManager) {
        this(webResourceIntegration, servletContextFactory, webResourceUrlProvider,
                new DefaultResourceBatchingConfiguration(), pluginEventManager, new NoOpResourceCompiler());
    }

    public PluginResourceLocatorImpl(final WebResourceIntegration webResourceIntegration,
                                     final ServletContextFactory servletContextFactory, final WebResourceUrlProvider webResourceUrlProvider,
                                     final ResourceBatchingConfiguration batchingConfiguration, final PluginEventManager pluginEventManager) {
        this(webResourceIntegration, servletContextFactory, webResourceUrlProvider, batchingConfiguration, pluginEventManager, new NoOpResourceCompiler());
    }

    public PluginResourceLocatorImpl(final WebResourceIntegration webResourceIntegration,
                                     final ServletContextFactory servletContextFactory,
                                     final WebResourceUrlProvider webResourceUrlProvider,
                                     final ResourceBatchingConfiguration batchingConfiguration,
                                     final PluginEventManager pluginEventManager, final StaticTransformers staticTransformers) {
        this(webResourceIntegration, servletContextFactory, webResourceUrlProvider, batchingConfiguration,
                pluginEventManager, staticTransformers, new NoOpResourceCompiler());
    }

    protected void initialize(PluginEventManager pluginEventManager, Config config) {
        globals = new Globals(config);

        // TODO find better way to register, move it outside of the constructor.
        pluginEventManager.register(this);
    }

    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        globals.triggerStateChange();
    }

    @PluginEventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        globals.triggerStateChange();
    }

    @PluginEventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
        if (event.getModule() instanceof WebResourceModuleDescriptor) {
            globals.triggerStateChange();
        }
    }

    @PluginEventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
        if (event.getModule() instanceof WebResourceModuleDescriptor) {
            globals.triggerStateChange();
        }
    }

    @Override
    public boolean matches(final String url) {
        return globals.getRouter().canDispatch(url);
    }

    @Override
    public DownloadableResource getDownloadableResource(final String url, Map<String, String> queryParams) {
        // This code should be deleted when Confluence would be updated and stop using
        // PluginResourceLocator.getDownloadableResource
        // See https://ecosystem.atlassian.net/browse/PLUGWEB-193

        // For unknown reason Confluence could pass null as query params.
        if (queryParams == null) {
            queryParams = new HashMap<>();
        }

        final DownloadableResource[] downloadableResource = new DownloadableResource[]{null};
        Router router = new Router(globals) {
            @Override
            protected Controller createController(final Globals globals, final Request request, final Response response) {
                return new Controller(globals, request, response) {
                    @Override
                    protected void sendCached(final Content content, Map<String, String> params, final boolean isCachingEnabled) {
                        downloadableResource[0] = asDownloadableResource(
                                new ContentImpl(content.getContentType(), content.isTransformed()) {
                                    @Override
                                    public SourceMap writeTo(final OutputStream out, final boolean isSourceMapEnabled) {
                                        Cache cache = isCachingEnabled ? globals.getContentCache() : new PassThroughCache();
                                        cache.cache("http", request.getUrl(), out,
                                                producerOut -> content.writeTo(producerOut, false));
                                        return null;
                                    }
                                }
                        );
                    }

                    @Override
                    protected boolean checkIfCachedAndNotModified(final Date updatedAt) {
                        return false;
                    }
                };
            }
        };
        router.dispatch(new Request(globals, url, queryParams), null);
        return downloadableResource[0];
    }

    public Globals temporaryWayToGetGlobalsDoNotUseIt() {
        return globals;
    }
}
