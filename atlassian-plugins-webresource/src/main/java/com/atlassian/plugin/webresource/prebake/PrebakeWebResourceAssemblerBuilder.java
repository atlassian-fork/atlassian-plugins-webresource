package com.atlassian.plugin.webresource.prebake;

import com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;

/**
 * @since v3.5.0
 */
public interface PrebakeWebResourceAssemblerBuilder extends WebResourceAssemblerBuilder {
    PrebakeWebResourceAssemblerBuilder withCoordinate(Coordinate coord);

    @Override
    PrebakeWebResourceAssembler build();
}
