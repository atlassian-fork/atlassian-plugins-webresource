package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.dom4j.Element;

import javax.annotation.Nonnull;

/**
 * Defines a module descriptor for a {@link WebResourceTransformerFactory}.
 *
 * @since 3.0
 */
public class UrlReadingWebResourceTransformerModuleDescriptor extends AbstractModuleDescriptor<WebResourceTransformerFactory> {
    private String aliasKey;

    public UrlReadingWebResourceTransformerModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    private final ResettableLazyReference<WebResourceTransformerFactory> moduleLazyReference = new ResettableLazyReference<WebResourceTransformerFactory>() {
        @Override
        protected WebResourceTransformerFactory create() throws Exception {
            return moduleFactory.createModule(moduleClassName, UrlReadingWebResourceTransformerModuleDescriptor.this);
        }
    };

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        this.aliasKey = element.attributeValue("alias-key");
        super.init(plugin, element);
    }

    @Override
    public void disabled() {
        moduleLazyReference.reset();
        super.disabled();
    }

    @Override
    public WebResourceTransformerFactory getModule() {
        return moduleLazyReference.get();
    }

    public String getAliasKey() {
        return aliasKey;
    }
}
