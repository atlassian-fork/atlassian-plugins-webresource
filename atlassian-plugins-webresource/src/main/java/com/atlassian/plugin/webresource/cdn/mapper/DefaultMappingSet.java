package com.atlassian.plugin.webresource.cdn.mapper;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;

/**
 * Simple wrapper for mappings.
 *
 * @since v3.5.7
 */
public class DefaultMappingSet implements MappingSet {

    public static final MappingSet EMPTY =
            new DefaultMappingSet(emptyList());

    private final Map<String, Mapping> mappings;

    public DefaultMappingSet(@Nonnull final Collection<Mapping> mappings) {
        checkNotNull(mappings, "Collection of mappings is null!");
        TreeMap<String, Mapping> tm = new TreeMap<>();
        mappings.stream().forEach(e -> tm.put(e.originalResource(), e));
        this.mappings = Collections.unmodifiableMap(tm);
    }

    @Override
    @Nonnull
    public Optional<Mapping> get(final String originalResource) {
        return Optional.ofNullable(mappings.get(originalResource));
    }

    @Override
    @Nonnull
    public List<String> getMappedResources(final String originalResource) {
        return get(originalResource).map(Mapping::mappedResources).orElseGet(Collections::emptyList);
    }

    @Override
    @Nonnull
    public Iterable<Mapping> all() {
        return mappings.values();
    }

    public int size() {
        return mappings.size();
    }
}
