package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.sourcemap.SourceMap;

import java.io.OutputStream;

/**
 * Binary content of Web Resource.
 *
 * @since 3.3
 */
public interface Content
{
    /**
     * Write the resource to the supplied OutputStream and return its Source Map. Note that the OutputStream will not be
     * closed by this method.
     *
     * @param out the stream to write to
     * @param isSourceMapEnabled should the source mab being generated
     * @return source map of the resource, may return null.
     */
    SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled);

    /**
     * Returns the content type for the resource. May return null if it cannot resolve its own content type.
     */
    String getContentType();

    /**
     * @return if content has been transformed.
     */
    boolean isTransformed();
}