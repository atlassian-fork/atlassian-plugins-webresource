package com.atlassian.plugin.webresource.cdn.mapper;

import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * <p>
 * Parses a JSON document containing web resource mappings.
 * </p>
 * JSON Example: {@code { "/jira/local/resource/url.css" : ["//remote/resource/url.css"], ... }}
 *
 * @since v3.5.0
 */
public class MappingParser {
    private static final Logger log = LoggerFactory.getLogger(MappingParser.class);

    public MappingParser() {
    }

    /**
     * @param mappingReader Reader pointing to the mapping file.
     * @return Mappings read from the file.
     * @throws MappingParserException
     */
    @Nonnull
    public MappingSet parse(@Nonnull final Reader mappingReader) throws MappingParserException {
        checkNotNull(mappingReader, "Can't read from null reader!");
        try {
            final JsonElement mappingRoot = new JsonParser().parse(mappingReader);
            final JsonObject mappings = mappingRoot.getAsJsonObject();
            if (mappings == null) {
                throw new MappingParserException("Root object 'mappings' not found in JSON!");
            }
            final List<Mapping> mappedResources = new ArrayList<>();
            for (Map.Entry<String, JsonElement> item : mappings.entrySet()) {
                Stream<String> values = StreamSupport.stream(item.getValue().getAsJsonArray().spliterator(), false)
                        .map(JsonElement::getAsString);
                mappedResources.add(new DefaultMapping(item.getKey(), values));
            }
            log.debug("MappingParser just read {} entries.", mappedResources.size());
            return new DefaultMappingSet(mappedResources);
        } catch (JsonIOException | JsonSyntaxException | IllegalStateException e) {
            log.error("Failed to read mappings!", e);
            throw new MappingParserException("Failed to read mappings!", e);
        }
    }

    @Nonnull
    public String getAsString(@Nonnull final MappingSet mappings) throws IOException {
        checkNotNull(mappings, "Can't write null mappings!");
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.setLenient(true);
            jsonWriter.beginObject();

            for (Mapping mapping : mappings.all()) {
                jsonWriter.name(mapping.originalResource());

                jsonWriter.beginArray();
                for (String mappedResource : mapping.mappedResources()) {
                    jsonWriter.value(mappedResource);
                }
                jsonWriter.endArray();
            }

            jsonWriter.endObject();
            return stringWriter.toString();
        } catch (IOException e) {
            log.error("Failed to serialize MappingSet to JSON!", e);
            throw e;
        }
    }
}
