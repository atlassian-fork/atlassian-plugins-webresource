package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.json.marshal.Jsonable;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

/**
 */
public final class BigPipe {
    private final FutureCompletionService<String, Jsonable> completor;

    public BigPipe() {
        this.completor = new QueueFutureCompletionService<>();
    }

    public BigPipe push(final String key, CompletionStage<Jsonable> promise) {
        completor.add(key, promise);
        return this;
    }

    /**
     * @return all available content without blocking. If no content is available, the empty list is returned.
     */
    public Iterable<KeyedValue<String, Jsonable>> getAvailableContent() {
        return completor.poll();
    }

    /**
     * @return return all content without blocking. Any incomplete futures are converted to failures
     */
    public Iterable<KeyedValue<String, Jsonable>> forceCompleteAll() {
        completor.forceCompleteAll();
        return completor.poll();
    }

    /**
     * Waits for content.
     * <p>
     * If any content is available, this method returns immediately. If content is not available,
     * it blocks until any content becomes available.
     *
     * Warning:
     * This function doesn't contain any timeout - may be dangerous to those who create 'promises without future'.
     * Always make sure that your promises are being executed when using this function.
     *
     * Experimental:
     * Currently function only waits if there are any pending tasks, it may change in the future.
     *
     * @return content from the Big Pipe.
     * @throws InterruptedException if interrupted while executing
     */
    @ExperimentalApi
    public Iterable<KeyedValue<String, Jsonable>> waitForContent() throws InterruptedException {
        completor.waitAnyPendingToComplete();
        return completor.poll();
    }

    /**
     * Waits for content.
     * <p>
     * If any content is available, this method returns immediately. If content is not available,
     * it blocks until any content becomes available.
     *
     * @param timeout how long to wait before giving up, in units of unit
     * @param unit    a TimeUnit determining how to interpret the timeout parameter
     * @return content from the Big Pipe. If no content becomes available before the timeout, the empty
     * list is returned.
     * @throws InterruptedException if interrupted while executing
     */
    public Iterable<KeyedValue<String, Jsonable>> waitForContent(long timeout, TimeUnit unit) throws InterruptedException {
        return completor.poll(timeout, unit);
    }

    public boolean isComplete() {
        return completor.isComplete();
    }

}
