package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerMatcher;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;

/**
 * Implementation of {@link com.atlassian.plugin.webresource.transformer.StaticTransformersSupplier}
 *
 * @since v3.1.0
 */
public class DefaultStaticTransformersSupplier implements StaticTransformersSupplier {
    private final Iterable<DescribedTransformer> describedTransformers;

    public DefaultStaticTransformersSupplier(
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider urlProvider,
            CdnResourceUrlTransformer cdnResourceUrlTransformer) {
        RelativeUrlTransformerFactory relativeUrlTransformerFactory =
                new RelativeUrlTransformerFactory(webResourceIntegration, urlProvider, cdnResourceUrlTransformer);
        RelativeUrlTransformerMatcher relativeUrlTransformerMatcher = new RelativeUrlTransformerMatcher();
        describedTransformers = Lists.newArrayList(
                new DescribedTransformer(relativeUrlTransformerMatcher, relativeUrlTransformerFactory)
        );
    }

    @Override
    public Dimensions computeDimensions() {
        Dimensions d = Dimensions.empty();
        for (DescribedTransformer dt : describedTransformers) {
            DimensionAwareWebResourceTransformerFactory t = dt.transformerFactory;
            d = d.product(t.computeDimensions());
        }
        return d;
    }

    @Override
    public Iterable<DimensionAwareWebResourceTransformerFactory> get(final String locationType) {
        return toTransformerFactories(describedTransformers, new Predicate<DescribedTransformer>() {
            @Override
            public boolean apply(@Nullable DescribedTransformer input) {
                return input.matcher.matches(locationType);
            }
        });
    }

    @Override
    public Iterable<DimensionAwareWebResourceTransformerFactory> get(final ResourceLocation resourceLocation) {
        return toTransformerFactories(describedTransformers, new Predicate<DescribedTransformer>() {
            @Override
            public boolean apply(@Nullable DescribedTransformer input) {
                return input.matcher.matches(resourceLocation);
            }
        });
    }

    private static Iterable<DimensionAwareWebResourceTransformerFactory> toTransformerFactories(
            Iterable<DescribedTransformer> describedTransformers, Predicate<DescribedTransformer> predicate) {
        return Iterables.transform(Iterables.filter(describedTransformers, predicate), new Function<DescribedTransformer, DimensionAwareWebResourceTransformerFactory>() {
            @Override
            public DimensionAwareWebResourceTransformerFactory apply(@Nullable DescribedTransformer input) {
                return input.transformerFactory;
            }
        });
    }

    private static final class DescribedTransformer {
        private final WebResourceTransformerMatcher matcher;
        private final DimensionAwareWebResourceTransformerFactory transformerFactory;

        private DescribedTransformer(WebResourceTransformerMatcher matcher, DimensionAwareWebResourceTransformerFactory transformerFactory) {
            this.matcher = matcher;
            this.transformerFactory = transformerFactory;
        }
    }
}
