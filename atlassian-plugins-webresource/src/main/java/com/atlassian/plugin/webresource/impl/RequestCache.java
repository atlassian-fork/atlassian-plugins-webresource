package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Request cache.
 *
 * @since v3.3
 */
public class RequestCache
{
    /**
     * Key to identify Resource.
     */
    public static class ResourceKey
    {
        public String key;
        public String name;

        public ResourceKey(String key, String name)
        {
            this.key = key;
            this.name = name;
        }
    }

    /**
     * Container storing resource keys.
     */
    public static class ResourceKeysSupplier
    {
        private final List<ResourceKey> keys;

        public ResourceKeysSupplier(List<ResourceKey> keys)
        {
            this.keys = keys;
        }

        public List<ResourceKey> getKeys()
        {
            return keys;
        }
    }

    public static List<ResourceKey> toResourceKeys(List<Resource> resources)
    {
        List<ResourceKey> resourceKeys = new ArrayList<>();
        for (Resource resource : resources)
        {
            resourceKeys.add(new ResourceKey(resource.getKey(), resource.getName()));
        }
        return resourceKeys;
    }

    protected final Globals globals;
    protected final Map<Bundle, LinkedHashMap<String, Resource>> cachedResources = new HashMap<>();
    protected final Map<ResourceKeysSupplier, List<Resource>> cachedResourceLists
        = new HashMap<>();
    protected final Map<CachedCondition, Boolean> cachedConditionsEvaluation
        = new HashMap<>();
    protected final Map<CachedCondition, DefaultUrlBuilder> cachedConditionsParameters
        = new HashMap<>();
    protected Snapshot cachedSnapshot;

    public RequestCache(Globals globals)
    {
        this.globals = globals;
    }

    public Globals getGlobals()
    {
        return globals;
    }

    /**
     * Needed by WebResource as a storage for Cache.
     */
    public Map<Bundle, LinkedHashMap<String, Resource>> getCachedResources()
    {
        return cachedResources;
    }

    /**
     * Batches and Sub Batches are cached and as such can't contain references to Resources, this cache helps
     * with that.
     */
    public List<Resource> getCachedResources(ResourceKeysSupplier resourceKeysSupplier)
    {
        List<Resource> resources = cachedResourceLists.get(resourceKeysSupplier);
        if (resources == null)
        {
            resources = new ArrayList<>();
            for (ResourceKey key : resourceKeysSupplier.getKeys())
            {
                resources.add(cachedSnapshot.get(key.key).getResources(this).get(key.name));
            }
            cachedResourceLists.put(resourceKeysSupplier, resources);
        }
        return resources;
    }

    public Map<CachedCondition, Boolean> getCachedConditionsEvaluation()
    {
        return cachedConditionsEvaluation;
    }

    public Map<CachedCondition, DefaultUrlBuilder> getCachedConditionsParameters()
    {
        return cachedConditionsParameters;
    }

    /**
     * Get all bundles.
     *
     * It is another layer of cache over the `globals.getBundles()` because it is used very
     * heavily, to avoid any performance drawbacks of atomic reference in the `globals.getBundles()`.
     */
    public Snapshot getSnapshot()
    {
        if (cachedSnapshot == null)
        {
            cachedSnapshot = globals.getSnapshot();
        }
        return cachedSnapshot;
    }
}