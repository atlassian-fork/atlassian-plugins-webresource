package com.atlassian.plugin.webresource;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import com.atlassian.webresource.spi.TransformationDto;
import com.atlassian.webresource.spi.TransformerDto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * The integration points between the Web Resource layer, and specific applications (eg JIRA, Confluence).
 *
 * @see com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService#DefaultPageBuilderService(WebResourceIntegration, com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory)
 * @see com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory#DefaultWebResourceAssemblerFactory(PluginResourceLocator)
 * @see WebResourceManagerImpl#WebResourceManagerImpl(PluginResourceLocator, WebResourceIntegration, WebResourceUrlProvider)
 */
public interface WebResourceIntegration {
    /**
     * Applications must implement this method to get access to the application's PluginAccessor
     */
    PluginAccessor getPluginAccessor();

    /**
     * Applications must implement this method to get access to the application's PluginEventManager
     *
     * @since 3.1.1
     * @deprecated since v3.3.2
     */
    @Deprecated
    PluginEventManager getPluginEventManager();

    /**
     * This must be a thread-local cache that will be accessible from both the page, and the decorator
     */
    Map<String, Object> getRequestCache();

    /**
     * Represents the unique number for this system, which when updated will flush the cache. This should be a number
     * and is generally stored in the global application-properties.
     *
     * @return A string representing the count
     */
    String getSystemCounter();

    /**
     * Represents the last time the system was updated.  This is generally obtained from BuildUtils or similar.
     */
    String getSystemBuildNumber();

    /**
     * The version number of the host application, for example "7.0.0-OD-07" or "5.6". It is intended to identify
     * if two instances of an application are the same version (the versions of plugins are checked separately).
     * This differs from {@link #getSystemBuildNumber()}, which does not necessarily change when the application changes
     * (at least in JIRA this represents the database build number, not the application version number).
     *
     * @since 3.4.7
     */
    String getHostApplicationVersion();

    /**
     * Returns the base URL for this application.  This method may return either an absolute or a relative URL.
     * Implementations are free to determine which mode to use based on any criteria of their choosing. For example, an
     * implementation may choose to return a relative URL if it detects that it is running in the context of an HTTP
     * request, and an absolute URL if it detects that it is not.  Or it may choose to always return an absolute URL, or
     * always return a relative URL.  Callers should only use this method when they are sure that either an absolute or
     * a relative URL will be appropriate, and should not rely on any particular observed behavior regarding how this
     * value is interpreted, which may vary across different implementations.
     * <p>
     * In general, the behavior of this method should be equivalent to calling {@link
     * #getBaseUrl(UrlMode)} with a {@code urlMode} value of {@link
     * UrlMode#AUTO}.
     *
     * @return the string value of the base URL of this application
     */
    String getBaseUrl();

    /**
     * Returns the base URL for this application in either relative or absolute format, depending on the value of {@code
     * urlMode}.
     * <p>
     * If {@code urlMode == {@link UrlMode#ABSOLUTE}}, this method returns an absolute URL, with URL
     * scheme, hostname, port (if non-standard for the scheme), and context path.
     * <p>
     * If {@code urlMode == {@link UrlMode#RELATIVE}}, this method returns a relative URL containing
     * just the context path.
     * <p>
     * If {@code urlMode == {@link UrlMode#AUTO}}, this method may return either an absolute or a
     * relative URL.  Implementations are free to determine which mode to use based on any criteria of their choosing.
     * For example, an implementation may choose to return a relative URL if it detects that it is running in the
     * context of an HTTP request, and an absolute URL if it detects that it is not.  Or it may choose to always return
     * an absolute URL, or always return a relative URL.  Callers should only use {@code
     * WebResourceManager.UrlMode#AUTO} when they are sure that either an absolute or a relative URL will be
     * appropriate, and should not rely on any particular observed behavior regarding how this value is interpreted,
     * which may vary across different implementations.
     *
     * @param urlMode specifies whether to use absolute URLs, relative URLs, or allow the concrete implementation to
     *                decide
     * @return the string value of the base URL of this application
     * @since 2.3.0
     */
    String getBaseUrl(UrlMode urlMode);

    /**
     * This version number is used for caching URL generation, and needs to be incremented every time the contents
     * of the superbatch may have changed. Practically this means updating every time the plugin system state changes
     *
     * @return a version number
     */
    String getSuperBatchVersion();

    /**
     * The locale identifier that should be inserted into static resource urls for the current request, if appropriate.
     *
     * @return null if the url should not have a locale component
     * @deprecated since 3.5.22 - This method includes current locale in URL hash which is unnecessary since it's
     * already in a query parameter. The current behaviour causes unnecessary cache misses of pre-baked resources
     * since it'll only work for instances with the same locale in which the pre-baker ran.
     * Use {@link #getI18nStateHash()} instead.
     */
    @Deprecated
    String getStaticResourceLocale();

    /**
     * The locale hash that should be inserted into static resource urls for the current request, if appropriate.
     * This method should return the hash for i18n-contributing plugins without the current locale attached to it.
     *
     * @return null if the url should not have a locale component.
     * @since 3.5.22
     */
    String getI18nStateHash();

    /**
     * A reference to the temporary directory the application want the plugin system to use. The temporary directory can
     * be cleared at any time by the application and can be used by the plugin system to cache things like batches or
     * other things that can easily be re-generated. It is recommended that this directory be /apphome/tmp/webresources.
     * The plugin system can delete any or all files it sees fit that exists under this directory at any time.
     * The directory does not need to exist.
     *
     * @return a File reference to the temporary directory. This can not return null.
     * @since 2.9.0
     */
    File getTemporaryDirectory();

    /**
     * Returns the CDNStrategy for serving resources via CDN. This may return null, in which case no resources should
     * be served via CDN.
     *
     * @return CDN strategy
     */
    CDNStrategy getCDNStrategy();

    /**
     * @return the current user's locale. Must return a default locale if none is found.
     */
    Locale getLocale();

    /**
     * @return the set of locales supported by this application. Used to pre-bake resources.
     * @since 3.4.7
     */
    Iterable<Locale> getSupportedLocales();

    /**
     * Retrieve the unformatted message text associated with this key.
     *
     * @param locale locale in which to look up keys
     * @param key    key for the i18ned message
     * @return the unformatted message text if key is found for the given locale, otherwise returns key itself.
     */
    String getI18nRawText(Locale locale, String key);

    /**
     * Retrieve the message text associated with this key.
     *
     * @param locale locale in which to look up keys
     * @param key    key for the i18ned message
     * @return the message text if key is found for the given locale, otherwise returns key itself.
     */
    String getI18nText(Locale locale, String key);

    /**
     * Works together with `forbidCondition1AndTransformer1` and temporarily allows for given set of plugins to have
     * legacy stuff. Eventually when all plugins will be updated it will ratchet down to an empty list and will be
     * removed.
     *
     * @since 3.3.0
     */
    Set<String> allowedCondition1Keys();

    /**
     * Works together with `forbidCondition1AndTransformer1` and temporarily allows for given set of plugins to have
     * legacy stuff. Eventually when all plugins will be updated it will ratchet down to an empty list and will be
     * removed.
     *
     * @since 3.3.0
     */
    Set<String> allowedTransform1Keys();

    /**
     * If enabled and there's at leas one Condition1 or Transformer1 Web Resource will throw an error and stop working.
     * Should be enabled in on-demand only.
     *
     * @since 3.3.0
     */
    default boolean forbidCondition1AndTransformer1() {
        return false;
    }

    /**
     * If incremental cache should be used. It's the experimental feature that potentially could slow down
     * the instance and should be disabled by default.
     * <p>
     * Please note that if it's enabled the size of the cache should be big enough. It caches the
     * transformed content of the individual JS / CSS files, and there are lots of such files. The size defined by
     * `plugin.webresource.incrementalcache.size` system property.
     * <p>
     * See https://extranet.atlassian.com/display/~apetrushin/Incremental+build+Cache+for+Context+Batch for details.
     *
     * @since 3.4.0
     */
    default boolean isIncrementalCacheEnabled() {
        return false;
    }

    /**
     * CONFDEV-35445 Should WRM enable the defer attribute in its JS tags?
     * Experimental Api
     *
     * @since 3.4.0
     */
    default boolean isDeferJsAttributeEnabled() {
        return false;
    }

    default BigPipeConfiguration getBigPipeConfiguration() {
        return new DefaultBigPipeConfiguration();
    }

    /**
     * If enabled all javascript tags will be generated with `async` attribute.
     *
     * @since 3.5.11
     */
    @ExperimentalApi
    default boolean useAsyncAttributeForScripts() {
        return false;
    }

    /**
     * Synchronous resources will be served before `async` ones.
     * For example, analytics or AMD loader should be available for every other scripts.
     * Important note. Only JS files will be taken from specified web-resources and
     * other types of resources will be ignored. Warning will be logged in such case.
     * Any dependencies of specified web-resources will be also treated as sync and will be included
     * before those web-resources so be careful.
     * It's guaranteed that sync web-resources will be included only once, hence they will be also
     *  excluded from other batches.
     *
     * @return list of complete resource keys
     * @since 3.5.27
     */
    @ExperimentalApi
    default List<CompleteWebResourceKey> getSyncWebResourceKeys() {
        return new ArrayList<>();
    }

    /**
     * If AMD enabled.
     *
     * @since 3.4.9
     */
    default boolean amdEnabled() {
        return false;
    }

    /**
     * If true the plugin install time will be used instead of the plugin version for the SNAPSHOT plugins.
     * The plugin version is used when building the cache hash for URLs. for details see https://ecosystem.atlassian.net/browse/PLUGWEB-304
     * <p>
     * It's needed to simplify development and flush the cache when updated SNAPSHOT plugin installed.
     *
     * @since 3.5.9
     */
    default boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins() {
        return false;
    }

    /**
     * Default transformers that should be applied to AMD modules.
     *
     * @since 3.5.13
     */
    default List<TransformationDto> getDefaultAmdModuleTransformers() {
        List<TransformationDto> transformations = new ArrayList<>();

        // JS.
        List<TransformerDto> jsTransformers = new ArrayList<>();
        jsTransformers.add(new TransformerDto("jsI18n"));
        transformations.add(new TransformationDto("js", jsTransformers));

        // LESS.
        List<TransformerDto> lessTransformers = new ArrayList<>();
        lessTransformers.add(new TransformerDto("lessTransformer"));
        transformations.add(new TransformationDto("less", lessTransformers));

        // SOY.
        List<TransformerDto> soyTransformers = new ArrayList<>();
        soyTransformers.add(new TransformerDto("soyTransformer"));
        transformations.add(new TransformationDto("soy", soyTransformers));

        return transformations;
    }

    /**
     * Checks if CT-CDN is enable at this moment. Use this to disable this feature in case of problems.
     *
     * @return "True" enables CT-CDN and "false" disabled it at runtime.
     */
    default boolean isCtCdnMappingEnabled() {
        return false;
    }

    /**
     * Returns whether "compiled resources" are enabled. This will cause WRM to look for a 'compiledLocation' property
     * on a downloadable web resource. If the property is present, and this method returns true, then WRM will serve the
     * resource located on the path provided by 'compiledLocation' instead of at the path provided by 'location'.
     */
    default boolean isCompiledResourceEnabled() {
        return false;
    }
}
