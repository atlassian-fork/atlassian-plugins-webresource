package com.atlassian.plugin.webresource.assembler;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableBoolean;
import com.atlassian.json.marshal.wrapped.JsonableNumber;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.webresource.bigpipe.BigPipe;
import com.atlassian.plugin.webresource.bigpipe.KeyedValue;
import com.atlassian.plugin.webresource.data.DefaultPluginDataResource;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import com.atlassian.plugin.webresource.impl.helpers.UrlGenerationHelpers;
import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssembler;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.data.PluginDataResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import static com.atlassian.plugin.webresource.impl.config.Config.SUPERBATCH_KEY;
import static com.atlassian.plugin.webresource.impl.config.Config.nameAndLoaderToNameWithExtension;
import static com.atlassian.plugin.webresource.impl.config.Config.parseNameWithLoader;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;

/**
 * Implementation of WebResourceAssembler.
 *
 * @since v3.0
 */
class DefaultWebResourceAssembler implements PrebakeWebResourceAssembler {
    private static Logger LOG = LoggerFactory.getLogger(DefaultWebResourceAssembler.class);

    private final RequestState requestState;
    private final AssembledResources assembledResourcesStub;
    private final RequiredResources requiredResourcesStub;
    private final RequiredData requiredDataStub;
    private final Config config;
    private final Globals globals;

    public DefaultWebResourceAssembler(final RequestState requestState, final Globals globals) {
        this.requestState = requestState;
        this.config = globals.getConfig();
        this.globals = globals;

        assembledResourcesStub = new AssembledResources() {
            @Override
            public WebResourceSet drainIncludedResources() {
                return drainOrPoll(false);
            }

            @Override
            public WebResourceSet pollIncludedResources() {
                return drainOrPoll(true);
            }

            private WebResourceSet drainOrPoll(boolean blockOnBigPipe) {
                Tuple<DefaultWebResourceSet, LinkedHashSet<String>> resolved = resolve(true, blockOnBigPipe);
                requestState.clearIncludedAndUpdateExcluded(resolved.getLast());
                return resolved.getFirst();
            }

            @Override
            public WebResourceSet peek() {
                return resolve(false, false).getFirst();
            }

            private Tuple<DefaultWebResourceSet, LinkedHashSet<String>> resolve(boolean drainBigPipe, boolean blockOnBigPipe) {
                UrlGenerationHelpers.Resolved resolved = Helpers.resolve(requestState);

                List<PluginDataResource> pluginDataResources = new LinkedList<>();
                for (Map.Entry<String, Jsonable> entry : resolved.data.entrySet()) {
                    pluginDataResources.add(new DefaultPluginDataResource(entry.getKey(), entry.getValue()));
                }

                if (drainBigPipe) {
                    for (KeyedValue<String, Jsonable> keyedValue : drainBigPipe(blockOnBigPipe)) {
                        keyedValue.value().left().forEach(ex ->
                                LOG.error("Error generating bigpipe content for '" + keyedValue.key() + "': " + ex.getMessage(), ex));
                        Optional<Jsonable> v = keyedValue.value().isRight() ?
                                Optional.of(keyedValue.value().right().get()) :
                                Optional.empty();
                        pluginDataResources.add(new DefaultPluginDataResource(keyedValue.key(), v));
                    }
                }
                boolean pipeComplete = requestState.getBigPipe().isComplete();

                return new Tuple<>(
                        new DefaultWebResourceSet(requestState, pluginDataResources, resolved.urls, pipeComplete, config),
                        resolved.excludedResolved
                );
            }

            private Iterable<KeyedValue<String, Jsonable>> drainBigPipe(boolean blockOnBigPipe) {
                long waitForMs = requestState.getBigPipeDeadline() - System.currentTimeMillis();
                boolean exceededDeadline = waitForMs <= 0;
                BigPipe bigPipe = requestState.getBigPipe();

                //alternative route if Deadline is disabled
                if (globals.getConfig().getBigPipeDeadlineDisabled()) {
                    if (blockOnBigPipe && !bigPipe.isComplete()) // don't block if there's nothing there
                    {
                        try {
                            return bigPipe.waitForContent();
                        } catch (InterruptedException e) {
                            LOG.info("Interrupted while waiting for bigpipe", e);
                            return emptyList();
                        }
                    }
                    return bigPipe.getAvailableContent();
                }

                if (exceededDeadline) {
                    return bigPipe.forceCompleteAll();
                } else if (blockOnBigPipe && !bigPipe.isComplete()) // don't block if there's nothing there
                {
                    try {
                        return bigPipe.waitForContent(waitForMs, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        LOG.info("Interrupted while waiting for bigpipe", e);
                        return emptyList();
                    }
                } else {
                    return bigPipe.getAvailableContent();
                }
            }
        };

        requiredResourcesStub = new RequiredResources() {
            @Override
            public RequiredResources requireWebResource(String completeKey) {
                if (!Config.isWebResourceKey(completeKey)) {
                    Support.LOGGER.warn("requiring something that doesn't look like the web resource \"" + completeKey + "\", it will be ignored.");
                    return this;
                }
                requestState.getIncluded().add(completeKey);
                return this;
            }

            @Override
            public RequiredResources requireModule(String name) {
                if (Config.isWebResourceKey(name)) {
                    Support.LOGGER.warn("requiring web resource \"" + name + "\" as a module, it will be ignored.");
                    return this;
                }
                Tuple<String, String> loaderAndName = parseNameWithLoader(name);
                String nameWithExtension = nameAndLoaderToNameWithExtension(loaderAndName.getFirst(), loaderAndName.getLast());
                requestState.getIncluded().add(nameWithExtension);
                return this;
            }

            @Override
            public RequiredResources requireContext(String context) {
                requestState.getIncluded().add(Config.CONTEXT_PREFIX + ":" + context);
                return this;
            }

            @Override
            public RequiredResources exclude(Set<String> excludeWebResources, Set<String> excludeContexts) {
                excludeWebResources = null == excludeWebResources ? emptySet() : new HashSet<>(excludeWebResources);
                excludeContexts = null == excludeContexts ? new HashSet<>() : new HashSet<>(excludeContexts);

                LinkedHashSet<String> exclude = new LinkedHashSet<>();
                for (String context : excludeContexts) {
                    exclude.add(Config.CONTEXT_PREFIX + ":" + context);
                }
                exclude.addAll(excludeWebResources);


                /*
                  By default the superbatch is always included when a WebResourceAssemblerFactory
                  is created. If the superbatch has already been loaded so far (it is included in the excludeContexts)
                  remove it from the contexts that should be included
                 */
                if (excludeContexts.contains(SUPERBATCH_KEY)) {
                    requestState.getIncluded().remove(SUPERBATCH_KEY);
                }

                LinkedHashSet<String> excludedResolved = Helpers.resolveExcluded(
                        requestState.getCache(), requestState.getUrlStrategy(), new ArrayList<>(exclude), requestState.getExcluded());

                requestState.getExcluded().clear();
                requestState.getExcluded().addAll(excludedResolved);

                return this;
            }

            @Override
            public RequiredResources requirePage(String key) {
                RootPage rp = globals.getSnapshot().getRootPage(key);
                if (rp == null) {
                    throw new IllegalArgumentException("Root page '" + key + "' does not exist!");
                }
                requestState.getIncluded().addAll(rp.getWebResource().getDependencies());
                return this;
            }
        };

        requiredDataStub = new RequiredData() {
            @Override
            public RequiredData requireData(String key, Jsonable content) {
                requestState.getIncludedData().put(key, content);
                return this;
            }

            @Override
            public RequiredData requireData(String key, Number content) {
                requestState.getIncludedData().put(key, new JsonableNumber(content));
                return this;
            }

            @Override
            public RequiredData requireData(String key, String content) {
                requestState.getIncludedData().put(key, new JsonableString(content));
                return this;
            }

            @Override
            public RequiredData requireData(String key, Boolean content) {
                requestState.getIncludedData().put(key, new JsonableBoolean(content));
                return this;
            }

            @Override
            public RequiredData requireData(String key, CompletionStage<Jsonable> content) {
                requestState.getBigPipe().push(key, content);
                return this;
            }
        };
    }

    @Override
    public AssembledResources assembled() {
        return assembledResourcesStub;
    }

    @Override
    public RequiredResources resources() {
        return requiredResourcesStub;
    }

    @Override
    public RequiredData data() {
        return requiredDataStub;
    }

    @Override
    public WebResourceAssembler copy() {
        return new DefaultWebResourceAssembler(requestState.deepClone(), globals);
    }
}
