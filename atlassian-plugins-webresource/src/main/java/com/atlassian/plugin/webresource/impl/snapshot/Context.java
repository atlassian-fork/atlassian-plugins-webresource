package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.config.Config;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Context.
 *
 * @since v3.4.4
 */
public class Context extends Bundle {
    private final List<WebModule> jsModulesDependencies;

    public Context(Snapshot snapshot, String key, List<String> dependencies, List<String> moduleDependencies,
                   Date updatedAt, String version, boolean isTransformable) {
        super(snapshot, key, dependencies, updatedAt, version, isTransformable);

        // Calculating list of JS module dependencies for this context. It's needed to automatically require it.
        jsModulesDependencies = new ArrayList<>();
        for (String name : moduleDependencies) {
            Bundle bundle = getSnapshot().get(name);
            if (bundle != null && (bundle instanceof WebModule)) {
                WebModule module = (WebModule) bundle;
                if (Config.JS_TYPE.equals(module.getResource().getNameOrLocationType())) {
                    jsModulesDependencies.add(module);
                }
            }
        }
    }

    @Override
    public LinkedHashMap<String, Resource> getResources(RequestCache cache) {
        LinkedHashMap<String, Resource> resources = new LinkedHashMap<>();
        if (!jsModulesDependencies.isEmpty()) {
            Resource resource = new ContextResource(this);
            resources.put(resource.getName(), resource);
        }
        return resources;
    }

    /**
     * Get list of JS AMD modules dependencies for this context (modules that should be injected in this context).
     */
    public List<WebModule> getJsModulesDependencies() {
        return jsModulesDependencies;
    }
}
