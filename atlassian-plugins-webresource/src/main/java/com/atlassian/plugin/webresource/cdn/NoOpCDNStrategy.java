package com.atlassian.plugin.webresource.cdn;

import com.atlassian.plugin.webresource.prebake.PrebakeConfig;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * CDNStrategy that does nothing. Could be used in {@link com.atlassian.plugin.webresource.WebResourceIntegration#getCDNStrategy()}
 * when there's no custom strategy and yet you want map URLs using {@link com.atlassian.plugin.webresource.cdn.mapper.WebResourceMapper}.
 *
 * @since v3.5.0
 */
public class NoOpCDNStrategy implements CDNStrategy {
    private final Optional<PrebakeConfig> prebakeConfig;

    public NoOpCDNStrategy(@Nonnull final Optional<PrebakeConfig> prebakeConfig) {
        this.prebakeConfig = prebakeConfig;
    }

    @Override
    public boolean supportsCdn() {
        return true;
    }

    @Override
    public String transformRelativeUrl(final String s) {
        return s;
    }

    @Override
    public Optional<PrebakeConfig> getPrebakeConfig() {
        return prebakeConfig;
    }
}
