package com.atlassian.plugin.webresource.assembler;

/**
 * Utilities for converting between {@link com.atlassian.plugin.webresource.UrlMode} and
 * {@link com.atlassian.webresource.api.UrlMode}
 * @since v3.0
 */
public class UrlModeUtils
{
    public static com.atlassian.webresource.api.UrlMode convert(com.atlassian.plugin.webresource.UrlMode urlMode)
    {
        switch (urlMode)
        {
            case ABSOLUTE:
                return com.atlassian.webresource.api.UrlMode.ABSOLUTE;
            case RELATIVE:
                return com.atlassian.webresource.api.UrlMode.RELATIVE;
            case AUTO:
                return com.atlassian.webresource.api.UrlMode.AUTO;
            default:
                throw new IllegalArgumentException("Unrecognised UrlMode: " + urlMode);
        }
    }
}
