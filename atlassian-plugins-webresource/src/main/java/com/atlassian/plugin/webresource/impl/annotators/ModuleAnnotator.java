package com.atlassian.plugin.webresource.impl.annotators;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.ModuleResource;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.WebModule;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Wraps AMD module inside of `WRM.atlassianModule`, needed by AMD loader to resolve dependencies.
 *
 * @since v6.3
 */
public class ModuleAnnotator extends ResourceContentAnnotator {
    @Override
    public int beforeResourceInBatch(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        return before(requiredResources, resource, params, stream);
    }

    @Override
    public int beforeResource(LinkedHashSet<String> requiredResources, String url, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        return before(requiredResources, resource, params, stream);
    }

    @Override
    public void afterResourceInBatch(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        after(requiredResources, resource, params, stream);
    }

    @Override
    public void afterResource(LinkedHashSet<String> requiredResources, String url, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        after(requiredResources, resource, params, stream);
    }

    protected int before(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        if (!(resource instanceof ModuleResource)) {
            return 0;
        }
        WebModule module = (WebModule) resource.getParent();
        if (Config.SOY_TYPE.equals(module.getSrcType())) {
            stream.write(("define(\"" + module.getKey() + "\", function(){ return " + module.getSoyNamespace() + "; });\n").getBytes());
            return 1;
        } else {
            stream.write(("\nWRM.atlassianModule(\"" + module.getKey() + "\"").getBytes());
            stream.write(", {".getBytes());
            boolean isFirst = true;
            for (Map.Entry<String, String> entry : module.getUnresolvedDependencies().entrySet()) {
                // Skipping if dependency is the same as resolved dependency to minimize the size of batch.
                if (isFirst) {
                    isFirst = false;
                } else {
                    stream.write(", ".getBytes());
                }
                stream.write("\"".getBytes());
                stream.write(entry.getKey().getBytes());
                stream.write("\": \"".getBytes());
                stream.write(entry.getValue().getBytes());
                stream.write("\"".getBytes());
            }
            stream.write("}, function(define) {\n".getBytes());
            return 1;
        }
    }

    protected void after(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException {
        if (resource instanceof ModuleResource) {
            WebModule module = (WebModule) resource.getParent();

            // SOY modules should not be wrapped.
            if (!Config.SOY_TYPE.equals(module.getSrcType())) {
                stream.write("\n});\n".getBytes());
            }

            // Resolving immediately if it's explicitly required. Checking for both name and base name.
            if (requiredResources.contains(module.getKey()) || requiredResources.contains(module.getBaseName())) {
                stream.write(("require([\"" + module.getKey() + "\"], function(){});\n").getBytes());
            }
        }
    }

    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}