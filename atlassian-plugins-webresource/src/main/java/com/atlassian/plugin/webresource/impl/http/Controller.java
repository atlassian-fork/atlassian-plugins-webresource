package com.atlassian.plugin.webresource.impl.http;

import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.cache.filecache.impl.PassThroughCache;
import com.atlassian.plugin.servlet.util.LastModifiedHandler;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.LineCountingProxyOutputStream;
import com.atlassian.plugin.webresource.impl.support.NullOutputStream;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.impl.support.http.BaseController;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Util;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.net.HttpHeaders;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.hasLegacyCondition;
import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.isConditionsSatisfied;
import static com.atlassian.plugin.webresource.impl.helpers.Helpers.transform;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.getResource;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.shouldBeIncludedInBatch;
import static com.atlassian.plugin.webresource.impl.http.Router.sourceMapUrlToUrl;
import static com.atlassian.plugin.webresource.impl.support.Support.copy;
import static com.atlassian.sourcemap.Util.generateSourceMapComment;
import static com.google.common.base.Predicates.not;
import static java.util.Collections.emptyMap;

/**
 * Handles HTTP requests.
 *
 * @since 3.3
 */
public class Controller extends BaseController {
    private final RequestCache requestCache;
    public static final String APPLICATION_JSON = "application/json";

    public Controller(Globals globals, Request request, Response response) {
        super(globals, request, response);
        this.requestCache = new RequestCache(globals);
    }

    /**
     * Serves single Resource.
     */
    public void serveResource(String completeKey, String resourceName) {
        LinkedHashSet<String> requiredResources = new LinkedHashSet<>();
        requiredResources.add(completeKey);
        serveResource(requiredResources, getResource(requestCache, completeKey, resourceName), true, false);
    }

    /**
     * Serves Source Map for single Resource.
     */
    public void serveResourceSourceMap(String completeKey, String resourceName) {
        LinkedHashSet<String> requiredResources = new LinkedHashSet<>();
        requiredResources.add(completeKey);
        serveSourceMap(requiredResources, getResource(requestCache, completeKey, resourceName), false);
    }

    /**
     * Serves Batch (all types of Batch - Web Resource Batch, Super Batch, Context Batch).
     *
     * @param included            keys of included Web Resources.
     * @param excluded            keys of excluded Web Resources.
     * @param type                type of Batch.
     * @param resolveDependencies if dependencies should be resolved.
     */
    public void serveBatch(final Collection<String> included, final LinkedHashSet<String> excluded, final String type,
                           final boolean resolveDependencies, final boolean withLegacyConditions, boolean isCachingEnabled) {
        serveResources(new LinkedHashSet<>(included), new Supplier<Collection<Resource>>() {
            @Override
            public Collection<Resource> get() {
                return getBatchResources(included, excluded, type, resolveDependencies, withLegacyConditions);
            }
        }, isCachingEnabled);
    }

    /**
     * Serves Source Map for Batch.
     */
    public void serveBatchSourceMap(final Collection<String> included, final LinkedHashSet<String> excluded,
                                    final String type, final boolean resolveDependencies, final boolean withLegacyConditions) {
        serveResourcesSourceMap(new LinkedHashSet<>(included), new Supplier<Collection<Resource>>() {
            @Override
            public Collection<Resource> get() {
                return getBatchResources(included, excluded, type, resolveDependencies, withLegacyConditions);
            }
        });
    }

    /**
     * Get Resources for Batch (all types of Batch - Web Resource Batch, Super Batch, Context Batch).
     *
     * @param included            keys of included Web Resources.
     * @param excluded            keys of excluded Web Resources.
     * @param type                type of Batch.
     * @param resolveDependencies if dependencies should be resolved.
     */
    protected List<Resource> getBatchResources(Collection<String> included, LinkedHashSet<String> excluded,
                                               String type, boolean resolveDependencies, boolean withLegacyConditions)

    {
        // Exclude sync resources
        LinkedHashSet<String> excludedAndSync = new LinkedHashSet<>(excluded);
        Bundle syncContext = requestCache.getSnapshot().get(Config.SYNCBATCH_KEY);
        if (null != syncContext) {
            excludedAndSync.addAll(syncContext.getDependencies());
        }

        return requestCache.getSnapshot().find()
                .included(included)
                .excluded(excludedAndSync, isConditionsSatisfied(requestCache, request.getParams()))
                .deep(resolveDependencies)
                .deepFilter(isConditionsSatisfied(requestCache, request.getParams()))
                .deepFilter(withLegacyConditions ? Predicates.alwaysTrue() : not(hasLegacyCondition()))
                .resources(requestCache)
                .filter(shouldBeIncludedInBatch(type, request.getParams()))
                .end();
    }

    /**
     * Serves Resource relative to Batch.
     */
    public void serveResourceRelativeToBatch(Collection<String> included, LinkedHashSet<String> excluded,
                                             String resourceName, boolean resolveDependencies, boolean withLegacyConditions) {
        serveResource(new LinkedHashSet<>(included), getResourceRelativeToBatch(included, excluded, resourceName, resolveDependencies,
                withLegacyConditions), true, false);
    }

    /**
     * Serves Source Map for Resource relative to Batch.
     */
    public void serveResourceRelativeToBatchSourceMap(Collection<String> included, LinkedHashSet<String> excluded,
                                                      String resourceName, boolean resolveDependencies, boolean withLegacyConditions) {
        serveSourceMap(new LinkedHashSet<>(included), getResourceRelativeToBatch(included, excluded, resourceName, resolveDependencies,
                withLegacyConditions), false);
    }

    /**
     * Get Resource relative to Batch.
     */
    protected Resource getResourceRelativeToBatch(Collection<String> included, LinkedHashSet<String> excluded,
                                                  String resourceName, boolean resolveDependencies, boolean withLegacyConditions)

    {
        List<String> bundles = requestCache.getSnapshot().find()
                .included(included)
                .excluded(excluded, isConditionsSatisfied(requestCache, request.getParams()))
                .deep(resolveDependencies)
                .deepFilter(isConditionsSatisfied(requestCache, request.getParams()))
                .deepFilter(withLegacyConditions ? Predicates.alwaysTrue() : not(hasLegacyCondition()))
                .end();
        return getResource(requestCache, bundles, resourceName);
    }

    /**
     * Serves Source Code of the Resource.
     */
    public void serveSource(final String completeKey, String resourceName) {
        if (Resource.isPrebuiltSourceName(resourceName)) {
            resourceName = Resource.getResourceNameFromPrebuiltSourceName(resourceName);
        }

        final Resource resource = getResource(requestCache, completeKey, resourceName);
        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }

        sendCached(new ContentImpl(resource.getContentType(), false) {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                String prebuildSourceName = Resource.getPrebuiltSourcePath(resource.getLocation());
                InputStream sourceStream = resource.getStreamFor(prebuildSourceName);
                if (sourceStream != null) {
                    copy(sourceStream, out);
                } else {
                    resource.getContent().writeTo(out, isSourceMapEnabled);
                }
                return null;
            }
        }, resource.getParams(), false);
    }

    /*
     * Helpers.
     */

    /**
     * Serves Resource.
     *
     * @param applyTransformations if transformations should be applied.
     */
    protected void serveResource(LinkedHashSet<String> requiredResources, Resource resource, boolean applyTransformations, boolean isCachingEnabled) {
        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }
        Content content = applyTransformations ? transform(globals, requiredResources, request.getUrl(), resource, request.getParams(), true)
                : resource.getContent();
        sendCached(content, resource.getParams(), isCachingEnabled);
    }

    /**
     * Serves resources.
     */
    protected void serveResources(LinkedHashSet<String> requiredResources, Supplier<Collection<Resource>> resources, boolean isCachingEnabled) {
        String type = request.getType();
        Content content = transform(globals, requiredResources, request.getUrl(), type, resources, request.getParams());
        sendCached(content, emptyMap(), isCachingEnabled);
    }

    /**
     * Serves Source Map.
     */
    protected void serveSourceMap(LinkedHashSet<String> requiredResources, Resource resource, boolean isCachingEnabled) {
        if (handleNotFoundRedirectAndNotModified(resource)) {
            return;
        }
        Content content = transform(globals, requiredResources, sourceMapUrlToUrl(request.getUrl()), resource, request.getParams(), true);
        sendCached(content, resource.getParams(), isCachingEnabled);
    }

    /**
     * Serves Source Map for Batch of Resources.
     */
    private void serveResourcesSourceMap(LinkedHashSet<String> requiredResources, Supplier<Collection<Resource>> resources) {
        String resourcePath = sourceMapUrlToUrl(request.getPath());
        String type = Request.getType(resourcePath);
        Content content = transform(globals, requiredResources, sourceMapUrlToUrl(request.getUrl()), type, resources, request.getParams());
        sendCached(content, emptyMap(), true);
    }

    /**
     * Handle not found resources, redirects and not modified resources.
     *
     * @return true if request has been handled and no further processing needed.
     */
    protected boolean handleNotFoundRedirectAndNotModified(Resource resource) {
        if (resource == null) {
            response.sendError(404);
            return true;
        }
        if (checkIfCachedAndNotModified(resource.getParent().getUpdatedAt())) {
            return true;
        }
        if (resource.isRedirect()) {
            response.sendRedirect(resource.getLocation(), resource.getContentType());
            return true;
        }
        return false;
    }

    /**
     * Check if resource is not modified and replies with not-modified response if so.
     *
     * @param updatedAt when resource has been updated.
     * @return true if Resources is not modified and no further processing is needed.
     */
    protected boolean checkIfCachedAndNotModified(Date updatedAt) {
        LastModifiedHandler lastModifiedHandler = new LastModifiedHandler(updatedAt);
        return request.isCacheable() && lastModifiedHandler.checkRequest(request.getOriginalRequest(),
                response.getOriginalResponse());
    }

    protected void sendCached(Content content, Map<String, String> params, boolean isCachingEnabled) {
        if (Boolean.TRUE.toString().equals(params.get(Config.ALLOW_PUBLIC_USE_PARAM_NAME))) {
            response.getOriginalResponse().addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        }
        if (isSourceMapEnabled() && content.isTransformed() && globals.getConfig().optimiseSourceMapsForDevelopment()) {
            sendCachedInDevelopment(content, isCachingEnabled);
        } else {
            sendCachedInProduction(content, isCachingEnabled);
        }
    }

    protected void sendCachedInProduction(final Content content, boolean isCachingEnabled) {
        String contentType = content.getContentType() != null ? content.getContentType() : request.getContentType();
        response.setContentTypeIfNotBlank(contentType);

        Cache cache = isCachingEnabled && request.isCacheable() ? globals.getContentCache() : new PassThroughCache();
        String cacheKey = buildCacheKey(request.getUrl());

        if (Router.isSourceMap(request)) {
            // Serving Source Map.
            if (globals.getConfig().isSourceMapEnabled()) {
                cache.cache("http", cacheKey, response.getOutputStream(), new Cache.StreamProvider() {
                    @Override
                    public void write(OutputStream producerOut) {
                        LineCountingProxyOutputStream lineCountingStream = new LineCountingProxyOutputStream(new NullOutputStream());
                        SourceMap sourceMap = content.writeTo(lineCountingStream, true);

                        // For some resources source map could not be generated, in such case generating 1 to 1 source map.
                        if (sourceMap == null) {
                            String resourceUrl = sourceMapUrlToUrl(request.getUrl());
                            sourceMap = Util.create1to1SourceMap(lineCountingStream.getLinesCount(), resourceUrl);
                        }
                        try {
                            producerOut.write(sourceMap.generate().getBytes());
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            } else {
                response.sendError(503);
            }
        } else {
            // Serving Resources.
            cache.cache("http", cacheKey, response.getOutputStream(), producerOut -> content.writeTo(producerOut, false));
            if (isSourceMapEnabled() && content.isTransformed()) {
                String sourceMapUrl = globals.getRouter().sourceMapUrl(request.getPath(), request.getParams());
                try {
                    response.getOutputStream().write(("\n" + generateSourceMapComment(sourceMapUrl, contentType)).getBytes());
                } catch (RuntimeException | IOException e) {
                    Support.LOGGER.error("can't generate source map comment", e);
                }
            }
        }
    }

    protected void sendCachedInDevelopment(final Content content, boolean isCachingEnabled) {
        // Setting content type.
        String contentType = content.getContentType() != null ? content.getContentType() : request.getContentType();
        response.setContentTypeIfNotBlank(contentType);

        Cache cache = isCachingEnabled && request.isCacheable() ? globals.getContentCache() : new PassThroughCache();

        // Detecting content type of the resource.
        final String resourceContentType;
        String cacheKey;
        if (Router.isSourceMap(request)) {
            String resourcePath = sourceMapUrlToUrl(request.getPath());
            resourceContentType = content.getContentType() != null ? content.getContentType() : globals
                    .getConfig().getContentType(resourcePath);
            cacheKey = buildCacheKey(sourceMapUrlToUrl(request.getUrl()));
        } else {
            resourceContentType = contentType;
            cacheKey = buildCacheKey(request.getUrl());
        }

        // Using two stream provider to cache both resource and its source map.
        Cache.TwoStreamProvider twoStreamProvider = new Cache.TwoStreamProvider() {
            @Override
            public void write(OutputStream out1, OutputStream out2) {
                LineCountingProxyOutputStream lineCountingStream = new LineCountingProxyOutputStream(out1);
                SourceMap sourceMap = content.writeTo(lineCountingStream, true);

                String sourceMapUrl = globals.getRouter().sourceMapUrl(request.getPath(), request.getParams());
                try {
                    out1.write(("\n" + generateSourceMapComment(sourceMapUrl, resourceContentType)).getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                // For some resources source map could not be generated, in such case generating 1 to 1 source map.
                if (sourceMap == null) {
                    String resourceUrl = sourceMapUrlToUrl(request.getUrl());
                    sourceMap = Util.create1to1SourceMap(lineCountingStream.getLinesCount(), resourceUrl);
                }
                try {
                    out2.write(sourceMap.generate().getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        // Sending data to browser.
        OutputStream out = response.getOutputStream();
        if (Router.isSourceMap(request)) {
            cache.cacheTwo("http", cacheKey, null, out, twoStreamProvider);
        } else {
            cache.cacheTwo("http", cacheKey, out, null, twoStreamProvider);
        }
    }

    /**
     * Creates cache key for resource or batch.
     */
    protected String buildCacheKey(String url) {
        // Currently minified and non-minified resources use same url, it would be better to use different urls.
        return url + "&_isMinificationEnabled=" + globals.getConfig().isMinificationEnabled();
    }

    /**
     * If Source Map is enabled for the current Request.
     */
    protected boolean isSourceMapEnabled() {
        // The `isSourceMapEnabled` used to choose single stream or double stream cache,
        // and as soon the cache should be chosen
        // before of the resolution of the resource, it can't use any data from resource (so it can't use `resource
        // .getType()`).
        // The decision should be based purely on data available in the request.
        String resourcePath = "map".equals(request.getType()) ? sourceMapUrlToUrl(request.getPath()) : request.getPath();
        String type = Request.getType(resourcePath);
        return globals.getConfig().isSourceMapEnabledFor(type);
    }
}
