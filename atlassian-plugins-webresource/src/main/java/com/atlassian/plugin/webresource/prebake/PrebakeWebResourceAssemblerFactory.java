package com.atlassian.plugin.webresource.prebake;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.Dimensions;

/**
 * A WebResourceAssemblerFactory that exposes prebake capabilities
 * (also see {@link com.atlassian.webresource.api.prebake}).
 * <p>
 * This factory provides direct capability to produce all possible dimensions via {@link #computeDimensions()}.
 * For any given coordinate within those dimensions, you can create a {@link PrebakeWebResourceAssembler}
 * locked to that coordinate via {@link PrebakeWebResourceAssemblerBuilder#withCoordinate(Coordinate)}.
 * Subsequent calls to requireResource()/requireContext()/drain()/poll() will deal with the given coordinate,
 * and not the thread-local behaviour a condition/transform would normally have.
 * </p>
 * <p>
 * If a condition or transform is encountered that is not dimension-aware, then the attempt
 * to prebake that resource will fail, and this can be detected with {@link PluginUrlResource#getPrebakeErrors()}.
 * This should be checked on each webresource after each call to drain() or poll().
 * </p>
 *
 * @since v3.5.0
 */
public interface PrebakeWebResourceAssemblerFactory extends WebResourceAssemblerFactory {
    @Override
    PrebakeWebResourceAssemblerBuilder create();

    Dimensions computeDimensions();

    /**
     * @param bundle for which you want to get dimensions.
     * @return {@link Dimensions} related to given bundle.
     * @since 3.5.22
     */
    Dimensions computeBundleDimensions(Bundle bundle);

    /**
     * Produce a hash that indicates the "state" of the WRM. Two WRMs in two different
     * JVMs will produce the same state hash if they would produce the same pre-bake.
     */
    String computeGlobalStateHash();
}
