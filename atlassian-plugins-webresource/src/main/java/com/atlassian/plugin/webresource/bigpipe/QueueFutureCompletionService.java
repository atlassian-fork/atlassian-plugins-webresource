package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.plugin.webresource.util.ConsList;
import com.google.common.collect.Iterables;

import javax.annotation.concurrent.GuardedBy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Collections.newSetFromMap;

/**
 * Implementation of FutureCompletionService that uses an internal {@link BlockingQueue}
 *
 * @since v3.3
 */
class QueueFutureCompletionService<K, T> implements FutureCompletionService<K, T> {

    /**
     * Guards against current read/write access to completionQueue and pendingPromises.
     * We do not want spurious isComplete() results:
     * - we write to both completionQueue and pendingPromises when a promise completes
     * - we read from both when we check isComplete()
     * - there's no ordering of that will guarantee isComplete() never returns spurious results
     *   without a lock
     *
     * A read/write lock could be used instead of a synchronized object, but we do not
     * expect a lot of concurrent readers at this point, so contention will be low for readers
     * (and writes need to be mutually exclusive)
     */
    private final Object lock = new Object();

    @GuardedBy("lock")
    private final BlockingQueue<KeyedValue<K, T>> completionQueue = new LinkedBlockingQueue<>();
    @GuardedBy("lock")
    private final PendingPromises<K> pendingPromises = new PendingPromises<>();

    private final Set<K> completeKeys = newSetFromMap(new ConcurrentHashMap<>());

    @Override
    public void add(final K key, final CompletionStage<T> promise) {
        pendingPromises.add(key);
        promise.whenComplete((result, throwable) -> {
            if (result != null) {
                complete(KeyedValue.success(key, result));
            } else {
                complete(KeyedValue.fail(key, throwable));
            }
        });
    }

    @Override
    public void forceCompleteAll() {
        Exception t = new RuntimeException("Deadline exceeded");
        for (K key : pendingPromises) {
            complete(KeyedValue.fail(key, t));
        }
    }

    @Override
    public void waitAnyPendingToComplete() throws InterruptedException {
        synchronized (lock) {
            while (completionQueue.isEmpty() && !pendingPromises.isEmpty()) {
                lock.wait(60000);
            }
        }
    }

    @Override
    public boolean isComplete() {
        synchronized (lock) // lock so ordering is consistent with complete()
        {
            return pendingPromises.isEmpty() && completionQueue.isEmpty();
        }
    }

    private void complete(KeyedValue<K, T> value) {
        // A promise could be complete twice (potentially concurrently) due to
        // forceCompleteAll(), so guard against that using completeKeys.
        if (completeKeys.add(value.key())) {
            synchronized (lock) // lock so ordering is consistent with isComplete()
            {
                completionQueue.add(value);
                pendingPromises.remove(value.key());
                lock.notifyAll();
            }
        }
    }

    @Override
    public Iterable<KeyedValue<K, T>> poll() {
        return getResult(completionQueue.poll());
    }

    @Override
    public Iterable<KeyedValue<K, T>> poll(long timeout, TimeUnit unit) throws InterruptedException {
        return getResult(completionQueue.poll(timeout, unit));
    }

    private Iterable<KeyedValue<K, T>> getResult(KeyedValue<K, T> promise) {
        List<KeyedValue<K, T>> results = new LinkedList<>();
        if (null != promise) {
            results.add(promise);
        }
        completionQueue.drainTo(results);
        return results;
    }

    private static final class PendingPromises<A> implements Iterable<A> {
        private final AtomicReference<ConsList<A>> promises = new AtomicReference<>(ConsList.empty());

        /**
         * Adds a new promise to the list of pending promises.
         *
         * @param p promise to add
         */
        void add(final A p) {
            promises.updateAndGet(input -> input.prepend(p));
        }

        /**
         * Removes the given promise from the list of pending promises.
         *
         * @param p promise to remove
         */
        void remove(final A p) {
            promises.updateAndGet(input -> input.remove(p));
        }

        /**
         * @return true if there are promises remaining.
         */
        boolean isEmpty() {
            return Iterables.isEmpty(this);
        }

        /**
         * Returns a stable iterator, a snapshot at the time of the call. You can modify
         * this object and a returned iterator will still be stable
         */
        @Override
        public Iterator<A> iterator() {
            return promises.get().iterator();
        }
    }
}
