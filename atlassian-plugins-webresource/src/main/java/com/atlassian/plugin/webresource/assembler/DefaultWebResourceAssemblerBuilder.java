package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssembler;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerBuilder;
import com.atlassian.plugin.webresource.util.TimeSpan;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of WebResourceAssemblerBuilder
 *
 * @since v3.0
 */
public class DefaultWebResourceAssemblerBuilder implements PrebakeWebResourceAssemblerBuilder {
    private final Globals globals;
    // Using optional to determine whether includeSuperbatchResources was invoked or not
    private Optional<Boolean> isSuperBatchingEnabled = Optional.empty();
    private Optional<TimeSpan> deadline = Optional.empty();
    private Optional<Coordinate> coord = Optional.empty();

    public DefaultWebResourceAssemblerBuilder(final Globals globals) {
        this.globals = globals;
    }

    @Override
    public PrebakeWebResourceAssemblerBuilder withCoordinate(Coordinate coord) {
        this.coord = Optional.of(coord);
        return this;
    }

    @Override
    public PrebakeWebResourceAssembler build() {
        Config config = globals.getConfig();
        UrlBuildingStrategy urlStrat = UrlBuildingStrategy.from(coord);
        RequestState requestState = new RequestState(globals, urlStrat);
        DefaultWebResourceAssembler assembler = new DefaultWebResourceAssembler(requestState, globals);

        // flush sync resources
        boolean resourcesAdded = false;
        if (config.useAsyncAttributeForScripts()) {
            assembler.resources().requireWebResource(Config.IN_ORDER_LOADER_RESOURCE_KEY);
            resourcesAdded = true;
        }
        if (config.isSyncContextCreated()) {
            assembler.resources().requireContext(Config.SYNCBATCH_CONTEXT_KEY);
            resourcesAdded = true;
        }
        if (resourcesAdded) {
            requestState.setSyncResourceSet((DefaultWebResourceSet) assembler.assembled().drainIncludedResources());
        }

        //
        if (deadline.isPresent()) {
            requestState.setBigPipeDeadline(System.currentTimeMillis() + deadline.get().toMillis());
        }

        boolean includeSuperbatch = config.isSuperBatchingEnabled();
        // Override previous superbatch settings if the user explicitly called includeSuperbatchResources
        if (isSuperBatchingEnabled.isPresent()) {
            includeSuperbatch = isSuperBatchingEnabled.get();
        }

        if (includeSuperbatch) {
            assembler.resources().requireContext(Config.SUPER_BATCH_CONTEXT_KEY);
        }
        return assembler;
    }

    @Override
    public WebResourceAssemblerBuilder includeSuperbatchResources(boolean include) {
        this.isSuperBatchingEnabled = Optional.of(include);
        return this;
    }

    @Override
    public WebResourceAssemblerBuilder asyncDataDeadline(long deadline, TimeUnit timeunit) {
        this.deadline = Optional.of(new TimeSpan(deadline, timeunit));
        return this;
    }
}
