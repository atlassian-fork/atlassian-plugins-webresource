package com.atlassian.plugin.webresource.impl;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceSet;
import com.atlassian.plugin.webresource.bigpipe.BigPipe;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.webresource.api.assembler.WebResourceSet;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.join;

/**
 * State maintained during single request.
 *
 * @since 3.3
 */
public class RequestState {
    protected final LinkedHashSet<String> included;
    protected final LinkedHashSet<String> excluded;
    protected final LinkedHashMap<String, Jsonable> includedData;
    protected final Set<String> excludedData;
    protected final RequestCache cache;
    private final UrlBuildingStrategy urlBuildingStrategy;
    /**
     * this is the ms-since-epoc that the dealine expires
     */
    private volatile long bigPipDeadline;
    /**
     * request-local
     */
    private final BigPipe pipe = new BigPipe();
    private Optional<DefaultWebResourceSet> syncResourceSet = Optional.empty();

    protected final Globals globals;

    private RequestState(Globals globals, LinkedHashSet<String> included, LinkedHashSet<String> excluded,
                         LinkedHashMap<String, Jsonable> includedData, Set<String> excludedData, RequestCache cache,
                         UrlBuildingStrategy urlBuildingStrategy, DefaultWebResourceSet syncResourceSet) {
        this.globals = globals;
        this.included = included;
        this.excluded = excluded;
        this.includedData = includedData;
        this.excludedData = excludedData;
        this.cache = cache;
        this.urlBuildingStrategy = urlBuildingStrategy;
        this.bigPipDeadline = System.currentTimeMillis() + globals.getConfig().getDefaultBigPipeDeadline().toMillis();

        if (null != syncResourceSet) {
            this.syncResourceSet = Optional.of(syncResourceSet);
        }
    }

    public RequestState(Globals globals, UrlBuildingStrategy urlBuildingStrategy) {
        this(globals, new LinkedHashSet<>(), new LinkedHashSet<>(), new LinkedHashMap<>(),
                new HashSet<>(), new RequestCache(globals), urlBuildingStrategy, null);
    }

    public UrlBuildingStrategy getUrlStrategy() {
        return urlBuildingStrategy;
    }

    public RequestState deepClone() {
        RequestState clone = new RequestState(globals, new LinkedHashSet<>(included), new LinkedHashSet<>(excluded),
                new LinkedHashMap<>(includedData), new HashSet<>(excludedData), cache, urlBuildingStrategy,
                syncResourceSet.orElse(null));
        clone.setBigPipeDeadline(bigPipDeadline);
        return clone;
    }

    public void setBigPipeDeadline(long deadline) {
        this.bigPipDeadline = deadline;
    }

    public long getBigPipeDeadline() {
        return bigPipDeadline;
    }

    public LinkedHashSet<String> getIncluded() {
        return included;
    }

    public LinkedHashMap<String, Jsonable> getIncludedData() {
        return includedData;
    }

    public Set<String> getExcludedData() {
        return excludedData;
    }

    public LinkedHashSet<String> getExcluded() {
        return excluded;
    }

    public BigPipe getBigPipe() {
        return pipe;
    }

    /**
     * Called after generating urls, to clear current state and remember already included resources.
     */
    public void clearIncludedAndUpdateExcluded(LinkedHashSet<String> excludedResolved) {
        included.clear();
        excluded.addAll(excludedResolved);

        excludedData.addAll(includedData.keySet());
        includedData.clear();
    }

    public Globals getGlobals() {
        return globals;
    }

    /**
     * Get all bundles.
     * <p>
     * It is another layer of cache over the `globals.getBundles()` because it is used very heavily, to avoid any
     * performance drawbacks of atomic reference in the `globals.getBundles()`.
     */
    public Snapshot getSnapshot() {
        return getCache().getSnapshot();
    }

    public RequestCache getCache() {
        return cache;
    }

    public String toString() {
        return "[" + join(included, ", ") + "] - [" + join(excluded, ", ") + "]";
    }

    /**
     * @return initial set of sync resources which should be written at the top of the page
     * @since 3.5.27
     */
    public Optional<DefaultWebResourceSet> getSyncResourceSet() {
        return syncResourceSet;
    }

    /**
     * Set the initial set of sync resources which should be written at the top of the page
     *
     * @since 3.5.27
     */
    public void setSyncResourceSet(DefaultWebResourceSet set) {
        syncResourceSet = Optional.of(set);
    }

    /**
     * Mark that the initial set of sync resources was already written into page
     *
     * @since 3.5.27
     */
    public void markSyncResourcesAsWritten() {
        syncResourceSet = Optional.empty();
    }
}