package com.atlassian.plugin.webresource.bigpipe;

import io.atlassian.fugue.Either;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a successful or failed invocation of an an asynchronous task.
 * @param <V> type of successful values
 * @since 3.3
 */
public class KeyedValue<K, V>
{
    private final K key;
    private final Either<Throwable, V> value;

    public static <K, T> KeyedValue<K, T> success(K key, T value)
    {
        return new KeyedValue<>(key, Either.right(value));
    }

    public static <K, T> KeyedValue<K, T> fail(K key, Throwable ex)
    {
        return new KeyedValue<>(key, Either.left(ex));
    }

    private KeyedValue(K key, Either<Throwable, V> value)
    {
        this.key = checkNotNull(key);
        this.value = checkNotNull(value);
    }

    public K key()
    {
        return key;
    }

    public Either<Throwable, V> value()
    {
        return value;
    }
}
