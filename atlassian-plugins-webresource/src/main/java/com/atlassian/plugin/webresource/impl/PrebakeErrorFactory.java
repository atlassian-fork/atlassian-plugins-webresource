package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.web.baseconditions.BaseCondition;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import org.apache.commons.lang3.StringUtils;

/**
 * Factory class for creating new {@link PrebakeError} instances.
 *
 * <p>The {@link PrebakeError}s generated by this class can be customized with
 * additional context objects to facilitate debugging.
 *
 * @since 3.5.9
 */
public final class PrebakeErrorFactory {

    private static final String DIMENSION_UNAWARE_CONDITION =
            "Encountered dimension unaware condition";
    private static final String DIMENSION_UNAWARE_TRANSFORMATION =
            "Encountered dimension unaware transformation";

    /**
     * Creates a new {@link PrebakeError} that wraps a resource-tainting condition.
     *
     * @param condition condition that generated the prebake error
     * @param <E> condition type
     * @return new {@link PrebakeError} wrapping a condition
     * @since 3.5.9
     */
    public static <E extends BaseCondition> PrebakeError from(E condition) {
        return new DimensionUnawarePrebakeError<>(DIMENSION_UNAWARE_CONDITION, condition);
    }

    /**
     * Creates a new {@link PrebakeError} that wraps a resource-tainting transformer.
     *
     * @param transformer transformer  that generated the prebake error
     * @param <E> transformer type
     * @return new {@link PrebakeError} wrapping a transformer
     * @since 3.5.9
     */
    public static <E extends TransformerUrlBuilder> PrebakeError from(E transformer) {
        return new DimensionUnawarePrebakeError<>(DIMENSION_UNAWARE_TRANSFORMATION, transformer);
    }

    /**
     * Creates a new {@link PrebakeError} that wraps a {@link Throwable} object.
     *
     * @param message additional error message
     * @param cause cause of the error
     * @return new {@link PrebakeError} wrapping a {@link Throwable}
     * @since 3.5.9
     */
    public static PrebakeError from(String message, Throwable cause) {
        return new ExceptionPrebakeError(message, cause);
    }

    /**
     * Creates a new {@link PrebakeError} that wraps a {@link Throwable} object.
     *
     * @param cause cause of the error
     * @return new {@link PrebakeError} wrapping a {@link Throwable}
     * @since 3.5.9
     */
    public static PrebakeError from(Throwable cause) {
        return new ExceptionPrebakeError(cause);
    }

    /**
     * Creates a new {@link PrebakeError} containing a simple text message.
     *
     * @param message error message
     * @return new {@link PrebakeError} containing a simple text message
     * @since 3.5.9
     */
    public static PrebakeError from(String message) {
        return new StringPrebakeError(message);
    }


    private static final class DimensionUnawarePrebakeError<E> implements PrebakeError {

        private final String info;
        private final E source;

        private DimensionUnawarePrebakeError(String info, E source) {
            this.info = info;
            this.source = source;
        }

        @Override
        public String toString() {
            return info + ": " + getClassName(source.getClass());
        }

        private String getClassName(Class<?> c) {
            String className = c.getCanonicalName();
            if (StringUtils.isEmpty(className)) {
                className = c.getName();
            }
            if (StringUtils.isEmpty(className)) {
                className = c.getSimpleName();
            }
            if (StringUtils.isEmpty(className)) {
                className = "unable to determine class name";
            }
            return className;
        }

    }


    private static final class StringPrebakeError implements PrebakeError {

        private final String message;

        private StringPrebakeError(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return message;
        }

    }


    private static final class ExceptionPrebakeError implements PrebakeError {

        private final String info;
        private final Throwable cause;

        private ExceptionPrebakeError(String info, Throwable cause) {
            this.info = info;
            this.cause = cause;
        }

        private ExceptionPrebakeError(Throwable cause) {
            this.info = null;
            this.cause = cause;
        }

        @Override
        public String toString() {
            if (StringUtils.isNotEmpty(info)) {
                return info + ": " + cause.toString();
            } else {
                return cause.toString();
            }
        }

    }

}
