package com.atlassian.plugin.webresource;

/**
 * A Web Resource Filter that allows for css and javascript resources.
 *
 * This is the default filter used by the {@link WebResourceManagerImpl} for include/get resource methods that do
 * not accept a filter as a parameter.
 *
 * @since 2.4
 */
public class DefaultWebResourceFilter implements WebResourceFilter
{
    private final JavascriptWebResource javascriptWebResource;
    private final CssWebResource cssWebResource;

    public DefaultWebResourceFilter(boolean isAmdEnabled)
    {
        this.javascriptWebResource = new JavascriptWebResource(isAmdEnabled);
        this.cssWebResource = new CssWebResource(isAmdEnabled);
    }

    public boolean matches(String resourceName)
    {
        return javascriptWebResource.matches(resourceName) || cssWebResource.matches(resourceName);
    }
}
