package com.atlassian.plugin.webresource.url;

import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.util.HashBuilder;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of UrlBuilder
 *
 * @since v3.0
 */
public class DefaultUrlBuilder implements UrlBuilder {
    private final List<String> hashes;
    private final Map<String, String> queryString;
    private final List<PrebakeError> prebakeErrors;

    public DefaultUrlBuilder() {
        this.hashes = new LinkedList<>();
        this.queryString = new LinkedHashMap<>();
        this.prebakeErrors = new LinkedList<>();
    }

    @Override
    public void addToHash(String name, Object value) {
        hashes.add(String.valueOf(value));
    }

    @Override
    public void addToQueryString(String key, String value) {
        if (queryString.containsKey(key) && !Support.equals(value, queryString.get(key))) {
            Support.LOGGER.warn("Different query values found for key: {} ({} / {})",
                    new String[]{key, queryString.get(key), value});
        }
        queryString.put(key, value);
    }

    @Override
    public void addPrebakeError(PrebakeError e) {
        prebakeErrors.add(e);
    }

    @Override
    public void addAllPrebakeErrors(Collection<PrebakeError> es) {
        prebakeErrors.addAll(es);
    }

    public List<PrebakeError> getPrebakeErrors() {
        return prebakeErrors;
    }

    public Map<String, String> buildParams() {
        return queryString;
    }

    public String buildHash() {
        return HashBuilder.buildHash(hashes);
    }

    public List<String> getHashes() {
        return hashes;
    }

    @Override
    public String toString() {
        return "hashes=" + hashes + ", queryString=" + queryString;
    }

    public void applyTo(UrlBuilder urlBuilder) {
        for (Map.Entry<String, String> entry : queryString.entrySet()) {
            urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
        }
        for (String hash : hashes) {
            urlBuilder.addToHash(null, hash);
        }
        urlBuilder.addAllPrebakeErrors(prebakeErrors);
    }
}
