package com.atlassian.plugin.webresource.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.PluginDataResource;

import java.util.Optional;

/**
 * Default PluginDataResource
 * @since v3.0
 */
public class DefaultPluginDataResource implements PluginDataResource
{
    private final String key;
    private final Optional<Jsonable> jsonable;

    public DefaultPluginDataResource(String key, Jsonable jsonable)
    {
        this.key = key;
        this.jsonable = Optional.of(jsonable);
    }
    public DefaultPluginDataResource(String key, Optional<Jsonable> result)
    {
        this.key = key;
        this.jsonable = result;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public Jsonable getJsonable() {
        return jsonable.get();
    }

    @Override
    public Optional<Jsonable> getData()
    {
        return jsonable;
    }


}
