package com.atlassian.plugin.webresource.cdn.mapper;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

/**
 * Represents a mappings file which contains all mappings.
 *
 * @since v3.5.7
 */
public interface MappingSet {

    /**
     * Returns the {@link Mapping} for a specific local resource, if found.
     * @param originalResource Relative URL of a local resource, without the context.
     * @return Mapping for the original resource.
     */
    @Nonnull
    Optional<Mapping> get(String originalResource);

    /**
     * Returns the list of mapped URLs for a specific local resource, if found.
     * @param originalResource Relative URL of a local resource, without the context.
     * @return List of mapped resources.
     */
    @Nonnull List<String> getMappedResources(String originalResource);

    /**
     * Inspects all {@link Mapping} in one instance.
     */
    @Nonnull Iterable<Mapping> all();

    /**
     * @return Number of {@link Mapping}s in this instance.
     */
    int size();

}
