package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.prebake.DimensionUnawareOverride;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareUrlReadingCondition;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.Map;

/**
 * Instance of a DecoratingCondition that wraps a UrlReadingCondition
 * @since v3.0
 */
public class DecoratingUrlReadingCondition implements DecoratingCondition
{
    protected final UrlReadingCondition urlReadingCondition;
    protected final Map<String, String> params;

    public DecoratingUrlReadingCondition(UrlReadingCondition urlReadingCondition, Map<String, String> params)
    {
        this.urlReadingCondition = urlReadingCondition;
        this.params = params;
    }


    @Override
    public Dimensions computeDimensions() {
        if (urlReadingCondition instanceof DimensionAwareUrlReadingCondition) {
            return ((DimensionAwareUrlReadingCondition) urlReadingCondition).computeDimensions();
        }

        // Temporary solution to inject dimension-awareness into a dimension-unaware condition.
        // Should be removed once all dimension-unaware conditions are updated.
        String className = urlReadingCondition.getClass().getName();
        if (DimensionUnawareOverride.contains(className)) {
            return DimensionUnawareOverride.dimensions(className);
        }

        return Dimensions.empty();
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy)
    {
        urlBuilderStrategy.addToUrl(urlReadingCondition, urlBuilder);
    }

    @Override
    public boolean canEncodeStateIntoUrl()
    {
        return true;
    }

    @Override
    public boolean shouldDisplayImmediate(Map<String, Object> context, UrlBuildingStrategy urlBuilderStrategy)
    {
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        addToUrl(urlBuilder, urlBuilderStrategy);
        return shouldDisplay(QueryParams.of(urlBuilder.buildParams()));
    }

    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        return urlReadingCondition.shouldDisplay(params);
    }

    @Override
    public DecoratingCondition invertCondition()
    {
        // It's never used, maybe it would be better to throw not implemented exception.
        return new DecoratingCondition()
        {
            @Override
            public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy)
            {
                DecoratingUrlReadingCondition.this.addToUrl(urlBuilder, urlBuilderStrategy);
            }

            @Override
            public boolean shouldDisplay(QueryParams params)
            {
                return !DecoratingUrlReadingCondition.this.shouldDisplay(params);
            }

            @Override
            public Dimensions computeDimensions()
            {
                return DecoratingUrlReadingCondition.this.computeDimensions();
            }

            @Override
            public boolean canEncodeStateIntoUrl()
            {
                return DecoratingUrlReadingCondition.this.canEncodeStateIntoUrl();
            }

            @Override
            public boolean shouldDisplayImmediate(Map<String, Object> context, UrlBuildingStrategy urlBuilderStrategy)
            {
                return !DecoratingUrlReadingCondition.this.shouldDisplayImmediate(context, urlBuilderStrategy);
            }

            @Override
            public DecoratingCondition invertCondition()
            {
                return DecoratingUrlReadingCondition.this;
            }
        };
    }

    public Map<String, String> getParams()
    {
        return params;
    }

    public UrlReadingCondition getUrlReadingCondition()
    {
        return urlReadingCondition;
    }
}
