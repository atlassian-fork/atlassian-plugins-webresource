package com.atlassian.plugin.webresource.cdn;

import java.util.Optional;

/**
 * Interface for an OSGi service providing {@link CDNStrategy} objects to the host application.
 *
 * @since v4.0.4
 */
public interface CdnStrategyProvider {
    /**
     * Returns the strategy for serving resources via CDN. This may return {@link Optional#empty()}, in which case no resources should
     * be served via CDN.
     *
     * @return CDN strategy
     */
    Optional<CDNStrategy> getCdnStrategy();
}
