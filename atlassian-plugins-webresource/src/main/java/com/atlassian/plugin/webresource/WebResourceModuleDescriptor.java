package com.atlassian.plugin.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.condition.UrlReadingConditionElementParser;
import com.atlassian.plugin.webresource.data.WebResourceDataProviderParser;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.Deprecation;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.atlassian.fugue.Option;
import org.dom4j.Attribute;
import org.dom4j.Element;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.atlassian.fugue.Option.option;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;

/**
 * A way of linking to web 'resources', such as javascript or css.  This allows us to include resources once
 * on any given page, as well as ensuring that plugins can declare resources, even if they are included
 * at the bottom of a page.
 */
public class WebResourceModuleDescriptor extends AbstractModuleDescriptor<Void>
{
    private boolean isRootPage;

    private List<String> dependencies = emptyList();
    private List<String> moduleDependencies = emptyList();
    private Set<String> contextDependencies = emptySet();

    private Set<String> asyncDependencies = emptySet();
    private Set<String> asyncContextDependencies = emptySet();

    private Set<String> contexts = emptySet();

    private boolean disableMinification;
    private final HostContainer hostContainer;
    private WebResourceDataProviderParser dataProviderParser;
    private Deprecation deprecation;
    private Map<String, WebResourceDataProvider> dataProviders = emptyMap();
    private List<WebResourceTransformation> webResourceTransformations = emptyList();
    private UrlReadingConditionElementParser conditionElementParser;
    private Element element;
    private DecoratingCondition condition;

    /**
     * @deprecated Since 3.0.0 - use {@link #WebResourceModuleDescriptor(com.atlassian.plugin.module.ModuleFactory, com.atlassian.plugin.hostcontainer.HostContainer)}
     */
    @Deprecated
    public WebResourceModuleDescriptor(final HostContainer hostContainer)
    {
        this(ModuleFactory.LEGACY_MODULE_FACTORY, hostContainer);
    }

    public WebResourceModuleDescriptor(ModuleFactory moduleFactory, final HostContainer hostContainer)
    {
        super(moduleFactory);
        this.conditionElementParser = new UrlReadingConditionElementParser(hostContainer);
        this.hostContainer = hostContainer;
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException
    {
        super.init(plugin, element);

        this.dependencies = parseDependencies(element, "web-resource", "dependency");
        this.moduleDependencies = parseDependencies(element, "web-module", "module-dependency");

        this.contextDependencies = parseDependencySet(element, "dependencies", "context");
        this.asyncDependencies = parseDependencySet(element, "async-dependencies", "web-resource");
        this.asyncContextDependencies = parseDependencySet(element, "async-dependencies", "context");

        this.isRootPage = element.element("root-page") != null;
        final LinkedHashSet<String> ctxs = new LinkedHashSet<>(contexts.size());
        ctxs.add(getCompleteKey()); // every <web-resource> has an implicit context based on its name

        List<Element> contexts = (List<Element>) element.elements("context");
        if (!contexts.isEmpty())
        {
            for (Element contextElement : contexts)
            {
                ctxs.add(contextElement.getTextTrim());
            }
        }
        this.contexts = Collections.unmodifiableSet(ctxs);

        webResourceTransformations = parseTransformations(element);
        deprecation = parseDeprecation(element, getCompleteKey());

        dataProviderParser = new WebResourceDataProviderParser(hostContainer, element.elements("data"));

        final Attribute minifiedAttribute = element.attribute("disable-minification");
        disableMinification = minifiedAttribute == null ? false : Boolean.valueOf(minifiedAttribute.getValue());
        this.element = element;
    }

    /**
     * Parses dependencies of the given type form the XML and returns them as a set.
     */
    private Set<String> parseDependencySet(final Element element,
                                           final String parentElementName,
                                           final String subElementName)
    {
        return Collections.unmodifiableSet((Set<String>) Stream.of(element.element(parentElementName))
                .filter(e -> e != null)
                .flatMap(parentElement -> (Stream<Element>) parentElement.elements(subElementName).stream())
                .map(Element::getTextTrim)
                .collect(Collectors.toCollection(LinkedHashSet::new)));
    }

    /**
     * Parses dependencies of the given types from the XML and returns them as a combined list.
     * @param element the XML root element
     * @param name the element name of the dependency (used in the dependencies block)
     * @param legacyName the legacy name of the dependency (used outside the dependencies block)
     */
    private List<String> parseDependencies(final Element element, final String name, final String legacyName)
    {
        final List<Element> webResourceDependencyElement = element.elements(legacyName);
        List<String> webResourceDependencies = webResourceDependencyElement.stream().map(Element::getTextTrim).collect(Collectors.toList());

        final Option<Element> dependenciesElementOpt = option(element.element("dependencies"));
        List<String> dependenciesElementWebResources = dependenciesElementOpt.map(dependenciesElement -> {
            final List<Element> webResources = dependenciesElement.elements(name);
            return webResources.stream().map(Element::getTextTrim).collect(Collectors.toList());
        }).getOrElse(new ArrayList<>());

        List<String> allWebResourceDependencies =
                new ArrayList<>(webResourceDependencies.size() + dependenciesElementWebResources.size());
        allWebResourceDependencies.addAll(webResourceDependencies);
        allWebResourceDependencies.addAll(dependenciesElementWebResources);

        return ImmutableList.copyOf(allWebResourceDependencies);
    }

    private static Deprecation parseDeprecation(Element element, @Nonnull String key) {
        final Element depEl = element.element("deprecated");
        Deprecation depNotice = null;
        if (depEl != null) {
            depNotice = new Deprecation(key);
            depNotice.setSinceVersion(depEl.attributeValue("since"));
            depNotice.setRemoveInVersion(depEl.attributeValue("remove"));
            depNotice.setAlternative(depEl.attributeValue("alternative"));
            depNotice.setExtraInfo(depEl.getText());
        }
        return depNotice;
    }

    /**
     * Parse transformations.
     */
    public static List<WebResourceTransformation> parseTransformations(Element element)
    {
        List<Element> transformations = (List<Element>) element.elements("transformation");
        if (!transformations.isEmpty())
        {
            final List<WebResourceTransformation> trans = new ArrayList<>(transformations.size());
            for (Element e : transformations)
            {
                trans.add(new WebResourceTransformation(e));
            }
            return ImmutableList.copyOf(trans);
        }
        return emptyList();
    }

    /**
     * As this descriptor just handles resources, you should never call this
     */
    @Override
    public Void getModule()
    {
        throw new UnsupportedOperationException("There is no module for Web Resources");
    }

    @Override
    public void enabled()
    {
        super.enabled();
        condition = parseCondition(conditionElementParser, plugin, element);
        try
        {
            dataProviders = dataProviderParser.createDataProviders(plugin, this.getClass());
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException("Unable to enable web resource due to an issue processing data-provider", e);
        }
        catch (final PluginParseException e)
        {
            throw new RuntimeException("Unable to enable web resource due to an issue processing data-provider", e);
        }
    }

    public static DecoratingCondition parseCondition(UrlReadingConditionElementParser conditionElementParser, Plugin plugin, Element element)
    {
        try
        {
            return conditionElementParser.makeConditions(plugin, element, AbstractConditionElementParser.CompositeType
                .AND);
        }
        catch (final PluginParseException e)
        {
            // is there a better exception to throw?
            throw new RuntimeException("Unable to enable web resource due to issue processing condition", e);
        }
    }

    @Override
    public void disabled()
    {
        super.disabled();
        condition = null;
        dataProviders = emptyMap();
    }

    public boolean isDeprecated() {
        return deprecation != null;
    }

    public Deprecation getDeprecation() {
        return deprecation;
    }

    /**
     * Returns the web resource contexts this resource is associated with.
     *
     * @return  the web resource contexts this resource is associated with.
     * @since 2.5.0
     */
    public Set<String> getContexts()
    {
        return contexts;
    }

    /**
     * Returns a list of dependencies on other web resources.
     * @return a list of module complete keys
     */
    public List<String> getDependencies()
    {
        return dependencies;
    }

    /**
     * Returns a list of dependencies on other web resources.
     * @return a list of module complete keys
     * @since 3.4.8
     */
    public List<String> getModuleDependencies()
    {
        return moduleDependencies;
    }

    /**
     * Returns an Option of the RootPage object.
     * @return an Option of the RootPage object
     */
    public boolean isRootPage()
    {
        return isRootPage;
    }

    /**
     * Returns the set of contexts that this resource depends on.
     * @return the set of contexts that this resource depends on
     * @since 3.5.9
     */
    public Set<String> getContextDependencies()
    {
        return contextDependencies;
    }

    /**
     * Returns the set of web resources that will be required asynchronously by the root page.
     * @return the set of web resources that will be required asynchronously by the root page
     * @since 3.5.9
     */
    public Set<String> getAsyncDependencies()
    {
        return asyncDependencies;
    }

    /**
     * Returns the set of contexts that will be required asynchronously by the root page.
     * @return the set of contexts that will be required asynchronously by the root page
     * @since 3.5.9
     */
    public Set<String> getAsyncContextDependencies()
    {
        return asyncContextDependencies;
    }

    public List<WebResourceTransformation> getTransformations()
    {
        return webResourceTransformations;
    }

    public DecoratingCondition getCondition() {
        return condition;
    }

    /**
     * @return <code>true</code> if resource minification should be skipped, <code>false</code> otherwise.
     */
    public boolean isDisableMinification()
    {
        return disableMinification;
    }

    /**
     * @return true if this resource's conditions can fully encode themselves into the URL, false if they can't.
     */
    public boolean canEncodeStateIntoUrl()
    {
        return getCondition() == null || getCondition().canEncodeStateIntoUrl();
    }

    /**
     * @param params querystring params
     * @return true if this resource should be displayed for the given query parameters, otherwise false
     */
    public boolean shouldDisplay(QueryParams params)
    {
        return getCondition() == null || getCondition().shouldDisplay(params);
    }

    /**
     * @return True if this web resource should be displayed based on the optional condition
     * @since 2.7.0
     */
    public boolean shouldDisplayImmediate() // TODO this is "immediatE" and should prob be deprecated
    {
        UrlBuildingStrategy urlBuilderStrategy = UrlBuildingStrategy.normal();
        return getCondition() == null || getCondition().shouldDisplayImmediate(ImmutableMap.<String, Object>of(), urlBuilderStrategy);
    }

    /**
     * @return data providers specified by this web-resource
     * @since 3.0
     */
    public Map<String, WebResourceDataProvider> getDataProviders()
    {
        return dataProviders;
    }

    /**
     * Returns all condition-1 keys used by this descriptor.
     * This is a temporary method, used to extract all condition1's in a running instance.
     * @deprecated in 3.0.5
     */
    @Deprecated
    public Set<String> getDeprecatedConditionKeys()
    {
        final Set<String> allConditions = new HashSet<>();
        UrlReadingConditionElementParser parser = new UrlReadingConditionElementParser(hostContainer) {
            @Override
            protected DecoratingCondition makeConditionImplementation(Plugin plugin, Element element) throws PluginParseException
            {
                DecoratingCondition condition = super.makeConditionImplementation(plugin, element);
                if (!condition.canEncodeStateIntoUrl())
                {
                    allConditions.add(element.attributeValue("class"));
                }
                return condition;
            }
        };
        parser.makeConditions(plugin, element, AbstractConditionElementParser.CompositeType.AND);
        return allConditions;
    }
    /**
     * Returns all transform-1 keys used by this descriptor.
     * This is a temporary method, used to extract all transform1's in a running instance.
     * @deprecated in 3.0.5
     */
    @Deprecated
    public Set<String> getDeprecatedTransformKeys(TransformerCache transformerCache)
    {
        final Set<String> allTransforms= new HashSet<>();
        for (WebResourceTransformation transformation : getTransformations())
        {
            for (WebResourceTransformerModuleDescriptor descriptor : transformation.getDeprecatedTransformers(transformerCache))
            {
                allTransforms.add(descriptor.getCompleteKey());
            }
        }
        return allTransforms;
    }
}
