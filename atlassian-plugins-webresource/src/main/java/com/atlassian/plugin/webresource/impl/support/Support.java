package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.sourcemap.Mapping;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.SourceMapImpl;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.List;

/**
 * Basic utility functions.
 *
 * @since v6.3
 */
public class Support {
    public static final Logger LOGGER = LoggerFactory.getLogger("webresource");

    /**
     * Join list of predicates with AND operand.
     */
    public static Predicate<Bundle> efficientAndPredicate(List<Predicate<Bundle>> predicates) {
        if (predicates.size() == 0) {
            return Predicates.alwaysTrue();
        } else if (predicates.size() == 1) {
            return predicates.get(0);
        } else {
            return Predicates.and(predicates);
        }
    }

    /**
     * Indent string, multiline also supported.
     */
    public static String indent(String string, String indent) {
        return indent + string.replaceAll("\\n", "\n" + indent);
    }

    /**
     * Helper to check for equality without bothering to handle nulls.
     */
    public static boolean equals(Object a, Object b) {
        return a == null ? b == null : a.equals(b);
    }

    /**
     * Copy in into out and close streams safely.
     */
    public static void copy(final InputStream in, final OutputStream out) {
        try {
            IOUtils.copy(in, out);
        }
        catch (final IOException e)
        {
            logIOException(e);
        }
        finally
        {

            IOUtils.closeQuietly(in);
            try {
                out.flush();
            } catch (final IOException e) {
                Support.LOGGER.debug("Error flushing output stream", e);
            }
        }
    }

    public static void logIOException(IOException e){
        if (e instanceof SocketException && "Broken pipe".equals(e.getMessage())) {
            Support.LOGGER.trace("Client Abort error", e);
        } else {
            Support.LOGGER.debug("IO Exception", e);
        }
    }

    /**
     * Parses XML.
     */
    public static Element parseXml(String xml) {
        Document document;
        try {
            document = DocumentHelper.parseText(xml);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
        return document.getRootElement();
    }

    /**
     * Replaces source url in source map.
     */
    public static SourceMap replaceSourceUrl(SourceMap sourceMap, final String sourceUrl)
    {
        final SourceMap result = new SourceMapImpl();
        sourceMap.eachMapping(new SourceMap.EachMappingCallback() {
            @Override
            public void apply(final Mapping mapping) {
                result.addMapping(
                        mapping.getGeneratedLine(), mapping.getGeneratedColumn(),
                        mapping.getSourceLine(), mapping.getSourceColumn(),
                        sourceUrl, mapping.getSourceSymbolName());
            }
        });
        return result;
    }

    public static SourceMap getSourceMap(final String resourcePath, final Resource resource, final String sourceUrl) {
        // Loading source map.
        String pathForSourceMap = resourcePath + ".map";
        InputStream sourceMapAsStream = resource.getStreamFor(pathForSourceMap);
        if (sourceMapAsStream == null)
        {
            // Currently almost no one plugin has source maps, there will be too many
            // warnings in the log. Maybe it will be enabled later.
            // Support.LOGGER.warn("no source map for " + resource.getKey() + "/" + resource.getName());
            return null;
        }
        else
        {
            SourceMap sourceMap = new SourceMapImpl(sourceMapAsStream);
            // During the compilation step the source url is unknown so the file name used
            // as the source url. It is replaced to the actual source url at runtime.
            return replaceSourceUrl(sourceMap, sourceUrl);
        }
    }

    public static String inspect(Content content) {
        OutputStream buff = new ByteArrayOutputStream();
        SourceMap sourceMap = content.writeTo(buff, true);
        try {
            buff.write("\n".getBytes());
            if (sourceMap != null) {
                buff.write(sourceMap.generate().getBytes());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return buff.toString();
    }
}
