package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;

/**
 * Supplies static transformers
 *
 * @since v3.1.0
 */
public interface StaticTransformersSupplier {

    /**
     * Returns the {@link Dimensions} of all registered transformers.
     *
     * @return dimensions of all registeres transformers
     */
    Dimensions computeDimensions();

    /**
     * @param locationType type of resource (eg js / css)
     * @return static transformers to apply to the given type
     */
    Iterable<DimensionAwareWebResourceTransformerFactory> get(String locationType);

    /**
     * @param resourceLocation resource location
     * @return static transformers to apply to the given resourceLocation
     */
    Iterable<DimensionAwareWebResourceTransformerFactory> get(ResourceLocation resourceLocation);

}
