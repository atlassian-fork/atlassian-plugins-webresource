package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.BaseHelpers;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Snapshot of the current state of the plugin system, captures list of web resources and its transformers
 * and conditions.
 *
 * @since v3.3
 */
public class Snapshot
{
    final Config config;
    private final Map<String, Bundle> cachedBundles;
    private final Map<String, RootPage> rootPages;

    // Storing some attribute of Resource outside of it in a separate Set in order to consume less memory
    // because amount of such attributes should be much smaller than the amount of web resources.
    final Map<WebResource, CachedTransformers> webResourcesTransformations;
    final Map<WebResource, CachedCondition> webResourcesCondition;
    final Map<String, Deprecation> webResourceDeprecationWarnings;
    final Set<WebResource> webResourcesWithLegacyConditions;
    final Set<WebResource> webResourcesWithLegacyTransformers;
    final Set<WebResource> webResourcesWithDisabledMinification;

    public Snapshot(Config config)
    {
        this(config, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashSet<>(),
                new HashSet<>(), new HashSet<>());
    }

    public Snapshot(
            Config config,
            Map<String, Bundle> cachedBundles,
            Map<String, RootPage> rootPages,
            Map<WebResource, CachedTransformers> webResourcesTransformations,
            Map<WebResource, CachedCondition> webResourcesCondition,
            Set<WebResource> webResourcesWithLegacyConditions,
            Set<WebResource> webResourcesWithLegacyTransformers,
            Set<WebResource> webResourcesWithDisabledMinification) {
        this(config, cachedBundles, rootPages, webResourcesTransformations, webResourcesCondition,
                new HashMap<>(), webResourcesWithLegacyConditions, webResourcesWithLegacyTransformers,
                webResourcesWithDisabledMinification);
    }

    public Snapshot(
            Config config,
            Map<String, Bundle> cachedBundles,
            Map<String, RootPage> rootPages,
            Map<WebResource, CachedTransformers> webResourcesTransformations,
            Map<WebResource, CachedCondition> webResourcesCondition,
            Map<String, Deprecation> webResourceDeprecationWarnings,
            Set<WebResource> webResourcesWithLegacyConditions,
            Set<WebResource> webResourcesWithLegacyTransformers,
            Set<WebResource> webResourcesWithDisabledMinification)
    {
        this.cachedBundles = cachedBundles;
        this.rootPages = rootPages;
        this.webResourcesTransformations = webResourcesTransformations;
        this.webResourcesCondition = webResourcesCondition;
        this.webResourcesWithLegacyConditions = webResourcesWithLegacyConditions;
        this.webResourcesWithLegacyTransformers = webResourcesWithLegacyTransformers;
        this.webResourcesWithDisabledMinification = webResourcesWithDisabledMinification;
        this.webResourceDeprecationWarnings = webResourceDeprecationWarnings;
        this.config = config;
    }

    public List<Bundle> toBundles(Iterable<String> keys)
    {
        List<Bundle> bundles = new ArrayList<>();
        for (String key : keys)
        {
            Bundle bundle = get(key);
            if (bundle != null)
            {
                bundles.add(bundle);
            }

        }
        return bundles;
    }

    /**
     * Apply some function to each {@link Bundle} of this {@link Snapshot}.
     * @param consumer - function to apply to Bundle.
     */
    public void forEachBundle(Consumer<Bundle> consumer) {
        cachedBundles.values().forEach(consumer);
    }

    public Bundle get(final String key)
    {
        return cachedBundles.get(key);
    }

    public RootPage getRootPage(final String key) {
        return rootPages.get(key);
    }

    public Iterable<RootPage> getAllRootPages() {
        return rootPages.values();
    }

    public Iterable<CachedCondition> conditions()
    {
        return webResourcesCondition.values();
    }

    public Iterable<CachedTransformers> transformers()
    {
        return webResourcesTransformations.values();
    }
    /**
     * Find bundles.
     */
    public BaseHelpers.BundleFinder find()
    {
        return Helpers.find(this);
    }

    public Set<WebResource> getWebResourcesWithLegacyConditions()
    {
        return webResourcesWithLegacyConditions;
    }

    public Set<WebResource> getWebResourcesWithLegacyTransformers()
    {
        return webResourcesWithLegacyTransformers;
    }
}
