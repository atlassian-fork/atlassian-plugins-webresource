package com.atlassian.plugin.webresource.util;

import java.util.concurrent.TimeUnit;

/**
 */
public class TimeSpan {
    private final long time;
    private final TimeUnit units;

    public TimeSpan(long time, TimeUnit units) {
        this.time = time;
        this.units = units;
    }

    public long getTime() {
        return time;
    }

    public TimeUnit getUnits() {
        return units;
    }

    public long toMillis() {
        return units.toMillis(time);
    }
}
