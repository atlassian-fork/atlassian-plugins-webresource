package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.cache.filecache.impl.FileCacheImpl;
import com.atlassian.plugin.cache.filecache.impl.PassThroughCache;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.support.ResettableLazyReferenceWithVersionCheck;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.impl.support.UrlCache;

import java.util.ArrayList;
import java.util.List;

/**
 * Set of global final stateless objects like configuration, cache etc.
 *
 * @since 3.3
 */
public class Globals {
    private final Config config;
    private final ResettableLazyReferenceWithVersionCheck<Snapshot> cachedSnapshot;
    private final List<StateChangeCallback> stateChangeCallbacks;

    // Not all fields are final because some of them are initialized in a complex way and can't be set in constructor,
    // but, there are guards that will not allow to change those fields once they are initialized.
    private final Router router;
    private Cache contentCache;
    private Cache temporaryIncrementalCache;
    private UrlCache urlCache;

    public Globals(final Config config) {
        this.config = config;

        stateChangeCallbacks = new ArrayList<>();

        buildAndSetContentCache();
        buildAndSetTemporaryIncrementalCache();
        buildAndSetUrlGenerationCache();
        this.router = new Router(this);

        // Caching web resources. This cache get flushed on two events, the first is the plugin change event,
        // the second if config version has been changed (checked for every access).
        cachedSnapshot = new ResettableLazyReferenceWithVersionCheck<Snapshot>() {
            @Override
            protected Snapshot create() {
                Snapshot snapshot = config.getWebResourcesWithoutCache();

                // Perform Global minification of Snapshot if Enabled
                if (config.isGlobalMinificationEnabled()) {
                    config.runResourceCompilation(snapshot);
                }

                return snapshot;
            }

            @Override
            protected int getVersion() {
                return config.partialHashCode();
            }
        };
        onStateChange(cachedSnapshot::reset);
    }

    public Config getConfig() {
        return config;
    }

    public Router getRouter() {
        return router;
    }

    public com.atlassian.plugin.cache.filecache.Cache getContentCache() {
        return contentCache;
    }

    interface StateChangeCallback {
        public void apply();
    }

    /**
     * Provided callback would be called if the state of the system changed (any plugin change event). Used mostly to
     * setup cache clear listeners.
     */
    public void onStateChange(StateChangeCallback callback) {
        stateChangeCallbacks.add(callback);
    }

    /**
     * Trigger the state change event, it will notify all state change listeners. Used mostly to clear cache.
     */
    public void triggerStateChange() {
        for (StateChangeCallback callback : stateChangeCallbacks) {
            callback.apply();
        }
    }

    /**
     * Build and setup Content Cache according to configuration setting.
     */
    private void buildAndSetContentCache() {
        if (contentCache != null) {
            throw new RuntimeException("content cache already set!");
        }
        if (config.isContentCacheEnabled()) {
            try {
                contentCache = new FileCacheImpl(config.getCacheDirectory(), config.getContentCacheSize());
                onStateChange(contentCache::clear);
            } catch (Exception e) {
                Support.LOGGER.error("Could not create file cache object, will startup with filecaching disabled, "
                        + "please investigate the cause and correct it.", e);
                contentCache = new PassThroughCache();
            }
        } else {
            contentCache = new PassThroughCache();
        }
    }

    /**
     * Build and setup URL Generation Cache cache according to configuration setting.
     */
    private void buildAndSetUrlGenerationCache() {
        if (urlCache != null) {
            throw new RuntimeException("url cache already set!");
        }
        urlCache = config.isUrlGenerationCacheEnabled() ? new UrlCache.Impl(config.getUrlCacheSize()) : new UrlCache.PassThrough();
        onStateChange(urlCache::clear);
    }

    public UrlCache getUrlCache() {
        return urlCache;
    }

    /**
     * Build and setup cache according to configuration setting.
     */
    public void buildAndSetTemporaryIncrementalCache() {
        if (temporaryIncrementalCache != null) {
            throw new RuntimeException("temporary incremental cache already set!");
        }
        if (config.isIncrementalCacheEnabled()) {
            try {
                temporaryIncrementalCache = new FileCacheImpl(config.getCacheDirectory(), config.getIncrementalCacheSize());
                onStateChange(temporaryIncrementalCache::clear);
            } catch (Exception e) {
                Support.LOGGER.error("Could not create file cache object, will startup with filecaching disabled, "
                        + "" + "please investigate the cause and correct it.", e);
                temporaryIncrementalCache = new PassThroughCache();
            }
        } else {
            temporaryIncrementalCache = new PassThroughCache();
        }
    }

    public Cache getTemporaryIncrementalCache() {
        return temporaryIncrementalCache;
    }

    /**
     * Analyzes current state of the plugin system and return web resources. Uses cache, the cache would be cleared on
     * any plugin change event.
     */
    public Snapshot getSnapshot() {
        return cachedSnapshot.get();
    }
}