package com.atlassian.plugin.webresource.legacy;

import java.util.Set;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.3
 */
public class InclusionState
{
    /** Has the superbatch been included */
    public boolean superbatch;
    /** Webresources that have been included in previous calls to includeResources, and all the individual resources
     * in included contexts */
    public Set<String> webresources;
    /** Webresource contexts that have been included in previous calls to includeResources */
    public Set<String> contexts;

    public InclusionState(boolean superbatch, Set<String> webresources, Set<String> contexts)
    {
        this.superbatch = superbatch;
        this.webresources = webresources;
        this.contexts = contexts;
    }
}