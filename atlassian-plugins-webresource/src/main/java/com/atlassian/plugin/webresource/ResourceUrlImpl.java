package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v6.3
 */
public class ResourceUrlImpl extends ResourceUrl {
    private final Globals globals;
    private final Resource resource;
    private final String hash;
    private final Map<String, String> params;
    private final boolean hasLegacyTransformer;

    public ResourceUrlImpl(Globals globals, Resource resource, final Map<String, String> params, String hash,
                           boolean hasLegacyTransformer, List<PrebakeError> prebakeErrors) {
        super(prebakeErrors);
        this.globals = globals;
        this.resource = resource;
        this.params = params;
        this.hash = hash;
        this.hasLegacyTransformer = hasLegacyTransformer;
    }

    public String getName() {
        return resource.getName();
    }

    public String getKey() {
        return resource.getKey();
    }

    public String getType() {
        return resource.getNameOrLocationType();
    }

    public String getUrl(final boolean isAbsolute) {
        boolean isCdnSupported = !hasLegacyTransformer && !resource.getParent().hasLegacyConditions();
        return globals.getRouter().cloneWithNewUrlMode(isAbsolute).resourceUrl(getKey(), getName(),
                getParams(), resource.isCacheable(), isCdnSupported, hash, resource.getVersion());
    }

    public Map<String, String> getParams() {
        return params;
    }

    public PluginUrlResource.BatchType getBatchType() {
        return PluginUrlResource.BatchType.RESOURCE;
    }

    @Override
    public List<Resource> getResources(RequestCache requestCache) {
        List<Resource> resources = new ArrayList<>();
        resources.add(resource);
        return resources;
    }
}
