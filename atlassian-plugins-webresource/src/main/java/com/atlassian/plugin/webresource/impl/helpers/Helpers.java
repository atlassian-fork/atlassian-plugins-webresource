package com.atlassian.plugin.webresource.impl.helpers;

/**
 * Stateless helper functions.
 *
 * The goal of this class is to unite Url Generation and Serving helpers in one namespace.
 *
 * @since 3.3
 */
public class Helpers extends ResourceServingHelpers
{
}