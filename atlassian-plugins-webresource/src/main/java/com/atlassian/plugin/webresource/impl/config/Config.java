package com.atlassian.plugin.webresource.impl.config;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.util.validation.ValidationException;
import com.atlassian.plugin.webresource.PluginResourceContainer;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformerImpl;
import com.atlassian.plugin.webresource.cdn.mapper.DefaultWebResourceMapper;
import com.atlassian.plugin.webresource.cdn.mapper.MappingParser;
import com.atlassian.plugin.webresource.cdn.mapper.NoOpWebResourceMapper;
import com.atlassian.plugin.webresource.cdn.mapper.WebResourceMapper;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.annotators.AsyncLoaderAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.ListOfAnnotators;
import com.atlassian.plugin.webresource.impl.annotators.LocationContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.ModuleAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.SemicolonResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.TryCatchJsResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.modules.ModulesDescriptor;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Context;
import com.atlassian.plugin.webresource.impl.snapshot.Deprecation;
import com.atlassian.plugin.webresource.impl.snapshot.ModuleResource;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.snapshot.WebModule;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.impl.support.ConditionInstanceCache;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.util.HashBuilder;
import com.atlassian.plugin.webresource.util.TimeSpan;
import com.atlassian.sourcemap.Util;
import com.atlassian.webresource.api.assembler.resource.CompleteWebResourceKey;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.atlassian.webresource.spi.CompilerEntry;
import com.atlassian.webresource.spi.CompilerUtil;
import com.atlassian.webresource.spi.NoOpResourceCompiler;
import com.atlassian.webresource.spi.ResourceCompiler;
import com.atlassian.webresource.spi.TransformationDto;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.io.CharStreams;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.plugin.webresource.Flags.getFileCacheSize;
import static com.atlassian.plugin.webresource.Flags.isDevMode;
import static com.atlassian.plugin.webresource.Flags.isFileCacheEnabled;
import static com.atlassian.plugin.webresource.impl.support.Support.parseXml;
import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.joinWithSlashWithoutEmpty;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Collections.sort;

/**
 * Unites all the configurations in one place. Not exposed as API, so it is possible to change it and store settings
 * that should not be exposed publicly.
 *
 * @since 3.3
 */
public class Config {
    /** @deprecated Since v4.0. Will be removed in v5.0. */
    public static final String IEONLY_PARAM_NAME = "ieonly";
    public static final String SOURCE_PARAM_NAME = "source";
    public static final String BATCH_PARAM_NAME = "batch";
    public static final String MEDIA_PARAM_NAME = "media";
    public static final String CONTENT_TYPE_PARAM_NAME = "content-type";
    public static final String CACHE_PARAM_NAME = "cache";
    public static final String ALLOW_PUBLIC_USE_PARAM_NAME = "allow-public-use";
    /** @deprecated Since v4.0. Will be removed in v5.0. */
    public static final String CONDITIONAL_COMMENT_PARAM_NAME = "conditionalComment";
    public static final String ASYNC_SCRIPT_PARAM_NAME = "async";
    public static final String INITIAL_RENDERED_SCRIPT_PARAM_NAME = "data-initially-rendered";
    public static final String WRM_KEY_PARAM_NAME = "data-wrm-key";
    public static final String WRM_BATCH_TYPE_PARAM_NAME = "data-wrm-batch-type";

    public static final String DOWNLOAD_PARAM_VALUE = "download";

    public static final String JS_TYPE = "js";
    public static final String JS_CONTENT_TYPE = "application/javascript";
    public static final String CSS_TYPE = "css";
    public static final String CSS_CONTENT_TYPE = "text/css";
    public static final String SOY_TYPE = "soy";
    public static final String CSS_EXTENSION = ".css";
    public static final String LESS_EXTENSION = ".less";
    public static final String JS_EXTENSION = ".js";
    public static final String SOY_EXTENSION = ".soy";
    public static final String[] BATCH_TYPES = new String[]{CSS_TYPE, JS_TYPE};
    public static final String WEB_RESOURCE_MODULE_LOADER = "wr";

    private static final int ATLASSIAN_MODULES_VERSION = 2;
    public static final int GET_URL_MAX_LENGTH = 8 * 1024;

    public static final String[] HTTP_PARAM_NAMES = {
            IEONLY_PARAM_NAME,
            MEDIA_PARAM_NAME,
            CONTENT_TYPE_PARAM_NAME,
            CACHE_PARAM_NAME,
            CONDITIONAL_COMMENT_PARAM_NAME,
            ALLOW_PUBLIC_USE_PARAM_NAME
    };

    public static final List<String> PARAMS_SORT_ORDER = asList(
            CACHE_PARAM_NAME,
            MEDIA_PARAM_NAME,
            CONDITIONAL_COMMENT_PARAM_NAME,
            IEONLY_PARAM_NAME
    );

    public static final String ATLASSIAN_MODULES_XML = "atlassian-modules.xml";
    public static final String CONTEXT_PREFIX = "_context";
    public static final String SUPER_BATCH_CONTEXT_KEY = "_super";
    public static final String SUPERBATCH_KEY = Config.CONTEXT_PREFIX + ":" + SUPER_BATCH_CONTEXT_KEY;
    public static final String SYNCBATCH_CONTEXT_KEY = "_sync";
    public static final String SYNCBATCH_KEY = Config.CONTEXT_PREFIX + ":" + SYNCBATCH_CONTEXT_KEY;

    private static final String DISABLE_MINIFICATION = "atlassian.webresource.disable.minification";
    private static final String DISABLE_URL_CACHING = "atlassian.webresource.disable.url.caching";
    private static final String GLOBAL_MINIFICATION_ENABLED = "atlassian.webresource.enable.global.minification";
    private static final String PREBAKE_FEATURE_ENABLED = "atlassian.webresource.enable.prebake";
    private static final String CT_CDN_BASE_URL = "atlassian.webresource.ct.cdn.base.url";
    private static final String PREBAKE_CSS_RESOURCES = "atlassian.webresource.enable.css.prebake";
    private static final String IGNORE_PREBAKE_WARNINGS = "atlassian.webresource.ignore.prebake.warnings";
    private static final String STATIC_CONTEXT_ORDER_ENABLED = "atlassian.webresource.stable.contexts.enable";

    private static final String COMPILED_RESOURCE_LOCATION_PARAMETER = "compiledLocation";

    private static final String PLUGIN_KEY = "com.atlassian.plugins.atlassian-plugins-webresource-plugin";

    public static final String IN_ORDER_LOADER_RESOURCE_KEY = PLUGIN_KEY+":in-order-loader";
    private static final List<String> BEFORE_ALL_AMD_RESOURCES = singletonList(
            PLUGIN_KEY + ":atlassian-module"
    );

    private static final Pattern SNAPSHOT_PLUGIN_REGEX = Pattern.compile("SNAPSHOT", Pattern.CASE_INSENSITIVE);

    private static final Logger log = LoggerFactory.getLogger(Config.class);
    private Logger supportLogger = Support.LOGGER;

    private final WebResourceIntegration integration;
    private final ResourceBatchingConfiguration batchingConfiguration;
    private final WebResourceUrlProvider urlProvider;
    private final boolean isContentCacheEnabled;
    private final int contentCacheSize;
    private final Integer incrementalCacheSize;
    private final boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins;
    private ContentTypeResolver contentTypeResolver;
    private final ServletContextFactory servletContextFactory;
    private StaticTransformers staticTransformers;
    private final TransformerCache transformerCache;

    /**
     * Do not use this reference directly, instead call {@link #getWebResourceMapper()}
     */
    private final ResettableLazyReference<WebResourceMapper> cdnUrlMapper = new ResettableLazyReference<WebResourceMapper>() {
        @Override
        protected WebResourceMapper create() throws Exception {
            if (integration.isCtCdnMappingEnabled()) {
                final Optional<PrebakeConfig> prebakeConfig = integration.getCDNStrategy() != null ?
                        integration.getCDNStrategy().getPrebakeConfig() : Optional.empty();
                if (prebakeConfig.isPresent()) {
                    try {
                        log.info("Creating DefaultWebResourceMapper");
                        return new DefaultWebResourceMapper(integration, new MappingParser(), prebakeConfig.get(),
                                computeGlobalStateHash(), getCtCdnBaseUrl(), integration.getBaseUrl(UrlMode.RELATIVE));
                    } catch (FileNotFoundException e) {
                        //no additional logging, it isn't an error
                        return new NoOpWebResourceMapper(e);
                    } catch (Exception e) {
                        log.info("DefaultWebResourceMapper was not created properly. Pre-baked CT-CDN is disabled but will fall back to product's CDN Strategy, if any.", e);
                        return new NoOpWebResourceMapper(e);
                    }
                } else {
                    log.info("Creating NoOpWebResourceMapper since PrebakeConfig is not present. Pre-baked CT-CDN is disabled but will fall back to product's CDN Strategy, if any.");
                    return new NoOpWebResourceMapper(new Exception("PrebakeConfig not present"));
                }
            } else {
                log.info("Creating NoOpWebResourceMapper since WebResourceIntegration#isCtCdnMappingEnabled flag is disabled. Pre-baked CT-CDN is disabled but will fall back to product's CDN Strategy, if any.");
                return new NoOpWebResourceMapper(new Exception("CT-CDN is explicitly disabled by the product."));
            }
        }
    };

    private final CdnResourceUrlTransformer cdnResourceUrlTransformer;
    private final ResourceCompiler resourceCompiler;
    public static final String INCREMENTAL_CACHE_SIZE = "plugin.webresource.incrementalcache.size";
    private final int urlCacheSize;
    private final boolean isUrlCachingEnabled;
    private boolean syncContextCreated;

    public Config(ResourceBatchingConfiguration batchingConfiguration, WebResourceIntegration integration,
                  WebResourceUrlProvider urlProvider, ServletContextFactory servletContextFactory,
                  TransformerCache transformerCache, ResourceCompiler resourceCompiler) {
        this.batchingConfiguration = batchingConfiguration;
        this.integration = integration;
        this.urlProvider = urlProvider;
        this.servletContextFactory = servletContextFactory;
        this.transformerCache = transformerCache;
        this.isContentCacheEnabled = isFileCacheEnabled();
        this.contentCacheSize = getFileCacheSize(1000);
        this.isUrlCachingEnabled = !Boolean.getBoolean(DISABLE_URL_CACHING);
        this.urlCacheSize = getFileCacheSize(200);
        this.incrementalCacheSize = Integer.getInteger(INCREMENTAL_CACHE_SIZE, 1000);
        cdnResourceUrlTransformer = new CdnResourceUrlTransformerImpl(this);
        this.resourceCompiler = resourceCompiler == null ? new NoOpResourceCompiler() : resourceCompiler;
        this.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins = integration.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins();
        this.syncContextCreated = false;
    }

    /**
     * Needed because `contentTypeResolver` isn't available at the time Config is created and set up a bit later.
     */
    public void setContentTypeResolver(ContentTypeResolver contentTypeResolver) {
        if (this.contentTypeResolver != null) {
            throw new RuntimeException("content type resolver already set!");
        }
        this.contentTypeResolver = contentTypeResolver;
    }

    public void setStaticTransformers(StaticTransformers staticTransformers) {
        if (this.staticTransformers != null) {
            throw new RuntimeException("static transformers already set!");
        }
        this.staticTransformers = staticTransformers;
    }

    /**
     * @return if cache should be enabled.
     */
    public boolean isContentCacheEnabled() {
        return isContentCacheEnabled;
    }

    /**
     * @return amount of files to be stored in cache.
     */
    public int getContentCacheSize() {
        return contentCacheSize;
    }

    /**
     * @return amount of files to be stored in cache.
     */
    public int getIncrementalCacheSize() {
        return incrementalCacheSize;
    }

    /**
     * @return directory where cache content would be stored as files.
     */
    public File getCacheDirectory() {
        return integration.getTemporaryDirectory();
    }

    /**
     * @param typeOrContentType type or content type.
     * @return if source map enabled for this type or content type.
     */
    public boolean isSourceMapEnabledFor(String typeOrContentType) {
        return isSourceMapEnabled() && Util.isSourceMapSupportedBy(typeOrContentType);
    }

    /**
     * @return if source map enabled.
     */
    public boolean isSourceMapEnabled() {
        return batchingConfiguration.isSourceMapEnabled();
    }

    /**
     * @return if CDN is globally enabled.
     */
    public boolean isCdnEnabled() {
        return null != integration.getCDNStrategy() && integration.getCDNStrategy().supportsCdn();
    }

    /**
     * @return base url, where web resources is mounted to.
     */
    public String getBaseUrl() {
        return getBaseUrl(true);
    }

    /**
     * @param isAbsolute if url should be absolute or relative.
     * @return base url, where web resources is mounted to.
     */
    public String getBaseUrl(boolean isAbsolute) {
        try {
            return joinWithSlashWithoutEmpty(urlProvider.getBaseUrl(isAbsolute ? UrlMode.ABSOLUTE : UrlMode.RELATIVE),
                    AbstractFileServerServlet.SERVLET_PATH);
        } catch (AssertionError e) {
            // For unknown reason some links in Confluence (like /confluence/setup/setupstart.action ) doesn't
            // support the absolute base url, catching such cases and trying to use relative base url.
            if (isAbsolute && e.getMessage().contains("Unsupported URLMode")) {
                return getBaseUrl(false);
            } else {
                throw e;
            }
        }
    }

    /**
     * @return url prefix for resource, including base url.
     */
    public String getResourceUrlPrefix(String hash, String version, boolean isAbsolute) {
        return urlProvider.getStaticResourcePrefix(hash, version, (isAbsolute ? UrlMode.ABSOLUTE : UrlMode.RELATIVE))
                + "/" + AbstractFileServerServlet.SERVLET_PATH;
    }

    /**
     * @return url prefixed with CDN prefix.
     */
    public String getResourceCdnPrefix(final String url) {
        return getWebResourceMapper().mapSingle(url).orElseGet(() -> integration.getCDNStrategy().transformRelativeUrl(url));
    }

    /**
     * @return content type for given path.
     */
    public String getContentType(String path) {
        return contentTypeResolver.getContentType(path);
    }

    /**
     * @return content annotators for given type.
     */
    public ResourceContentAnnotator getContentAnnotator(String type) {
        List<ResourceContentAnnotator> annotators = new ArrayList<>();
        if (JS_TYPE.equals(type)) {
            annotators.add(new SemicolonResourceContentAnnotator());
            if (isJavaScriptTryCatchWrappingEnabled()) {
                annotators.add(new TryCatchJsResourceContentAnnotator());
            }
            if (useAsyncAttributeForScripts()) {
                annotators.add(new AsyncLoaderAnnotator());
            }
            annotators.add(new LocationContentAnnotator());
            annotators.add(new ModuleAnnotator());
        } else if (CSS_TYPE.equals(type)) {
            annotators.add(new LocationContentAnnotator());
        }
        return new ListOfAnnotators(annotators);
    }

    /**
     * Use async attribute for script tags.
     */
    public boolean useAsyncAttributeForScripts() {
        return integration.useAsyncAttributeForScripts();
    }

    /**
     * @since 3.5.27
     */
    private List<CompleteWebResourceKey> getSyncWebResourceKeys() {
        return new ArrayList<>(MoreObjects.firstNonNull(integration.getSyncWebResourceKeys(), emptyList()));
    }

    /**
     * @return static transformers.
     */
    public StaticTransformers getStaticTransformers() {
        return staticTransformers;
    }

    /**
     * If the key is context or web resource key.
     */
    public static boolean isContextKey(String key) {
        return key.contains(CONTEXT_PREFIX);
    }

    /**
     * Convert virtual context key to web resource key.
     */
    public static String virtualContextKeyToWebResourceKey(String virtualContextKey) {
        return virtualContextKey.replace(CONTEXT_PREFIX + ":", "");
    }


    /**
     * Analyzes current state of the plugin system and return web resources.
     */
    protected void ensureNoLegacyStuff(Snapshot snapshot) {
        if (integration.forbidCondition1AndTransformer1()) {
            // Checking for legacy conditions.
            List<String> resourcesWithLegacyConditions = new LinkedList<>();
            for (Bundle bundle : snapshot.getWebResourcesWithLegacyConditions()) {
                if (!integration.allowedCondition1Keys().contains(bundle.getKey())) {
                    resourcesWithLegacyConditions.add(bundle.getKey());
                }
            }

            // Checking for legacy transformers.
            List<String> resourcesWithLegacyTransformers = new LinkedList<>();
            for (Bundle bundle : snapshot.getWebResourcesWithLegacyTransformers()) {
                if (!integration.allowedTransform1Keys().contains(bundle.getKey())) {
                    resourcesWithLegacyTransformers.add(bundle.getKey());
                }
            }

            if (resourcesWithLegacyConditions.size() > 0 || resourcesWithLegacyTransformers.size() > 0) {
                List<String> messages = new ArrayList<>();
                if (resourcesWithLegacyConditions.size() > 0) {
                    messages.add("legacy conditions: \"" + Joiner.on("\", \"").join(resourcesWithLegacyConditions) + "\"");
                }
                if (resourcesWithLegacyTransformers.size() > 0) {
                    messages.add("legacy transformers: \"" + Joiner.on("\", \"").join(resourcesWithLegacyTransformers) + "\"");
                }
                throw new ValidationException("there are web resources with " + Joiner.on(", and ").join(messages),
                        emptyList());
            }
        }
    }

    /**
     * @return true if sync context has been created (is not empty)
     * @since 3.5.27
     */
    public boolean isSyncContextCreated() {
        return syncContextCreated;
    }

    protected static class IntermediaryContextData {
        public List<String> dependencies = new ArrayList<>();
        public Date updatedAt;
        public String version = "";
        public List<String> moduleDependencies = new ArrayList<>();
    }

    /**
     * Analyzes current state of the plugin system and return web resources.
     */
    public Snapshot getWebResourcesWithoutCache() {
        final Map<WebResource, CachedTransformers> webResourcesTransformations = new HashMap<>();
        final Map<String, Bundle> cachedBundles = new HashMap<>();
        final Map<String, RootPage> rootPages = new HashMap<>();
        final Map<WebResource, CachedCondition> webResourcesCondition = new HashMap<>();
        final Map<String, Deprecation> webResourceDeprecationWarnings = new HashMap<>();
        final Set<WebResource> webResourcesWithLegacyConditions = new HashSet<>();
        final Set<WebResource> webResourcesWithLegacyTransformers = new HashSet<>();
        final Set<WebResource> webResourcesWithDisabledMinification = new HashSet<>();
        final Snapshot snapshot = new Snapshot(this, cachedBundles, rootPages, webResourcesTransformations,
                webResourcesCondition, webResourceDeprecationWarnings, webResourcesWithLegacyConditions,
                webResourcesWithLegacyTransformers, webResourcesWithDisabledMinification);

        final Map<String, IntermediaryContextData> intermediaryContexts = new HashMap<>();
        final ConditionInstanceCache conditionInstanceCache = new ConditionInstanceCache();

        // Collecting web resource descriptors.
        final List<WebResourceModuleDescriptor> webResourceDescriptors = integration.getPluginAccessor()
                .getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class);

        // Collect deprecation details before processing all descriptors further.
        // We need to collect all deprecation details beforehand, otherwise some usages of
        // deprecated code will not be discovered.
        for (WebResourceModuleDescriptor webResourceDescriptor : webResourceDescriptors) {
            if (webResourceDescriptor == null) {
                continue;
            }
            if (webResourceDescriptor.isDeprecated()) {
                final String completeKey = webResourceDescriptor.getCompleteKey();
                webResourceDeprecationWarnings.put(completeKey, webResourceDescriptor.getDeprecation());
            }
        }

        // Processing descriptors and building web resources graph.
        for (WebResourceModuleDescriptor webResourceDescriptor : webResourceDescriptors) {
            if (webResourceDescriptor == null) {
                continue;
            }
            // Checks if the current web-resource is tagged as a root-page
            boolean isRootPage = webResourceDescriptor.isRootPage();

            Plugin plugin = webResourceDescriptor.getPlugin();
            Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();

            final String completeKey = webResourceDescriptor.getCompleteKey();

            // Checking for legacy conditions.
            DecoratingCondition condition = webResourceDescriptor.getCondition();
            boolean hasLegacyConditions = (condition != null) && !condition.canEncodeStateIntoUrl();

            // Checking for legacy transformers.
            List<WebResourceTransformation> transformations = webResourceDescriptor.getTransformations();
            boolean hasLegacyTransformers = false;
            // Strictly speaking we should check the extension of the transformer,
            // because legacy CSS transformers would not affect JS.
            // But, it would hurt the performance to check transformers against all the resources in the web resource.
            // So, to make it faster we just check if there's any legacy transformer at all.
            for (WebResourceTransformation transformation : transformations) {
                if (!transformation.containsOnlyPureUrlReadingTransformers(transformerCache)) {
                    hasLegacyTransformers = true;
                    break;
                }
            }

            // Detecting location resource types, needed during URL generation to detect all the transformers
            // that should be applied.
            Map<String, Set<String>> locationResourceTypes = new HashMap<>();
            for (ResourceLocation resourceLocation : getResourceLocations(webResourceDescriptor,
                    integration.isCompiledResourceEnabled())) {
                String nameType = ResourceUtils.getType(resourceLocation.getName());
                String locationType = ResourceUtils.getType(resourceLocation.getLocation());
                String nameOrLocationType = nameType.isEmpty() ? locationType : nameType;
                Set<String> list = locationResourceTypes.get(nameOrLocationType);
                if (list == null) {
                    list = new HashSet<>();
                    locationResourceTypes.put(nameOrLocationType, list);
                }
                list.add(locationType);
            }

            // Adding dependencies, forbidding to use module and root-page names in web resource dependencies.
            List<String> dependencies = new ArrayList<>();
            for (String key : webResourceDescriptor.getDependencies()) {
                if (rootPages.containsKey(key)) {
                    String msg = "invalid dependency found for \"" + completeKey +
                            "\": \"" + key + "\" cannot be as a dependency because it is tagged as a root-page and " +
                            "will be ignored.";
                    supportLogger.error(msg);
                    continue;
                }
                if (!isWebResourceKey(key)) {
                    supportLogger.warn("the dependency \"" + key + "\" doesn't look like the key of the web resource and will be ignored.");
                    continue;
                }
                if (webResourceDeprecationWarnings.containsKey(key)) {
                    String msg = webResourceDeprecationWarnings.get(key).buildLogMessage() +
                            " (required by \"" + completeKey + "\")";
                    if (isDevMode()) {
                        supportLogger.warn(msg);
                    } else {
                        supportLogger.debug(msg);
                    }
                }
                dependencies.add(key);
            }

            // Adding module dependencies, forbidding to use web resource name in module dependencies.
            for (String key : webResourceDescriptor.getModuleDependencies()) {
                if (isWebResourceKey(key)) {
                    supportLogger.warn("the module dependency \"" + key + "\" look like the key of the web resource and will be ignored.");
                    continue;
                }
                dependencies.add(key);
            }

            // Adding context dependencies. At the moment this is allowed behaviour only for root pages, but
            // will be expanded to all web-resources once APDEX-923 is complete
            for (String key : webResourceDescriptor.getContextDependencies()) {
                if (!isRootPage) {
                    String msg = "ignoring dependency \"" + key + "\" in \"" + completeKey
                            + "\": context dependencies are only supported in root-pages at the moment";
                    supportLogger.error(msg);
                    continue;
                }
                if (isWebResourceKey(key)) {
                    supportLogger.warn("the context dependency \"" + key + "\" look like the key of the web resource and will be ignored.");
                    continue;
                }
                dependencies.add(CONTEXT_PREFIX + ":" + key);
            }

            // Adding web resource.
            TransformerParameters transformerParameters = new TransformerParameters(webResourceDescriptor.getPluginKey(), webResourceDescriptor.getKey(), null);
            final WebResource webResource = new WebResource(
                    snapshot,
                    completeKey,
                    dependencies,
                    updatedAt,
                    getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins),
                    true,
                    transformerParameters,
                    locationResourceTypes);
            cachedBundles.put(completeKey, webResource);

            // Adding root-page
            if (isRootPage) {
                RootPage rootPage = new RootPage(webResource);
                rootPages.put(completeKey, rootPage);
            }

            if (hasLegacyConditions) {
                webResourcesWithLegacyConditions.add(webResource);
            }
            if (hasLegacyTransformers) {
                webResourcesWithLegacyTransformers.add(webResource);
            }
            if (webResourceDescriptor.isDisableMinification()) {
                webResourcesWithDisabledMinification.add(webResource);
            }

            // Forbidding contexts on <root-page> web-resources, except for the implicit context that corresponds to the
            // resource itself
            if (isRootPage && webResourceDescriptor.getContexts().size() > 1) {
                String msg = "web-resource \"" + completeKey +
                        "\" cannot be added to a context because it's tagged as a root-page";
                supportLogger.error(msg);
                throw new RuntimeException(msg);
            }

            // Collecting dependencies and dates for contexts, we would need this information later when transform
            // contexts into
            // virtual web resources.
            for (String context : webResourceDescriptor.getContexts()) {
                if (!context.equals(completeKey)) {
                    String contextResourceKey = CONTEXT_PREFIX + ":" + context;
                    IntermediaryContextData contextData = intermediaryContexts.get(contextResourceKey);
                    if (contextData == null) {
                        contextData = new IntermediaryContextData();
                        intermediaryContexts.put(contextResourceKey, contextData);
                    }
                    contextData.dependencies.add(completeKey);
                }
            }

            // Adding conditions.
            if (condition != null) {
                // Trying to optimize conditions and replace multiple instances of the same UrlReadingCondition with
                // one instance.
                CachedCondition cachedCondition = conditionInstanceCache.intern(condition);
                webResourcesCondition.put(webResource, cachedCondition);
            }

            // Adding transformers.
            if (!transformations.isEmpty()) {
                webResourcesTransformations.put(webResource, new CachedTransformers(transformations));
            }
        }

        if (amdEnabled()) {
            parseModules(snapshot, cachedBundles, conditionInstanceCache, webResourcesCondition, webResourcesTransformations,
                    intermediaryContexts);
        }

        // Turning contexts into virtual resources.
        for (Map.Entry<String, IntermediaryContextData> entry : intermediaryContexts.entrySet()) {
            String contextResourceKey = entry.getKey();
            IntermediaryContextData contextData = entry.getValue();
            updateContextData(cachedBundles, contextData);
            cachedBundles.put(contextResourceKey, new Context(snapshot, contextResourceKey, contextData.dependencies,
                    contextData.moduleDependencies, contextData.updatedAt, contextData.version, true));
        }

        // APDEX-1045: Adding sync batch (always first in the DOM) as virtual resource.
        List<String> syncWebResourceKeys = getSyncWebResourceKeys().stream()
                .map(CompleteWebResourceKey::getCompleteKey)
                .collect(Collectors.toList());
        if (!syncWebResourceKeys.isEmpty()) {
            addSpecialContext(snapshot, cachedBundles, SYNCBATCH_KEY, syncWebResourceKeys);
            syncContextCreated = true;
        }

        // Adding super batch as virtual resource.
        if (isSuperBatchingEnabled()) {
            List<String> dependencies = new ArrayList<>(getBeforeAllResources());
            dependencies.addAll(batchingConfiguration.getSuperBatchModuleCompleteKeys());

            addSpecialContext(snapshot, cachedBundles, SUPERBATCH_KEY, dependencies);
        }

        // Calculating transformer used parameters.
        for (CachedTransformers cachedTransformers : webResourcesTransformations.values()) {
            cachedTransformers.computeParamKeys(transformerCache);
        }

        ensureNoLegacyStuff(snapshot);
        return snapshot;
    }

    protected void parseModules(Snapshot snapshot, Map<String, Bundle> cachedBundles,
                                ConditionInstanceCache conditionInstanceCache, Map<WebResource, CachedCondition> webResourcesCondition,
                                Map<WebResource, CachedTransformers> webResourcesTransformations, Map<String,
            IntermediaryContextData> intermediaryContexts) {
        List<ModulesDescriptor> modulesDescriptors = integration.getPluginAccessor()
                .getEnabledModuleDescriptorsByClass(ModulesDescriptor.class);

        // Preparing default transformers.
        List<WebResourceTransformation> defaultTransformations = new ArrayList<>();
        for (TransformationDto transformationDto : integration.getDefaultAmdModuleTransformers()) {
            defaultTransformations.add(new WebResourceTransformation(transformationDto));
        }

        // Parsing module descriptors.
        Map<String, Map<String, List<WebModuleDescriptor>>> groupedModuleDescriptorsList = new HashMap<>();
        for (ModulesDescriptor modulesDescriptor : modulesDescriptors) {
            Plugin plugin = modulesDescriptor.getPlugin();

            // Getting list of modules for the plugin.
            Map<String, List<WebModuleDescriptor>> groupedModuleDescriptors = groupedModuleDescriptorsList.get(plugin.getKey());
            if (groupedModuleDescriptors == null) {
                try {
                    groupedModuleDescriptors = parseAtlassianModules(plugin);
                } catch (RuntimeException e) {
                    supportLogger.error("can't parse atlassian-modules.xml for \"" + plugin.getKey() + "\"", e);
                    groupedModuleDescriptors = new HashMap<>();
                }
                groupedModuleDescriptorsList.put(plugin.getKey(), groupedModuleDescriptors);
            }
        }

        // Translating SOY names to AMD names.
        Map<String, String> soyNameToAmdName = new HashMap<>();
        for (Map<String, List<WebModuleDescriptor>> groupedModuleDescriptors : groupedModuleDescriptorsList.values()) {
            for (List<WebModuleDescriptor> moduleDescriptors : groupedModuleDescriptors.values()) {
                for (WebModuleDescriptor moduleDescriptor : moduleDescriptors) {
                    if (SOY_TYPE.equals(moduleDescriptor.srcType)) {
                        for (String soyTemplate : moduleDescriptor.soyTemplates) {
                            String existingModule = soyNameToAmdName.get(soyTemplate);
                            if (existingModule != null && !moduleDescriptor.name.equals(existingModule)) {
                                supportLogger.error("SOY template with same names exists in two different files " + existingModule + " and " + moduleDescriptor.name);
                                continue;
                            }
                            soyNameToAmdName.put(soyTemplate, moduleDescriptor.name);
                        }
                    }
                }
            }
        }

        // Assembling modules.
        for (ModulesDescriptor modulesDescriptor : modulesDescriptors) {
            // Getting plugin version and updatedAt date.
            Plugin plugin = modulesDescriptor.getPlugin();
            Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();
            String version = getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins);

            // Getting list of modules for modules descriptor.
            Map<String, List<WebModuleDescriptor>> groupedModuleDescriptors = groupedModuleDescriptorsList.get(plugin.getKey());
            List<WebModuleDescriptor> moduleDescriptors = groupedModuleDescriptors.get(modulesDescriptor.getDir());
            if (moduleDescriptors != null) {
                // Adding conditions.
                CachedCondition cachedCondition = null;
                if (useConditionsForWebModules()) {
                    DecoratingCondition condition = modulesDescriptor.getCondition();
                    if (condition != null) {
                        // Trying to optimize conditions and replace multiple instances of the same UrlReadingCondition with
                        // one instance.
                        cachedCondition = conditionInstanceCache.intern(condition);
                        if (cachedCondition.isLegacy()) {
                            throw new RuntimeException("legacy condition not allowed for " + plugin.getKey() + " "
                                    + modulesDescriptor.getDir() + " module!");
                        }
                    }
                }

                // Adding transformers.
                List<WebResourceTransformation> transformations = modulesDescriptor.getTransformations();
                for (WebResourceTransformation transformation : transformations) {
                    if (!transformation.containsOnlyPureUrlReadingTransformers(transformerCache)) {
                        throw new RuntimeException("legacy transformers not allowed for " + plugin.getKey() + " "
                                + modulesDescriptor.getDir() + " module!");
                    }
                }

                for (WebModuleDescriptor moduleDescriptor : moduleDescriptors) {
                    // Resolving and normalising dependencies.
                    LinkedHashMap<String, String> unresolvedDependencies = new LinkedHashMap<>();
                    List<String> dependencies = new ArrayList<>();
                    LinkedHashSet<String> webResourceDependencies = new LinkedHashSet<>();
                    for (Map.Entry<String, WebModuleDescriptor.Dependency> entry : moduleDescriptor.dependencies.entrySet()) {
                        String dependencyName = entry.getKey();
                        WebModuleDescriptor.Dependency dependency = entry.getValue();

                        // Removing `wr!` prefix to simplify fitting it into existing implementation.
                        if (WEB_RESOURCE_MODULE_LOADER.equals(dependency.loader)) {
                            webResourceDependencies.add(dependency.name);
                        }
                        // Not allowing to reference web resources without the `wr!` prefix.
                        else if (isWebResourceKey(dependency.name)) {
                            supportLogger.warn("module \"" + moduleDescriptor.name + "\" references web resource \"" +
                                    dependency.name + "\" without `wr!` prefix, dependency will be ignored!");
                            continue;
                        }
                        dependencies.add(nameAndLoaderToNameWithExtension(dependency.loader, dependency.resolvedName));

                        String resolvedNameWithLoader = nameAndLoaderToNameWithLoader(dependency.loader, dependency.resolvedName);
                        if (!dependency.name.equals(resolvedNameWithLoader)) {
                            unresolvedDependencies.put(dependencyName, resolvedNameWithLoader);
                        }
                    }

                    // Resolving SOY dependencies.
                    if (SOY_TYPE.equals(moduleDescriptor.srcType)) {
                        for (String soyDependency : moduleDescriptor.soyDependencies) {
                            String soyDependencyAmdName = soyNameToAmdName.get(soyDependency);
                            if (soyDependencyAmdName == null) {
                                // Temporarily disabled. It will print false warning about missing dependency if
                                // SOY template is not packaged as AMD module. In future when all SOY templates will
                                // be AMD modules it should be enabled.
                                // Support.LOGGER.error("can't resolve SOY dependency " + soyDependency + " for " + moduleDescriptor.name + " module!");
                                continue;
                            }

                            // No need to store dependency for himself, also filtering out duplicate dependencies.
                            if (!soyDependencyAmdName.equals(moduleDescriptor.name) && !dependencies.contains(soyDependencyAmdName)) {
                                dependencies.add(soyDependencyAmdName);
                            }
                        }
                    }

                    // Creating module.
                    // JavaScript modules should have name without extension, modules with loaders should have name with extension.
                    String moduleName = JS_TYPE.equals(moduleDescriptor.srcType) ? moduleDescriptor.baseName : moduleDescriptor.name;
                    String fileType = ResourceUtils.getType(moduleDescriptor.filePath);
                    Map<String, Set<String>> locationResourceTypes = new HashMap<>();
                    Set<String> types = new HashSet<>();
                    types.add(fileType);
                    locationResourceTypes.put(moduleDescriptor.type, types);
                    WebModule module = new WebModule(snapshot, plugin.getKey(), moduleName, moduleDescriptor.baseName, dependencies,
                            unresolvedDependencies, webResourceDependencies, updatedAt, version,
                            // Transformers should not know the name of the module, it's deliberately left empty.
                            new TransformerParameters(plugin.getKey(), "", moduleDescriptor.filePath),
                            moduleDescriptor.filePath,
                            moduleDescriptor.type,
                            moduleDescriptor.srcType,
                            moduleDescriptor.soyNamespace,
                            locationResourceTypes
                    );

                    // Adding to bundles.
                    cachedBundles.put(moduleName, module);

                    // Conditions.
                    if (useConditionsForWebModules()) {
                        if (cachedCondition != null) {
                            webResourcesCondition.put(module, cachedCondition);
                        }
                    }

                    // Transformers.
                    List<WebResourceTransformation> allTransformations = new ArrayList<>(defaultTransformations);
                    allTransformations.addAll(transformations);
                    if (!allTransformations.isEmpty()) {
                        webResourcesTransformations.put(module, new CachedTransformers(allTransformations));
                    }

                    // Collecting dependencies and dates for contexts, we would need this information later when transform
                    // contexts into
                    // virtual web resources.
                    for (String context : moduleDescriptor.contexts) {
                        String contextResourceKey = CONTEXT_PREFIX + ":" + context;
                        IntermediaryContextData contextData = intermediaryContexts.get(contextResourceKey);
                        if (contextData == null) {
                            contextData = new IntermediaryContextData();
                            intermediaryContexts.put(contextResourceKey, contextData);
                        }
                        contextData.dependencies.add(moduleName);
                        contextData.moduleDependencies.add(moduleName);
                    }
                }
            }
        }
    }

    public static Tuple<String, String> parseNameWithLoader(String name) {
        String[] parts = name.split("!");
        if (parts.length == 1) {
            return new Tuple<>(null, parts[0]);
        } else if (parts.length == 2) {
            return new Tuple<>(parts[0], parts[1]);
        } else {
            throw new RuntimeException("invalid AMD name \"" + name + "\"!");
        }
    }

    public static String nameAndLoaderToNameWithExtension(String loader, String name) {
        return loader == null || "js".equals(loader) || "wr".equals(loader) ? name : name + "." + loader;
    }

    public static String nameAndLoaderToNameWithLoader(String loader, String name) {
        return loader == null || "js".equals(loader) ? name : loader + "!" + name;

    }

    private void addSpecialContext(Snapshot snapshot, Map<String, Bundle> bundles, String key, List<String> dependencies) {
        IntermediaryContextData contextData = new IntermediaryContextData();
        contextData.updatedAt = new Date(0);
        contextData.dependencies = dependencies;
        updateContextData(bundles, contextData);
        bundles.put(key, new Bundle(snapshot, key, dependencies, contextData.updatedAt, contextData.version, true));
    }

    private void updateContextData(Map<String, Bundle> bundles, IntermediaryContextData contextData) {
        for (String dependency : contextData.dependencies) {
            Bundle bundle = bundles.get(dependency);
            if (null == bundle) {
                continue;
            }

            Date updatedAt = bundle.getUpdatedAt();
            String version = bundle.getVersion();

            if (contextData.updatedAt == null || contextData.updatedAt.before(updatedAt)) {
                contextData.updatedAt = updatedAt;
            }
            contextData.version = HashBuilder.buildHash(contextData.version, version);
        }
    }

    protected static class WebModuleDescriptor {
        protected static class Dependency {
            public final String name;
            public final String resolvedName;
            public final String loader;

            public Dependency(String name, String resolvedName, String loader) {
                this.name = name;
                this.resolvedName = resolvedName;
                this.loader = loader;
            }
        }

        public final String name;
        private final String baseName;
        public final String type;
        public final String srcType;
        public final String filePath;
        public final LinkedHashMap<String, Dependency> dependencies;
        public final List<String> contexts;
        public final List<String> soyTemplates;
        public final List<String> soyDependencies;
        public final String soyNamespace;

        public WebModuleDescriptor(String name, String baseName, String type, String srcType, String filePath,
                                   LinkedHashMap<String, Dependency> dependencies, List<String> contexts, List<String> soyTemplates,
                                   List<String> soyDependencies, String soyNamespace) {
            this.name = name;
            this.baseName = baseName;
            this.type = validateType(name, type);
            this.srcType = validateSrcType(name, srcType);
            this.filePath = filePath;
            this.dependencies = dependencies;
            this.contexts = contexts;
            this.soyTemplates = soyTemplates;
            this.soyDependencies = soyDependencies;
            this.soyNamespace = soyNamespace;
        }

        private static String validateType(String name, String type) {
            // Validation.
            if (JS_TYPE.equals(type) || CSS_TYPE.equals(type)) {
                return type;
            }
            throw new RuntimeException("invalid type for " + name + " it should be js or css but instead it's \"" + type + "\"");
        }

        private static String validateSrcType(String name, String srcType) {
            // Validation.
            if (srcType == null || StringUtils.isBlank(srcType)) {
                throw new RuntimeException("invalid srcType for " + name + "!");
            }
            return srcType;
        }
    }

    protected Map<String, List<WebModuleDescriptor>> parseAtlassianModules(Plugin plugin) {
        // Reading file.
        InputStream stream = plugin.getResourceAsStream(ATLASSIAN_MODULES_XML);
        if (stream == null) {
            supportLogger.error("no " + ATLASSIAN_MODULES_XML + " for \"" + plugin.getKey() + "\" plugin!");
            return new HashMap<>();
        }
        String xmlData;
        try {
            xmlData = CharStreams.toString(new InputStreamReader(stream, Charsets.UTF_8));
        } catch (IOException e) {
            supportLogger.error("can't read " + ATLASSIAN_MODULES_XML + " for " + plugin.getKey(), e);
            return new HashMap<>();
        }

        // Parsing xml.
        Element modulesEl = parseXml(xmlData);

        int version = Integer.parseInt(modulesEl.attributeValue("version"));
        if (version != ATLASSIAN_MODULES_VERSION) {
            supportLogger.error("wrong version of atlassian-modules.xml for \"" + plugin.getKey() + "\"");
            return new HashMap<>();

        }

        Map<String, List<WebModuleDescriptor>> groupedModuleDescriptors = new HashMap<>();
        for (Object moduleElAsObject : modulesEl.elements()) {
            Element moduleEl = (Element) moduleElAsObject;

            // Dependencies.
            List<Element> dependencyEls = moduleEl.elements("dependency");
            LinkedHashMap<String, WebModuleDescriptor.Dependency> dependencies = new LinkedHashMap<>();
            for (Element dependency : dependencyEls) {
                String name = dependency.getTextTrim();
                dependencies.put(name, new WebModuleDescriptor.Dependency(
                        name, dependency.attributeValue("resolved-name"), dependency.attributeValue("loader")
                ));
            }

            // Contexts.
            List<Element> contextEls = moduleEl.elements("context");
            List<String> contexts = new ArrayList<>();
            for (Element context : contextEls) {
                contexts.add(context.getTextTrim());
            }

            // SOY templates.
            List<Element> soyTemplateEls = moduleEl.elements("soy-template");
            List<String> soyTemplates = new ArrayList<>();
            for (Element soyTemplate : soyTemplateEls) {
                soyTemplates.add(soyTemplate.getTextTrim());
            }

            // SOY dependencies.
            List<Element> soyDependencyEls = moduleEl.elements("soy-dependency");
            List<String> soyDependencies = new ArrayList<>();
            for (Element soyDependency : soyDependencyEls) {
                soyDependencies.add(soyDependency.getTextTrim());
            }

            String modulesDescriptorDir = moduleEl.attributeValue("modules-descriptor-dir");
            List<WebModuleDescriptor> modules = groupedModuleDescriptors.get(modulesDescriptorDir);
            if (modules == null) {
                modules = new ArrayList<>();
                groupedModuleDescriptors.put(modulesDescriptorDir, modules);
            }
            modules.add(new WebModuleDescriptor(moduleEl.attributeValue("name"), moduleEl.attributeValue("base-name"),
                    moduleEl.attributeValue("type"), moduleEl.attributeValue("src-type"), moduleEl.attributeValue("file-path"),
                    dependencies, contexts, soyTemplates, soyDependencies, moduleEl.attributeValue("soy-namespace")));
        }
        return groupedModuleDescriptors;
    }


    /**
     * Queries the plugin system and return list of Resources for Web Resource.
     */
    public LinkedHashMap<String, Resource> getResourcesWithoutCache(Bundle bundle) {
        // Adding resources, it is important to use `LinkedHashMap` because order of resources is important.
        LinkedHashMap<String, Resource> resources = new LinkedHashMap<>();

        WebResourceModuleDescriptor webResourceDescriptor = getWebResourceModuleDescriptor(bundle.getKey());
        if (webResourceDescriptor != null) {
            for (ResourceLocation resourceLocation : getResourceLocations(webResourceDescriptor,
                    integration.isCompiledResourceEnabled())) {
                Resource resource = buildResource(bundle, resourceLocation);
                resources.put(resource.getName(), resource);
            }
        }
        return resources;
    }

    protected static List<ResourceLocation> getResourceLocations(WebResourceModuleDescriptor webResourceModuleDescriptor,
                                                                 boolean compiledResourcesEnabled) {
        List<ResourceLocation> resourceDescriptors = new ArrayList<>();
        for (ResourceDescriptor resourceDescriptor : webResourceModuleDescriptor.getResourceDescriptors()) {
            if (DOWNLOAD_PARAM_VALUE.equals(resourceDescriptor.getType())) {
                ResourceLocation resourceLocation = resourceDescriptor.getResourceLocationForName(null);

                // Perform boolean check before checking for XML property.
                if (compiledResourcesEnabled) {
                    String compiledLocation = resourceDescriptor.getParameter(COMPILED_RESOURCE_LOCATION_PARAMETER);
                    if (compiledLocation != null) {
                        ResourceLocation newLocation = new ResourceLocation(compiledLocation, resourceLocation.getName(),
                                resourceLocation.getType(), resourceLocation.getContentType(), resourceLocation.getContent(),
                                resourceLocation.getParams());
                        resourceLocation = newLocation;
                    }
                }
                resourceDescriptors.add(resourceLocation);
            }
        }
        return resourceDescriptors;
    }

    /**
     * Get data resources for web resource.
     */
    public LinkedHashMap<String, Jsonable> getWebResourceData(String key) {
        // Adding resources, it is important to use `LinkedHashMap` because order of resources is important.
        LinkedHashMap<String, Jsonable> data = new LinkedHashMap<>();
        WebResourceModuleDescriptor webResourceDescriptor = getWebResourceModuleDescriptor(key);
        if (webResourceDescriptor != null) {
            for (Map.Entry<String, WebResourceDataProvider> entry : webResourceDescriptor.getDataProviders().entrySet()) {
                data.put(entry.getKey(), entry.getValue().get());
            }
        }
        return data;
    }

    private WebResourceModuleDescriptor getWebResourceModuleDescriptor(String key) {
        ModuleDescriptor<?> moduleDescriptor;
        try {
            moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(key);
        } catch (RuntimeException e) {
            moduleDescriptor = null;
        }
        if (moduleDescriptor == null) {
            return null;
        }
        if (!(moduleDescriptor instanceof WebResourceModuleDescriptor)) {
            return null;
        }
        return (WebResourceModuleDescriptor) moduleDescriptor;
    }

    /**
     * Returns stream for file with given location for given resource.
     */
    public InputStream getStreamFor(Resource resource, String path) {
        if (resource instanceof ModuleResource) {
            return getPlugin(((ModuleResource) resource).getPluginKey()).getResourceAsStream(path);
        } else {
            ResourceLocation resourceLocation = resource.getResourceLocation();
            String sourceParam = resourceLocation.getParameter(SOURCE_PARAM_NAME);
            final boolean isWebContextStatic = "webContextStatic".equalsIgnoreCase(sourceParam);

            if (isWebContextStatic) {
                // Tomcat 8 requires resource path to start with the slash.
                String pathWithSlash = path.startsWith("/") ? path : "/" + path;
                return servletContextFactory.getServletContext().getResourceAsStream(pathWithSlash);
            } else {
                return getPlugin(resource.getKey()).getResourceAsStream(path);
            }
        }
    }

    private Plugin getPlugin(String key) {
        // Detecting if it's a web resource key or plugin key.
        if (key.contains(":")) {
            ModuleDescriptor<?> moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(key);
            return checkNotNull(moduleDescriptor.getPlugin());
        } else {
            return checkNotNull(integration.getPluginAccessor().getEnabledPlugin(key));
        }
    }

    /**
     * If minification enabled.
     */
    public boolean isMinificationEnabled() {
        return !(Boolean.getBoolean(DISABLE_MINIFICATION) || isDevMode());
    }

    /**
     * Helper to hide bunch of integration code and simplify resource creation.
     */
    protected Resource buildResource(Bundle bundle, ResourceLocation resourceLocation) {
        // The code based on `SingleDownloadableResourceFinder.getDownloadablePluginResource`.
        return new Resource(bundle, resourceLocation,
                ResourceUtils.getType(resourceLocation.getName()),
                ResourceUtils.getType(resourceLocation.getLocation())
        );
    }

    /**
     * Get not declared Resource.
     */
    public Resource getModuleResource(String completeKey, String name) {
        // Confluence throws error if trying to query module with not complete key (with the plugin key for example).
        if (!isWebResourceKey(completeKey)) {
            return null;
        }
        ModuleDescriptor<?> moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(completeKey);
        // In case of virtual context or super batch resource there would be null returned.
        if (moduleDescriptor == null) {
            return null;
        }
        ResourceLocation resourceLocation = moduleDescriptor.getResourceLocation(DOWNLOAD_PARAM_VALUE, name);
        if (resourceLocation == null) {
            return null;
        }
        Plugin plugin = moduleDescriptor.getPlugin();
        Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();
        PluginResourceContainer resourceContainer = new PluginResourceContainer(new Snapshot(this), completeKey,
                updatedAt, getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
        return buildResource(resourceContainer, resourceLocation);
    }

    /**
     * Get Resource for Plugin.
     */
    public Resource getPluginResource(String pluginKey, String name) {
        Plugin plugin = integration.getPluginAccessor().getPlugin(pluginKey);
        if (plugin == null) {
            return null;
        }
        ResourceLocation resourceLocation = plugin.getResourceLocation(DOWNLOAD_PARAM_VALUE, name);
        if (resourceLocation == null) {
            return null;
        }
        PluginResourceContainer resourceContainer = new PluginResourceContainer(new Snapshot(this), pluginKey,
                plugin.getDateLoaded(), getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
        Resource resource = buildResource(resourceContainer, resourceLocation);
        return resource;
    }

    public boolean optimiseSourceMapsForDevelopment() {
        return batchingConfiguration.optimiseSourceMapsForDevelopment();
    }

    public boolean isSuperBatchingEnabled() {
        return batchingConfiguration.isSuperBatchingEnabled();
    }

    public boolean isBatchContentTrackingEnabled() {
        return batchingConfiguration.isBatchContentTrackingEnabled();
    }

    public boolean isContextBatchingEnabled() {
        return batchingConfiguration.isContextBatchingEnabled();
    }

    public boolean isWebResourceBatchingEnabled() {
        return batchingConfiguration.isPluginWebResourceBatchingEnabled();
    }

    /**
     * If incremental cache enabled.
     */
    public boolean isIncrementalCacheEnabled() {
        return integration.isIncrementalCacheEnabled();
    }

    /**
     * If `completeKey` is Web Resource key or Plugin key.
     */
    public static boolean isWebResourceKey(String completeKey) {
        return completeKey.contains(":");
    }

    /**
     * Hash code representing the state of config. State of the system could change not only on plugin system changes
     * but also some properties of config could change too. In such cases the cache additionally checked against the
     * version of config it's build for.
     * <p>
     * Strictly speaking this hash should include the full state of config, but it would be too slow, so instead we
     * check only for some of its properties.
     */
    public int partialHashCode() {
        return integration.getSuperBatchVersion().hashCode();
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public WebResourceIntegration getIntegration() {
        return integration;
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public TransformerCache getTransformerCache() {
        return transformerCache;
    }

    public boolean isUrlGenerationCacheEnabled() {
        return true;
    }

    public int getUrlCacheSize() {
        return urlCacheSize;
    }

    @Deprecated
    public ResourceBatchingConfiguration getBatchingConfiguration() {
        return batchingConfiguration;
    }

    public boolean isDeferJsAttributeEnabled() {
        return integration.isDeferJsAttributeEnabled();
    }

    public TimeSpan getDefaultBigPipeDeadline() {
        return integration.getBigPipeConfiguration().getDefaultBigPipeDeadline();
    }

    public boolean getBigPipeDeadlineDisabled() {
        return integration.getBigPipeConfiguration().getBigPipeDeadlineDisabled();
    }


    /**
     * Represents product state considering everything that contributes to content of resources.
     *
     * @since v3.5.0
     */
    public String computeGlobalStateHash() {
        // This needs to represent all the things that contribute
        // to the content of files or contribute to what
        // requireContext() or requireResource() would result in
        List<String> state = new LinkedList<>();
        state.add("productver");
        state.add(integration.getHostApplicationVersion());

        // annotators impact content of files
        state.add("annotators");
        state.add(String.valueOf(getContentAnnotator(JS_TYPE).hashCode()));
        state.add(String.valueOf(getContentAnnotator(CSS_TYPE).hashCode()));

        state.add("plugins");
        List<Plugin> plugins = new ArrayList<>(integration.getPluginAccessor().getEnabledPlugins());
        sort(plugins, consistentPluginOrder());
        for (Plugin plugin : plugins) {
            state.add(plugin.getKey());
            state.add(getPluginVersionOrInstallTime(plugin, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
        }

        String globalProductStateHash = HashBuilder.buildHash(state);
        log.info("Calculated global state hash {} based on {}", globalProductStateHash, state);

        return globalProductStateHash;

    }

    private Comparator<Plugin> consistentPluginOrder() {
        return Comparator.<Plugin, String>comparing(p -> p.getKey() + "-" + getPluginVersionOrInstallTime(p, usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins));
    }

    /**
     * If conditions should be enabled for AMD modules.
     */
    public boolean useConditionsForWebModules() {
        return false;
    }

    public List<String> getBeforeAllResources() {
        return amdEnabled() ? BEFORE_ALL_AMD_RESOURCES : emptyList();
    }

    /**
     * Returns a URL transformer to allow clients to map resource URLs as in {@link #getResourceCdnPrefix(String)}.
     *
     * @since v3.5.0
     */
    public CdnResourceUrlTransformer getCdnResourceUrlTransformer() {
        return cdnResourceUrlTransformer;
    }

    /**
     * Determines if pre-baker REST API is enabled based on a System Property..
     *
     * @since v3.5.0
     */
    public boolean isPreBakeEnabled() {
        return Boolean.getBoolean(PREBAKE_FEATURE_ENABLED);
    }

    /**
     * Determines if batch URLs contexts' are sorted to ensure that the same URL is generated no matter what order the
     * contexts are required in.
     *
     * @since 3.6.0
     */
    public static boolean isStaticContextOrderEnabled() {
        return Boolean.getBoolean(STATIC_CONTEXT_ORDER_ENABLED);
    }

    /**
     * Determines if global minification of resources is enabled based on a System Property.
     *
     * @since v3.6.0
     */
    public boolean isGlobalMinificationEnabled() {
        return isMinificationEnabled() && Boolean.getBoolean(GLOBAL_MINIFICATION_ENABLED);
    }

    /**
     * Returns the CDN base URL to prefix all pre-baked resources mapped with {@link WebResourceMapper}.
     *
     * @since v3.5.0
     */
    public String getCtCdnBaseUrl() {
        return System.getProperty(CT_CDN_BASE_URL);
    }

    public ResourceCompiler getResourceCompiler() {
        return resourceCompiler;
    }

    public void runResourceCompilation(Snapshot snapshot) {
        final Set<CompilerEntry> compilerEntries = new LinkedHashSet<>();
        RequestCache requestCache = new RequestCache(null);
        snapshot.forEachBundle(bundle -> {
            LinkedHashMap<String, Resource> resources = bundle.getResources(requestCache);
            if (MapUtils.isNotEmpty(resources)) {
                List<CompilerEntry<String>> entries = resources
                        .values()
                        .stream()
                        .filter(res -> res.getNameOrLocationType().equals(Config.JS_TYPE)) // Only JS files
                        .flatMap(resource -> {
                            try (InputStream in = getStreamFor(resource, resource.getPath())) {
                                return in != null ?
                                        Stream.of(CompilerEntry.ofKeyValue(
                                                resource.getPath(),
                                                IOUtils.toString(in, CompilerUtil.CHARSET.name()))) : Stream.empty();
                            } catch (IOException e) {
                                supportLogger.warn(String.format("Error compiling %s", resource.getKey()), e);
                            }
                            return Stream.empty();
                        })
                        .collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(entries)) {
                    compilerEntries.addAll(entries);
                }
            }
        });
        resourceCompiler.compile(compilerEntries.stream());
    }

    public boolean resplitMergedContextBatchesForThisRequest() {
        return batchingConfiguration.resplitMergedContextBatchesForThisRequest();
    }

    public boolean amdEnabled() {
        return integration.amdEnabled();
    }

    /**
     * Returns the current instance of {@link WebResourceMapper} or no-op when
     * {@link WebResourceIntegration#isCtCdnMappingEnabled()} is {@code false}.
     *
     * @since v3.5.7
     */
    public WebResourceMapper getWebResourceMapper() {
        return cdnUrlMapper.get();
    }

    /**
     * Forces the creation of a new instance of {@link WebResourceMapper}, resetting the current reference to it.
     * If the new instance is {@link NoOpWebResourceMapper} and {@link NoOpWebResourceMapper#reason()} returns an
     * exception, then this exception is thrown.
     *
     * @throws Exception Reason why NoOpWebResourceMapper and not an operational instance was created.
     * @since v3.5.7
     */
    public void reloadWebResourceMapper() throws Exception {
        cdnUrlMapper.reset();
        WebResourceMapper wrm = cdnUrlMapper.get();
        if (wrm instanceof NoOpWebResourceMapper) {
            NoOpWebResourceMapper noOpWebResourceMapper = (NoOpWebResourceMapper) wrm;
            if (noOpWebResourceMapper.reason().isPresent()) {
                throw noOpWebResourceMapper.reason().get();
            }
        }
    }

    public boolean isJavaScriptTryCatchWrappingEnabled() {
        return batchingConfiguration.isJavaScriptTryCatchWrappingEnabled();
    }

    /**
     * For SNAPSHOT plugins will return install time instead of the version.
     * Needed to simplify working with SNAPSHOT plugins and flushing cache when new SNAPSHOT plugin installed
     * with same version but different content.
     */
    public static String getPluginVersionOrInstallTime(Plugin plugin, boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins) {
        PluginInformation pluginInfo = plugin.getPluginInformation();
        // Seems like we need to guard against pluginInfo being null, this check has been taken from
        // `WebResourceUrlProviderImpl.getStaticPluginResourceUrl` in WRM@3.5.9.
        final String version = pluginInfo != null ? pluginInfo.getVersion() : "unknown";
        if (usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins && SNAPSHOT_PLUGIN_REGEX.matcher(version).find()) {
            final Date loadedAt = plugin.getDateLoaded() == null ? new Date() : plugin.getDateLoaded();
            return version + "-" + loadedAt.getTime();
        } else {
            return version;
        }
    }

    /**
     * Returns true if CSS prebaking is enabled, false otherwise
     *
     * @return true if CSS prebaking is enabled, false otherwise
     * @since v3.5.13
     */
    public boolean isCSSPrebakingEnabled() {
        return Boolean.getBoolean(PREBAKE_CSS_RESOURCES);
    }

    /**
     * This property indicates if resources tainted only with
     * {@link com.atlassian.webresource.api.assembler.resource.PrebakeWarning}s should be considered for prebaking.
     * <p>
     * <p> It is important to note that resources tainted with
     * {@link com.atlassian.webresource.api.assembler.resource.PrebakeError}s will never be considered safe for
     * prebaking, even when this property is set.
     *
     * @return true to allow prebaking of resources tainted with
     * {@link com.atlassian.webresource.api.assembler.resource.PrebakeWarning}s, false otherwise
     * @since v3.5.23
     */
    public boolean ignorePrebakeWarnings() {
        return Boolean.getBoolean(IGNORE_PREBAKE_WARNINGS);
    }

    /**
     * If URL caching enabled, used mostly to simplify debugging.
     */
    public boolean isUrlCachingEnabled() {
        return isUrlCachingEnabled;
    }

    void setLogger(Logger logger) {
        supportLogger = logger;
    }
}
