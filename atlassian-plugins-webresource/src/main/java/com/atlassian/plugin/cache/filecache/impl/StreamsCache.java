package com.atlassian.plugin.cache.filecache.impl;

import com.atlassian.plugin.cache.filecache.Cache;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Base class for caching streams, mostly contains concurrency related logic, the file operations implemented
 * in children.
 *
 * @since v3.3
 */
public abstract class StreamsCache {
    static enum State {
        UNCACHED, CACHED, NEEDSDELETE, DELETED
    }

    protected static interface Callback {
        void apply();
    }

    protected static interface CallbackWithBooleanReturn {
        boolean apply();
    }

    private Logger log = LoggerFactory.getLogger(OneStreamCache.class);

    private static final String CLIENT_ABORT_EXCEPTION = "org.apache.catalina.connector.ClientAbortException";

    // guards read/writes to `concurrentCount` and `state`.
    private final Object lock = new Object();
    private int concurrentCount;
    private State state = State.UNCACHED;

    public void deleteWhenPossible(Callback callback) {
        synchronized (lock) {
            if (state == State.UNCACHED) {
                state = State.DELETED;
            } else if (state == State.CACHED) {
                state = State.NEEDSDELETE;
            }

            if (state == State.NEEDSDELETE && concurrentCount == 0) {
                callback.apply();
                state = State.DELETED;
            }
        }
    }

    public abstract void stream(OutputStream out, Cache.StreamProvider provider);

    public abstract void streamTwo(OutputStream out1, OutputStream out2, Cache.TwoStreamProvider provider);

    public abstract void deleteWhenPossible();

    /**
     * Increments the concurrent count, and returns if it is okay
     * to read from the cached file, or not.
     * If necessary, it will create the cached file first.
     * doExit() must be called iff this method returns normally
     */
    protected boolean doEnter(Callback streamCallback, CallbackWithBooleanReturn checkFilesCallback) {
        synchronized (lock) {
            boolean useCache = false;

            if (state == State.UNCACHED) {
                useCache = tryStreamCallback(streamCallback);
            } else if (state == State.CACHED) {
                final boolean isFileStillValid = checkFilesCallback.apply();
                if (!isFileStillValid) {
                    useCache = tryStreamCallback(streamCallback);
                } else {
                    useCache = true;
                }
            } else if (state == State.NEEDSDELETE) {
                useCache = true;
            }

            concurrentCount++;
            return useCache;
        }
    }

    private boolean tryStreamCallback(final Callback streamCallback) {
        try {
            streamCallback.apply();
            state = State.CACHED;
            return true;
        } catch (Exception e) {
            log.warn("Problem caching to disk, skipping cache for this entry", e);
            state = State.UNCACHED;
            return false;
        }
    }

    /**
     * Decrements the concurrent count. Will delete the cache if requested, and there are no
     * other concurrent users.
     */
    protected void doExit(Callback callback) {
        synchronized (lock) {
            concurrentCount--;
            if (state == State.NEEDSDELETE && concurrentCount == 0) {
                callback.apply();
                state = State.DELETED;
            }
        }
    }

    protected static void streamFromFile(File file, OutputStream out) {
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            IOUtils.copyLarge(in, out);
            out.flush();
        } catch (IOException e) {
            // we don't care about the Tomcat's ClientAbortException as it only tells us that
            // a client closed the connection before we were able to send the resource
            if (!e.getClass().getName().equals(CLIENT_ABORT_EXCEPTION)) {
                throw new RuntimeException(e);
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    // This method needed for tests only.
    protected OutputStream createWriteStream(File file) throws FileNotFoundException {
        return new FileOutputStream(file);
    }

    /**
     * Needed for tests.
     */
    public void setLogger(Logger log) {
        this.log = log;
    }
}