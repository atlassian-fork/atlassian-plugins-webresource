package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.SemicolonResourceContentAnnotator;
import junit.framework.TestCase;

import java.io.ByteArrayOutputStream;

public class TestSemicolonResourceContentAnnotator extends TestCase {

    public void testSemicolonIsAlwaysAppendedAndPrepended() throws Exception {
        ResourceContentAnnotator annotator = new SemicolonResourceContentAnnotator();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        annotator.beforeResourceInBatch(null, null, null, stream);
        annotator.afterResourceInBatch(null, null, null, stream);

        String script = new String(stream.toByteArray());
        assertTrue(script.startsWith(";"));
        assertTrue(script.endsWith(";"));
    }
}
