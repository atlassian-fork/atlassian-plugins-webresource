package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Util;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

public class AddLocationStatic implements StaticTransformers {

    private static final String LOCATION_QUERY_PARAMETER = "location";

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty()
                .andExactly(LOCATION_QUERY_PARAMETER, String.valueOf(true))
                .andAbsent(LOCATION_QUERY_PARAMETER);
    }

    @Override
    public Dimensions computeBundleDimensions(Bundle bundle) {
        return computeDimensions();
    }

    @Override
    public void addToUrl(String locationType, TransformerParameters transformerParameters, UrlBuilder urlBuilder, UrlBuildingStrategy urlBuildingStrategy) {
        urlBuilder.addToQueryString(LOCATION_QUERY_PARAMETER, "true");
    }

    @Override
    public Content transform(Content content, TransformerParameters transformerParameters, ResourceLocation resourceLocation, String filePath,
                             QueryParams queryParams, String sourceUrl) {

        return new ContentImpl(content.getContentType(), true) {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                try {
                    if ("true".equals(queryParams.get(LOCATION_QUERY_PARAMETER))) {
                        out.write((LOCATION_QUERY_PARAMETER + "=" + transformerParameters.getPluginKey() + ":" +
                                transformerParameters.getModuleKey() + "/" + resourceLocation.getName() + " (static)\n").getBytes());
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                SourceMap sourceMap = content.writeTo(out, isSourceMapEnabled);
                if (isSourceMapEnabled && (sourceMap != null)) {
                    sourceMap = Util.offset(sourceMap, 1);
                }
                return sourceMap;
            }
        };
    }

    @Override
    public Set<String> getParamKeys() {
        Set<String> paramKeys = new HashSet<>();
        paramKeys.add(LOCATION_QUERY_PARAMETER);
        return paramKeys;
    }
}