package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceManagerImpl;

import java.util.List;

// Web Resource stub using deprecated implementation.
public class WebResourceImplDeprecated extends WebResourceImpl {
    private WebResourceManager webResourceManager;

    @Override
    protected void initialize(ConfigurationData configurationData) {
        super.initialize(configurationData);
        webResourceManager = new WebResourceManagerImpl(resourceLocator, integration, urlProvider, batchingConfiguration);
    }

    @Override
    public void requireResource(String key) {
        webResourceManager.requireResource(key);
    }

    @Override
    public void requireContext(String key) {
        webResourceManager.requireResourcesForContext(key);
    }

    @Override
    public void include(List<String> keys) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void exclude(List<String> keys) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void requireData(String key, String value) {
        pageBuilderService.assembler().data().requireData(key, value);
    }

    // Returns HTML that should be included in the head of the page to include all the required resources.
    @Override
    public String pathsAsHtml() {
        return webResourceManager.getRequiredResources(UrlMode.AUTO);
    }
}