package com.atlassian.plugin.webresource.integration;

import org.junit.Test;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.junit.Assert.assertThat;

public class TestAsyncScriptAttribute extends TestCase {
    @Test
    public void shouldServeAsyncScriptsForContextBatch() {
        wr.configure()
                .useAsync(true)
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:a")
                .context("general")
                .resource("b1.js")
                .end();

        wr.requireContext("general");

        String pathsAsHtml = wr.pathsAsHtml();
        assertThat(pathsAsHtml, matches(
                "async"
        ));

        String url = contextBatchUrl("general", "js", buildMap("async", "true"));
        assertThat(pathsAsHtml, matches(
                url
        ));
        assertThat(wr.getContent(url), matches(
                loaderBegin(),
                "content of a1",
                "content of b1",
                loaderEnd())
        );
    }

    @Test
    public void shouldServeAsyncScriptsForWebResourceBatch() {
        wr.configure()
                .useAsync(true)
                .disableContextBatching()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:a")
                .context("general")
                .resource("b1.js")
                .end();

        wr.requireContext("general");

        String pathsAsHtml = wr.pathsAsHtml();
        assertThat(pathsAsHtml, matches(
                "async",
                "async"
        ));

        String aUrl = webResourceBatchUrl("plugin:a", "js", buildMap("async", "true"));
        String bUrl = webResourceBatchUrl("plugin:b", "js", buildMap("async", "true"));
        assertThat(pathsAsHtml, matches(
                aUrl,
                bUrl
        ));

        assertThat(wr.getContent(aUrl), matches(
                loaderBegin(),
                "content of a1",
                loaderEnd()
        ));
    }

    @Test
    public void shouldServeAsyncScriptsForResources() {
        wr.configure()
                .useAsync(true)
                .disableContextBatching()
                .disableWebResourceBatching()
                .plugin("plugin")
                .webResource("a")
                .resource("a1.js")
                .webResource("b")
                .dependency("plugin:a")
                .context("general")
                .resource("b1.js")
                .end();

        wr.requireContext("general");

        String pathsAsHtml = wr.pathsAsHtml();
        assertThat(pathsAsHtml, matches(
                "async",
                "async"
        ));

        String aUrl = resourceUrl("plugin:a", "a1.js", buildMap("async", "true"));
        String bUrl = resourceUrl("plugin:b", "b1.js", buildMap("async", "true"));
        assertThat(pathsAsHtml, matches(
                aUrl,
                bUrl
        ));

        assertThat(wr.getContent(aUrl), matches(
                loaderBegin(),
                "content of a1",
                loaderEnd()
        ));
    }

    private static String loaderBegin() {
        return "WRM.InOrderLoader.define(function";
    }

    private static String loaderEnd() {
        return "});";
    }
}