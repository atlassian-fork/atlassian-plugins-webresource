package com.atlassian.plugin.webresource.integration.stub.dsl;

import com.atlassian.plugin.webresource.integration.stub.WebResource;

public class ModuleDsl {
    private boolean submitted = false;
    private ModulesDsl webModulesDsl;
    private final ModuleData data;

    public ModuleDsl(ModulesDsl webModulesDsl, String modulesDescriptorDir, String name, String content) {
        this.webModulesDsl = webModulesDsl;
        data = new ModuleData(modulesDescriptorDir, name, name, content);
    }

    public ModuleDsl module(String name) {
        submit();
        return webModulesDsl.module(name);
    }

    public ModuleDsl module(String name, String content) {
        submit();
        return webModulesDsl.module(name, content);
    }

    public ModuleDsl dependency(String name, String resolved) {
        data.dependencies.put(name, new ModuleData.DependencyData(name, resolved));
        return this;
    }

    public ModuleDsl dependency(String name) {
        String resolved = name.replace("./", "").replaceAll("^.*!", "");
        return dependency(name, resolved);
    }

    private void submit() {
        if (submitted) {
            return;
        }
        submitted = true;
        webModulesDsl.data.modules.add(data);
    }

    public WebResource.WebResourceDsl webResource(String key) {
        submit();
        return webModulesDsl.webResource(key);
    }

    public void end() {
        submit();
        webModulesDsl.end();
    }

    public WebResource.PluginDsl plugin(String key) {
        submit();
        return webModulesDsl.plugin(key);
    }

    public ModulesDsl modules(String filePath) {
        submit();
        return webModulesDsl.modules(filePath);
    }

    public ModuleDsl soyTemplate(String template) {
        data.soyTemplates.add(template);
        return this;
    }

    public ModuleDsl soyDependency(String soyDependency) {
        data.soyDependencies.add(soyDependency);
        return this;
    }

    public ModuleDsl soyNamespace(String soyNamespace) {
        data.soyNamespace = soyNamespace;
        return this;
    }
}