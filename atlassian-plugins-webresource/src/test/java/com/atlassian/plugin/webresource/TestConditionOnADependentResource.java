package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.integration.TestCase;
import org.junit.Test;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.junit.Assert.assertThat;

public class TestConditionOnADependentResource extends TestCase {

    @Test
    public void testConditionOnADependentResource(){
        wr.configure()
                .disableContextBatching()
                .disableWebResourceBatching()
                    .plugin("plugin")
                        .webResource("a")
                            .condition(AlwaysFalseCondition.class)
                            .resource("a1.js")
                        .webResource("b")
                            .dependency("plugin:a")
                            .resource("b1.js")
                .end();

        wr.requireResource("plugin:b");

        assertThat(wr.paths(), matches(
                resourceUrl("plugin:b", "b1.js")
        ));
    }
}
