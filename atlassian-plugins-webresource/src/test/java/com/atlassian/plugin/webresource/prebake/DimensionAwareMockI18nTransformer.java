package com.atlassian.plugin.webresource.prebake;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;

/**
 */
public class DimensionAwareMockI18nTransformer implements DimensionAwareWebResourceTransformerFactory {
    @Override
    public DimensionAwareTransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return new I18nTransformerUrlBuilder();
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters) {
        return new I18nUrlReadingWebResourceTransformer();
    }

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty().andExactly("locale", "en", "fr");
    }

    public static class I18nTransformerUrlBuilder implements DimensionAwareTransformerUrlBuilder {
        @Override
        public void addToUrl(UrlBuilder urlBuilder, Coordinate coord) {
            coord.copyTo(urlBuilder, "locale");
        }

        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    public static class I18nUrlReadingWebResourceTransformer implements UrlReadingWebResourceTransformer {
        @Override
        public DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
            return MockI18nTransformer.transformImpl(transformableResource, params);
        }
    }
}
