package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestPluginUrlResourceParams {
    @Test
    public void testJs() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put(Config.CONDITIONAL_COMMENT_PARAM_NAME, "ie lt 10");
        paramsMap.put(Config.IEONLY_PARAM_NAME, "true");
        paramsMap.put("charset", "UTF-16");
        paramsMap.put("title", "4shizzle");

        PluginJsResourceParams params = new DefaultPluginJsResourceParams(paramsMap, "com.atlassian.wrm:test-resource", BatchType.RESOURCE);

        assertEquals(ImmutableMap.builder()
                .put("charset", "UTF-16")
                .put("title", "4shizzle")
                .put(Config.WRM_KEY_PARAM_NAME, "com.atlassian.wrm:test-resource")
                .put(Config.WRM_BATCH_TYPE_PARAM_NAME, "resource")
                .build(), params.other());
        assertEquals(ImmutableMap.builder()
                .put(Config.CONDITIONAL_COMMENT_PARAM_NAME, "ie lt 10")
                .put(Config.IEONLY_PARAM_NAME, "true")
                .put("charset", "UTF-16")
                .put("title", "4shizzle")
                .put(Config.WRM_KEY_PARAM_NAME, "com.atlassian.wrm:test-resource")
                .put(Config.WRM_BATCH_TYPE_PARAM_NAME, "resource")
                .build(), params.all());
    }

    @Test
    public void testCss() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put(Config.CONDITIONAL_COMMENT_PARAM_NAME, "ie lt 10");
        paramsMap.put(Config.IEONLY_PARAM_NAME, "true");
        paramsMap.put("media", "print");
        paramsMap.put("charset", "UTF-16");
        paramsMap.put("title", "4shizzle");

        PluginCssResourceParams params = new DefaultPluginCssResourceParams(paramsMap, "com.atlassian.wrm:test-resource", BatchType.RESOURCE);

        assertEquals("print", params.media());
        assertEquals(ImmutableMap.builder()
                .put("charset", "UTF-16")
                .put("title", "4shizzle")
                .put(Config.WRM_KEY_PARAM_NAME, "com.atlassian.wrm:test-resource")
                .put(Config.WRM_BATCH_TYPE_PARAM_NAME, "resource")
                .build(), params.other());
        assertEquals(ImmutableMap.builder()
                .put(Config.CONDITIONAL_COMMENT_PARAM_NAME, "ie lt 10")
                .put(Config.IEONLY_PARAM_NAME, "true")
                .put("charset", "UTF-16")
                .put("media", "print")
                .put("title", "4shizzle")
                .put(Config.WRM_KEY_PARAM_NAME, "com.atlassian.wrm:test-resource")
                .put(Config.WRM_BATCH_TYPE_PARAM_NAME, "resource")
                .build(), params.all());
    }
}
