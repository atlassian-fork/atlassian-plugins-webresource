package com.atlassian.plugin.webresource.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.data.PluginDataResource;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import static com.atlassian.plugin.webresource.TestUtils.removeTrailingSpaces;
import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class DataTestFixture {
    public static final String EXPECTED_PRE_DATA = "<script>\nwindow.WRM=window.WRM||{};" +
            "window.WRM._unparsedData=window.WRM._unparsedData||{};" +
            "window.WRM._unparsedErrors=window.WRM._unparsedErrors||{};\n";
    public static final String EXPECTED_POST_DATA = "if(window.WRM._dataArrived)window.WRM._dataArrived();</script>\n";

    public static void assertDataTag(String key, final String json, String expectedMarkup) throws IOException {
        Iterable<PluginDataResource> data = singletonList(new DefaultPluginDataResource(key, new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException {
                writer.write(json);
            }
        }));
        assertDataTag(data, EXPECTED_PRE_DATA + expectedMarkup + "\n" + EXPECTED_POST_DATA);
    }

    public static void assertDataTag(Iterable<PluginDataResource> data, String expectedMarkup) throws IOException {
        StringWriter writer = new StringWriter();
        new DataTagWriter().write(writer, data);
        assertEquals(expectedMarkup, writer.toString());
    }

    public static void assertDataTag(WebResourceSet resources, String expectedMarkup) {
        StringWriter writer = new StringWriter();
        resources.writeHtmlTags(writer, UrlMode.AUTO);
        assertEquals(removeTrailingSpaces(EXPECTED_PRE_DATA + expectedMarkup + EXPECTED_POST_DATA), removeWebResourceLogs(writer.toString()));
    }

    public static void assertEmptyDataTag(WebResourceSet resources) {
        StringWriter writer = new StringWriter();
        resources.writeHtmlTags(writer, UrlMode.AUTO);
        assertEquals("", removeWebResourceLogs(writer.toString()));
    }
}
