package com.atlassian.plugin.webresource.cdn.mapper;

import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class TestMappingParser {
    MappingParser p = new MappingParser();

    @Test
    public void testEmptyMapping() throws IOException, MappingParserException {
        MappingSet m = p.parse(new StringReader("{}"));
        assertThat(m.size(), is(0));
    }

    @Test
    public void testSimpleMapping() throws IOException, MappingParserException {
        MappingSet m = p.parse(new StringReader("{\"k1\" : [\"v1\"]}"));
        assertThat(m.getMappedResources("k1").get(0), is("v1"));
    }

    @Test(expected = MappingParserException.class)
    public void testInvalidMappingSyntax() throws IOException, MappingParserException {
        p.parse(new StringReader("{\"k1\" : v1}"));
    }

    @Test(expected = MappingParserException.class)
    public void testInvalidArraySyntax() throws IOException, MappingParserException {
        p.parse(new StringReader("{\"k1\" : [\"v1\"}]"));
    }

    @Test(expected = MappingParserException.class)
    public void testEmptyInput() throws IOException, MappingParserException {
        p.parse(new StringReader(""));
    }

    @Test(expected = MappingParserException.class)
    public void testInvalidRootJsonFormat() throws IOException, MappingParserException {
        p.parse(new StringReader("\"x\" : []"));
    }

    @Test(expected = MappingParserException.class)
    public void testInvalidValueJsonFormat() throws IOException, MappingParserException {
        p.parse(new StringReader("{\"k1\" : \"v1\"}"));
    }

    @Test
    public void testMultipleMappings() throws IOException, MappingParserException {
        MappingSet m = p.parse(new StringReader("{\"k1\" : [\"v1\"], \"k2\" : [\"v2a\", \"v2b\"]}"));
        assertThat(m.getMappedResources("k1"), hasItems("v1"));
        assertThat(m.getMappedResources("k1"), hasSize(1));
        assertThat(m.getMappedResources("k2"), hasItems("v2a", "v2b"));
        assertThat(m.getMappedResources("k2"), hasSize(2));
    }

    @Test
    public void testWriting() throws IOException {
        MappingSet m = new DefaultMappingSet(asList(
                new DefaultMapping("/res1.js", Stream.of("/123.js", null)),
                new DefaultMapping("/res2.js", Stream.of("/456.js", "789.js"))
        ));
        String str = p.getAsString(m);
        assertThat(str.replaceAll("\\s", ""), is("{\"/res1.js\":[\"/123.js\"],\"/res2.js\":[\"/456.js\",\"789.js\"]}"));
    }

    @Test
    public void testWritingNothing() throws IOException {
        MappingSet m = new DefaultMappingSet(emptyList());
        String str = p.getAsString(m);
        assertThat(str.replaceAll("\\s", ""), is("{}"));
    }

    @Test
    public void testWriteRead() throws MappingParserException, IOException {
        MappingSet m = new DefaultMappingSet(asList(
                new DefaultMapping("/res1.js", Stream.of("/123.js")),
                new DefaultMapping("/res2.js", Stream.of("/456.js", "789.js"))
        ));
        MappingSet m2 = p.parse(new StringReader(p.getAsString(m)));
        assertThat(m2.getMappedResources("/res1.js"), contains("/123.js"));
        assertThat(m2.getMappedResources("/res2.js"), contains("/456.js", "789.js"));
    }
}
