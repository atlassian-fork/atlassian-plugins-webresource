package com.atlassian.plugin.webresource.integration.stub.dsl;

import com.atlassian.plugin.webresource.integration.stub.WebResource;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class ModulesDsl {
    private boolean submitted = false;
    private final WebResource.PluginDsl pluginDsl;
    protected final ModulesData data;

    public ModulesDsl(WebResource.PluginDsl pluginDsl, String dir) {
        this.pluginDsl = pluginDsl;
        data = new ModulesData(dir);
    }

    public ModuleDsl module(String name) {
        return module(name, "content of module " + name);
    }

    public ModuleDsl module(String name, String content) {
        return new ModuleDsl(this, data.dir, name, content);
    }

    public ModulesDsl context(String moduleName, String context) {
        if (!data.contexts.containsKey(moduleName)) {
            data.contexts.put(moduleName, new ArrayList<>());
        }
        data.contexts.get(moduleName).add(context);
        return this;
    }

    public ModulesDsl condition(Class klass) {
        this.data.conditions.add(klass);
        return this;
    }

    private void submit() {
        if (submitted) {
            return;
        }
        submitted = true;
        beforeSubmit();
        pluginDsl.data.modules.add(this.data);
    }

    private void beforeSubmit() {
        for (ModuleData moduleData : data.modules) {
            List<String> contexts = data.contexts.get(moduleData.name);
            if (contexts == null) {
                contexts = data.contexts.get(moduleData.baseName);
            }
            if (contexts != null) {
                moduleData.contexts = contexts;
            }
        }
    }

    public void end() {
        submit();
        pluginDsl.end();
    }

    public ModulesDsl transformation(String extension, String... transformerKeys) {
        if (!data.transformations.containsKey(extension)) {
            data.transformations.put(extension, new ArrayList<>());
        }
        List<String> transformers = data.transformations.get(extension);
        transformers.addAll(asList(transformerKeys));
        return this;
    }

    public WebResource.PluginDsl plugin(String key) {
        submit();
        return pluginDsl.plugin(key);
    }

    public WebResource.WebResourceDsl webResource(String key) {
        return pluginDsl.webResource(key);
    }

    public ModulesDsl modules(String filePath) {
        submit();
        return pluginDsl.modules(filePath);
    }
}