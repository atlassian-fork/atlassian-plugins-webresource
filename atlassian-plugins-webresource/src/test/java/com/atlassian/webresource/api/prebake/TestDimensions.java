package com.atlassian.webresource.api.prebake;

import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 */
public class TestDimensions {

    @Test
    public void testFromEmptyMap() {
        Dimensions dims = Dimensions.fromMap(emptyMap());
        assertThat(dims, equalTo(Dimensions.empty()));
    }

    @Test
    public void testFromMap() {
        String dim1Key = "nps-not-opted-out";
        String dim2Key = "relative-url";

        Map<String, List<String>> dimMap = new HashMap<>();
        dimMap.put(dim1Key, asList("true", null));
        dimMap.put(dim2Key, asList("true", null));

        Dimensions test = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andAbsent(dim2Key);

        Dimensions converted = Dimensions.fromMap(dimMap);
        assertThat(converted, equalTo(test));
    }

    @Test
    public void testWhitelistValues() {
        String dim1Key = "nps-not-opted-out";
        String dim2Key = "relative-url";
        String dim3Key = "locale";
        String dim4Key = "jag";
        String dim5Key = "jira_GET_7";

        Dimensions orig = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK", "de-DE", "fr-FR")
                .andAbsent(dim3Key)
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);

        Dimensions whitelist = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK")
                .andExactly(dim5Key, "true");

        Dimensions expected = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK")
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);

        Dimensions filtered = orig.whitelistValues(whitelist);
        assertThat(filtered, equalTo(expected));
    }

    @Test
    public void testWhitelistValuesWithEmptyDimensions() {
        String dim1Key = "nps-not-opted-out";
        String dim2Key = "relative-url";
        String dim3Key = "locale";
        String dim4Key = "jag";

        Dimensions orig = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK", "de-DE", "fr-FR")
                .andAbsent(dim3Key)
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);

        Dimensions filtered = orig.whitelistValues(Dimensions.empty());
        assertThat(filtered, equalTo(orig));
    }

    @Test
    public void testCartesianProductSize() {
        Dimensions dim = Dimensions.empty();
        assertThat(dim.cartesianProductSize(), equalTo(0L));

        String dim1Key = "nps-not-opted-out";
        String dim2Key = "relative-url";
        String dim3Key = "locale";
        String dim4Key = "jag";

        dim = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK", "de-DE", "fr-FR")
                .andAbsent(dim3Key)
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);
        assertThat(dim.cartesianProductSize(), equalTo(40L));
    }

    @Test
    public void testBlacklistValues() {
        String dim1Key = "nps-not-opted-out";
        String dim2Key = "relative-url";
        String dim3Key = "locale";
        String dim4Key = "jag";
        String dim5Key = "jira_GET_7";

        Dimensions orig = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK", "de-DE", "fr-FR")
                .andAbsent(dim3Key)
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);

        Dimensions blacklist = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK")
                .andExactly(dim5Key, "true");

        Dimensions expected = Dimensions.empty()
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andExactly(dim3Key, "de-DE", "fr-FR")
                .andAbsent(dim3Key)
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);

        Dimensions filtered = orig.blacklistValues(blacklist);
        assertThat(filtered, equalTo(expected));
    }

    @Test
    public void testBlacklistValuesWithEmptyDimensions() {
        String dim1Key = "nps-not-opted-out";
        String dim2Key = "relative-url";
        String dim3Key = "locale";
        String dim4Key = "jag";

        Dimensions orig = Dimensions.empty()
                .andExactly(dim1Key, "true")
                .andAbsent(dim1Key)
                .andExactly(dim2Key, "true")
                .andAbsent(dim2Key)
                .andExactly(dim3Key, "en-US", "en-UK", "de-DE", "fr-FR")
                .andAbsent(dim3Key)
                .andExactly(dim4Key, "true")
                .andAbsent(dim4Key);

        Dimensions filtered = orig.blacklistValues(Dimensions.empty());
        assertThat(filtered, equalTo(orig));
    }

    @Test
    public void testSimpleCartesianProduct() {
        Dimensions dims = Dimensions.empty()
                .andExactly("bar", "true").andAbsent("bar")
                .andExactly("foo", "on", "off");

        Set<Coordinate> coordinates = dims.cartesianProduct().collect(Collectors.toSet());

        Set<Coordinate> expectedCoords = new LinkedHashSet<>();
        expectedCoords.add(coords(param("foo", "on")));
        expectedCoords.add(coords(param("foo", "on"), param("bar", "true")));
        expectedCoords.add(coords(param("foo", "off")));
        expectedCoords.add(coords(param("foo", "off"), param("bar", "true")));

        assertEquals(expectedCoords, coordinates);
    }

    @Test
    public void testCartesianProductWithTwoAbsent() {
        Dimensions dims = Dimensions.empty()
                .andExactly("bar", "true").andAbsent("bar")
                .andExactly("foo", "true").andAbsent("foo");

        Set<Coordinate> coordinates = dims.cartesianProduct().collect(Collectors.toSet());

        Set<Coordinate> expectedCoords = new LinkedHashSet<>();
        expectedCoords.add(coords());
        expectedCoords.add(coords(param("foo", "true"), param("bar", "true")));
        expectedCoords.add(coords(param("foo", "true")));
        expectedCoords.add(coords(param("bar", "true")));

        assertEquals(expectedCoords, coordinates);
    }

    @Test
    public void testCartesianProductWithTwoAbsentViaProduct() {
        Dimensions a = Dimensions.empty().andExactly("bar", "true").andAbsent("bar");
        Dimensions b = Dimensions.empty().andExactly("foo", "true").andAbsent("foo");
        Dimensions dims = a.product(b);

        Set<Coordinate> coordinates = dims.cartesianProduct().collect(Collectors.toSet());

        Set<Coordinate> expectedCoords = new LinkedHashSet<>();
        expectedCoords.add(coords());
        expectedCoords.add(coords(param("foo", "true"), param("bar", "true")));
        expectedCoords.add(coords(param("foo", "true")));
        expectedCoords.add(coords(param("bar", "true")));

        assertEquals(expectedCoords, coordinates);
    }

    @SafeVarargs
    private static Coordinate coords(QueryParam... params) {
        return new CoordinateImpl(asList(params));
    }

    private static QueryParam param(String key, String val) {
        return new QueryParam(key, Optional.of(val));
    }
}
