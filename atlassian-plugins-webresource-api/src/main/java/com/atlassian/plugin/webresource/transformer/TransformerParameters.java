package com.atlassian.plugin.webresource.transformer;

/**
 * Transformer parameters to be passed to
 * {@link com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory}'s methods
 *
 * @since v3.0.5
 */
public class TransformerParameters {
    private final String pluginKey;
    private final String moduleKey;
    private final String amdModuleLocation;

    public TransformerParameters(String pluginKey, String moduleKey, String amdModuleLocation) {
        this.pluginKey = pluginKey;
        this.moduleKey = moduleKey;
        this.amdModuleLocation = amdModuleLocation;
    }

    /**
     * @return the key of the plugin containing resources to be transformed
     */
    public String getPluginKey() {
        return pluginKey;
    }

    /**
     * @return the key of the web-resource module containing resources to be transformed
     */
    public String getModuleKey() {
        return moduleKey;
    }

    /**
     * If it's an AMD module instead of web-resource.
     *
     * @since 3.5.9
     */
    public boolean isAmdModule() {
        return amdModuleLocation != null;
    }

    /**
     * If it's a parameters of AMD module (see `isAmdModule`) then it will return AMD module location,
     * otherwise returns null.
     *
     * @since 3.5.9
     */
    public String getAmdModuleLocation() {
        return amdModuleLocation;
    }
}
