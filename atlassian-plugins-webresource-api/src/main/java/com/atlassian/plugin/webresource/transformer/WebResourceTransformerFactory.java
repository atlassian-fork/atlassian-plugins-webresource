package com.atlassian.plugin.webresource.transformer;

/**
 * Factory to create URL aware web resource transformers
 *
 * @since v3.0
 */
public interface WebResourceTransformerFactory {
    /**
     * Return the URL builder for this transform
     *
     * @param parameters transformer parameters
     * @return an builder that contributes parameters to the URL
     */
    TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters);

    /**
     * Return the transformer for this transform
     *
     * @param parameters transformer parameters
     * @return a transformer that reads values from the url and transforms a webresource
     */
    UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters);
}
