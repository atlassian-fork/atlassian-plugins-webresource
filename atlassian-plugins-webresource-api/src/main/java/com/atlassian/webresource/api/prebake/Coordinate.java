package com.atlassian.webresource.api.prebake;

import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 * A Coordinate represents a single possible query-string combination.
 * <p>
 * Implementors of this interface will define equals/hashCode.
 * </p>
 */
public interface Coordinate {
    /**
     * Copies the given query key/value to the url builder, if this Coordinate contains that key.
     */
    void copyTo(UrlBuilder urlBuilder, String key);

    /**
     * The value for a given query param, or null if that query param is not present.
     */
    String get(String key);

    /**
     * Get list of all keys.
     *
     * @since 3.5.27
     */
    Iterable<String> getKeys();
}