package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

/**
 * Builder for constructing WebResourceAssembler
 *
 * @since v3.0
 */
@ExperimentalApi
public interface WebResourceAssemblerBuilder {
    /**
     * In addition to any resources explicitly added to the WebResourceAssembler, always implicitly include
     * resources from the superbatch.
     * When disabled, superbatch resources are not implicitly included, but may be individually (and explicitly) included
     * by requiring them with the WebResourceAssembler.
     */
    WebResourceAssemblerBuilder includeSuperbatchResources(boolean include);

    /**
     * Asynchronous data can be required with {@link RequiredData#requireData(String, CompletionStage)}. Such
     * promises must complete within a deadline, otherwise they will considered as if they completed exceptionally
     * and any value they may have returned will be discarded.
     * The timer for the deadline starts when {@link #build()} is called.
     */
    WebResourceAssemblerBuilder asyncDataDeadline(long deadline, TimeUnit timeunit);

    /**
     * Constructs a WebResourceAssembler
     *
     * @return a WebResourceAssembler
     */
    WebResourceAssembler build();

}
