package com.atlassian.webresource.api.assembler.resource;

/**
 * A marking interface that identifies a warning condition generated while prebaking resources.
 *
 * <p> Implementations of this interface are commonly used to taint a resource URL if the associated resource is not
 * safe for prebaking, i.e., if the resource is manipulated by transformations or conditions that are not
 * dimension-aware.</p>
 *
 * <p> Resources tainted with {@link PrebakeWarning}s can be prebaked safely, but might result in degraded
 * performance under certain conditions.</p>
 *
 * <p> Example: JIRA CSS files can be prebaked even if their Less files make use of instance-dependent Look and Feel
 * properties. These files can be prebaked safely, but will result in CT-CDN misses whenever the instance has custom
 * Look and Feel properties set.</p>
 *
 * @since v3.5.23
 */
public interface PrebakeWarning extends PrebakeError { }
