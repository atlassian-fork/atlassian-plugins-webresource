package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.json.marshal.Jsonable;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

/**
 * Interface for clients to require data blobs.
 *
 * @since v3.0
 */
@ExperimentalApi
public interface RequiredData {
    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     *
     * @param key     data key
     * @param content JSON data
     * @return this, to support method chaining
     * {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, Jsonable content);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     *
     * @param key     data key
     * @param content content to render
     * @return this, to support method chaining
     * {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, Number content);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     *
     * @param key     data key
     * @param content content to render
     * @return this, to support method chaining
     * {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, String content);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     *
     * @param key     data key
     * @param content content to render
     * @return this, to support method chaining
     * {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, Boolean content);

    /**
     * Specifies that JSON data with the given key should be avaialbe to the client-side webpage at some point
     * in the future. This may be at a subsequent call to drainIncludedResources() or pollIncludedResources().
     * <p>
     * A CompletionStage may complete normally or exceptionally. Both situations will be communicated to the client-side.
     * <p>
     * The Web Resource mechanism will not keep track of the promise for an indefinite period of time,
     * see {@link WebResourceAssemblerBuilder#asyncDataDeadline(long, TimeUnit)}. A promise that is not complete
     * by the deadline will be returned to client-side as if it had completed exceptionally.
     *
     * @param key     data key
     * @param promise promise content to render
     *                {@code WRM.data()} JavaScript API
     */
    RequiredData requireData(String key, CompletionStage<Jsonable> promise);

}
