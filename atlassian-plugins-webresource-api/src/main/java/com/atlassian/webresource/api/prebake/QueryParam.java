package com.atlassian.webresource.api.prebake;

import java.util.Optional;

/**
 * A single query-param of a co-ordinate. If queryValue is empty then it reprensents the absense of queryKey
 */
class QueryParam {
    final String queryKey;
    final Optional<String> queryValue;

    public QueryParam(String queryKey, Optional<String> queryValue) {
        this.queryKey = queryKey;
        this.queryValue = queryValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueryParam that = (QueryParam) o;

        if (!queryKey.equals(that.queryKey)) return false;
        return queryValue.equals(that.queryValue);

    }

    @Override
    public int hashCode() {
        int result = queryKey.hashCode();
        result = 31 * result + queryValue.hashCode();
        return result;
    }
}
