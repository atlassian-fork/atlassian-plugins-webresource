package com.atlassian.webresource.api.prebake;

import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 * A condition that supports prebaking.
 *
 * @see com.atlassian.webresource.api.prebake
 */
public interface DimensionAwareUrlReadingCondition extends UrlReadingCondition {
    Dimensions computeDimensions();

    void addToUrl(UrlBuilder urlBuilder, Coordinate coord);
}
