package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;

import java.util.List;

/**
 * Representation of a resource accessible via a URL
 *
 * @since v3.0
 */
@ExperimentalApi
public interface PluginUrlResource<T extends PluginUrlResourceParams> extends WebResource {

    /**
     * The resource's url.
     *
     * @param urlMode url mode, either absolute or relative
     * @return the url to this resource, prefixed by a static resource. If caching has been disabled for this resource,
     * the raw url without the static prefix is returned.
     */
    String getStaticUrl(UrlMode urlMode);

    /**
     * Returns params that go outside this url's querystring. Examples are conditional comments, ie only, media queries
     * and attributes that may be added to an HTML tag
     *
     * @return non-querystring params
     */
    T getParams();

    boolean isTainted();

    /**
     * If this url was generated as part of a pre-bake, this indicates any errors that occured.
     */
    List<PrebakeError> getPrebakeErrors();

    /**
     * @return the WRM key(s) contained in this resource (e.g. "_super")
     * @since 3.5.12
     */
    String getKey();

    /**
     * @return the type of resources contained in this batch (either "context" or "resource")
     * @since 3.5.12
     */
    BatchType getBatchType();

    enum BatchType {
        CONTEXT, RESOURCE
    }
}
