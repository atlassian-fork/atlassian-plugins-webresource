package com.atlassian.webresource.api.prebake;

import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 * A transformer that supports prebaking.
 *
 * @see com.atlassian.webresource.api.prebake
 */
public interface DimensionAwareTransformerUrlBuilder extends TransformerUrlBuilder {
    void addToUrl(UrlBuilder urlBuilder, Coordinate coord);
}
