package com.atlassian.webresource.api.prebake;

import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;

/**
 * A transformer that supports prebaking.
 *
 * @see com.atlassian.webresource.api.prebake
 */
public interface DimensionAwareWebResourceTransformerFactory extends WebResourceTransformerFactory {
    Dimensions computeDimensions();

    @Override
    DimensionAwareTransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters);
}
