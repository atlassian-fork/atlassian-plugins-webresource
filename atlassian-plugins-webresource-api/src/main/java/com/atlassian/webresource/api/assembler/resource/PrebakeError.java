package com.atlassian.webresource.api.assembler.resource;

/**
 * A marking interface that identifies an error generated while prebaking resources.
 *
 * <p> Implementations of this interface are commonly used to taint a resource URL if the associated resource is not
 * safe for prebaking, i.e., if the resource is manipulated by transformations or conditions that are not
 * dimension-aware.
 *
 * <p> Resources tainted with {@link PrebakeError}s will never be considered for prebaking.
 *
 * @since v3.5.9
 */
public interface PrebakeError {
}
