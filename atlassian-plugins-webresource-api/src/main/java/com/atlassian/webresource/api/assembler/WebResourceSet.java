package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.webresource.api.UrlMode;
import com.google.common.base.Predicate;

import java.io.Writer;
import java.util.concurrent.CompletionStage;

/**
 * Set of assembled web resources and web resources contexts.
 *
 * @since v3.0
 */
@ExperimentalApi
public interface WebResourceSet {
    /**
     * Write the HTML tags for this object's resources to the writer. Writing will be done in order of
     * webResourceFormatters so that all CSS resources will be output before Javascript.
     *
     * @param writer  writer to output to
     * @param urlMode url formatting mode
     */
    public void writeHtmlTags(Writer writer, UrlMode urlMode);

    /**
     * Write the HTML tags for this object's resources to the writer. Writing will be done in order of
     * webResourceFormatters so that all CSS resources will be output before Javascript.
     *
     * @param writer    writer to output to
     * @param urlMode   url formatting mode
     * @param predicate only resources matching this predicate will be written
     */
    public void writeHtmlTags(Writer writer, UrlMode urlMode, Predicate<WebResource> predicate);

    /**
     * Write the HTML tags for non-data resources as prefetch links: {@code <link rel="prefetch">}.
     * <p>
     * <b>Data resources will not be output.</b>
     * Writing will be done in the same order as the resources appear in this set, not ordered by type.
     * <p>
     * Typical usage:
     * <code>
     * WebResourceAssembler pageAssembler = wr.getPageBuilderService().assembler();
     * WebResourceAssembler prefetchAssembler = pageAssembler.copy();
     *
     * pageAssembler.resources().requireWebResource("myplugin:resource-for-this-page");
     * WebResourceSet pageSet = pageAssembler.assembled().drainIncludedResources();
     * pageSet.writeHtmlTags(writer, urlMode);
     *
     * prefetchAssembler.resources().requireWebResource("myplugin:resource-for-next-page");
     * WebResourceSet prefetchSet = prefetchAssembler.assembled().drainIncludedResources();
     * prefetchSet.writePrefetchLinks(writer, urlMode);
     * </code>
     *
     * @param writer  writer to output to
     * @param urlMode url formatting mode
     * @since 3.6.0
     */
    void writePrefetchLinks(Writer writer, UrlMode urlMode);

    /**
     * Returns a list of included WebResources
     *
     * @return included resources
     */
    public Iterable<WebResource> getResources();

    /**
     * Returns a filtered list of included WebResources matching the given class
     *
     * @param clazz class of webresource to return
     * @return included resources
     */
    public <T extends WebResource> Iterable<T> getResources(Class<T> clazz);

    /**
     * If promises of data have been required via {@link RequiredData#requireData(String, CompletionStage)}
     * then this {@code WebResourceSet} will only contain the completed promises. This method
     * indicates if there are no promises left.
     *
     * @return true if there is nothing more to drain.
     */
    boolean isComplete();
}
