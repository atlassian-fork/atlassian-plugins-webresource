# Host application upgrade notes

With 3.6.0, the WRM no longer depends upon [the AUI plugin][aui]. This change to the dependency chain
has a few consequences to application consumers.

## Translations

To make translations with text substitutions work in the UI at runtime, the WRM needs a `format` function.
Previously, the AUI plugin's `AJS.format` function was used. With 3.6.0, a new `WRM.format` function
is provided.

Previously, the `I18nJsTransformer` would substitute calls similar to `someObject.I18n.getText()`
in to `someObject.format(...)`. With 3.6.0, it writes out exactly `WRM.format(...)` -- the intermediary
namespace is never invoked.

Add the `com.atlassian.plugins.atlassian-plugins-webresource-plugin:format` web-resource to the superbatch.
It must be added before any other web-resources that make use of the `I18nJsTransformer`.

## Slimmed down web-resource dependencies

With 3.6.0, the WRM consumes its upstream dependencies directly and does not require the AUI plugin
as an intermediary. If you were previously only including `com.atlassian.auiplugin:*` 
web-resources in your superbatch to make WRM behaviours work, you can now remove them.

There is one caveat: if you are using a jQuery version higher than 1.8.3, you must ensure that
the [`jQuery.browser`][jqbrowser] property is implemented. This property was deprecated in jQuery 1.9.0 and removed
in subsequent versions. Immediately after you include jquery in your application, you can either:

* Include [the `jquery-migrate` plugin][jqmigrate] (recommended), or 
* Implement the [`jQuery.browser`][jqbrowser] behaviour yourself (not advised).

[jqmigrate]: https://github.com/jquery/jquery-migrate
[jqbrowser]: https://api.jquery.com/jquery.browser/
[aui]: https://aui.atlassian.com/
