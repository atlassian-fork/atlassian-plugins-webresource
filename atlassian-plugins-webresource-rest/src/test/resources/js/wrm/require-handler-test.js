WRM.curl([
    "wrm/jquery",
    "wrm/_",
    "wrm/logger",
    "wrm/require-handler"
], function(
    $,
    _,
    logger,
    RequireHandler
) {
    function createResource(data) {
        var resource = {
            key: "",
            batchType: ""
        };
        _.each(['url', 'resourceType', 'batchType', 'ieOnly', 'conditionalComment', 'media'], function(prop) {
            if (!_.isUndefined(data[prop]) && !_.isNull(data[prop])) {
                resource[prop] = data[prop];
            }
        });
        return resource;
    }

    function createJs(url) {
        return createResource({
            url: url,
            resourceType: "JAVASCRIPT"
        });
    }

    function createCss(url, media) {
        return createResource({
            url: url,
            resourceType: "CSS",
            media: media
        });
    }

    module('WRM RequireHandler', {
        setup: function() {
            this.log = sinon.spy(logger, 'log');
            this.server = sinon.fakeServer.create();
            this.fakeAjax = { restore: function() {} };
        },
        teardown: function() {
            this.log.restore();
            this.server.restore();
            this.fakeAjax.restore();
            this.wrmMock && this.wrmMock.restore();
            this.findTestLinks().remove();

            WRM.data.claim.restore && WRM.data.claim.restore();
            WRM.curl.restore && WRM.curl.restore();
        },
        findTestLinks: function() {
            return $("head link[href^=test-]");
        },
        stubJqueryAjax: function() {
            var originalAjax = $.ajax;
            // mock out jQuery ajax so it works more like it did in jQuery 1.4.
            // thanks, atlassian-gadgets, for your monkeypatch-as-API. :(
            this.fakeAjax = sinon.stub($, "ajax", function(options) {
                var params = $.extend({}, options);
                params.success = function(/*data, status, xhr*/) {
                    if (typeof options.success === 'function') {
                        options.success.apply(this, arguments);
                    }
                };
                params.error = function(/*xhr*/) {
                    if (typeof options.error === 'function') {
                        options.error.apply(this, arguments);
                    }
                };
                originalAjax(params);
                // we return undefined because jQuery 1.4 only had callbacks.
            });
        },
        stubRequireXHR: function(resources, data, condition, errors) {
            var that = this;
            data = data || {};
            errors = errors || {};
            condition = condition || function () { return true; };

            // Setup a collection of XHR handlers, and hook it up to the fake server.
            if (!this.requireXHRHandlers) {
                this.requireXHRHandlers = [];
                this.server.respondWith("POST", /.*webResources.*/, function (xhr) {
                    that.requireXHRHandlers.some(function (handler) {
                        handler(xhr);
                        // break if the handler responded to the XHR
                        return xhr.status == undefined;
                    });
                });
            }

            this.requireXHRHandlers.push(function (xhr) {
                if (condition(xhr)) {
                    xhr.respond(200, { "Content-Type": "application/json" }, JSON.stringify({
                        resources: resources,
                        unparsedData: data,
                        unparsedErrors: errors
                    }));
                    return true;
                }
            });
        },
        expectCurlInclude: function() {
            if (!this.wrmMock) {
                this.wrmMock = sinon.mock(WRM);
            }
            return this.curlMock = this.wrmMock.expects("curl");
        },
        invokeCurlHander: function() {
            var handler = this.curlMock.lastCall.args[1];
            handler && handler();
        },
    });

    test("Base js", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().once().withArgs(["js!blah.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("Base css", function() {
        this.stubRequireXHR([createCss("blah.css")]);
        this.expectCurlInclude().once().withArgs(["css!blah.css"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("JS + CSS", function() {
        this.stubRequireXHR([createJs("blah.js"), createCss("blah.css"),
            createJs("blah1.js"), createCss("blah1.css")]);
        this.expectCurlInclude().once().withArgs(["js!blah.js!order", "css!blah.css", "js!blah1.js!order", "css!blah1.css"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("Passed callback is executed", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().once().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x", expectRequireSuccess);
        this.server.respond();

        this.curlMock.verify();
        expectRequireSuccess.verify();
    });

    test("Deferred callback is executed", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().once().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x").done(expectRequireSuccess);
        this.server.respond();

        this.curlMock.verify();
        expectRequireSuccess.verify();
    });

    test("Handles empty requests", function() {
        this.stubRequireXHR([]);
        this.expectCurlInclude().never();
        var expectRequireSuccess = sinon.mock().thrice().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("", expectRequireSuccess);
        requireHandler.require([], expectRequireSuccess);
        requireHandler.require(false, expectRequireSuccess);

        this.curlMock.verify();
        expectRequireSuccess.verify();
    });

    test("Caches resource requests for multiple invocations", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().twice().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x").done(expectRequireSuccess);
        this.server.respond();
        requireHandler.require("x").done(expectRequireSuccess);

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireSuccess.verify();
    });

    test("Caches resource requests for multiple invocations before first request completes", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().twice().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x").done(expectRequireSuccess);
        requireHandler.require("x").done(expectRequireSuccess);
        this.server.respond();

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireSuccess.verify();
    });

    test("Caches resource requests for multiple invocations (array version)", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().twice().withExactArgs(window, window, window);
        var requireHandler = new RequireHandler();

        requireHandler.require(["x", "y", "z"]).done(expectRequireSuccess);
        this.server.respond();
        requireHandler.require(["x", "y", "z"]).done(expectRequireSuccess);

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireSuccess.verify();
    });

    test("Does not do resource request cache collisions between unrelated objects", function() {
        this.stubRequireXHR([createJs("blah1.js")], null, function (xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "xxx";
        });
        this.stubRequireXHR([createJs("blah2.js")], null, function (xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "yyy";
        });
        this.expectCurlInclude().twice().callsArg(1);
        var expectRequireSuccess1 = sinon.mock().once().withExactArgs(window);
        var expectRequireSuccess2 = sinon.mock().once().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("wr!xxx").done(expectRequireSuccess1);
        requireHandler.require("wr!yyy").done(expectRequireSuccess2);
        this.server.respond();
        this.server.respond();

        this.curlMock.verify();
        equal(this.server.requests.length, 2);
        expectRequireSuccess1.verify();
        expectRequireSuccess2.verify();
    });

    test("Fires failure callback if request fails", function() {
        var expectRequireFailure = sinon.mock().once(); //fails because required resource doesn't exist
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();

        expectRequireFailure.verify();
    });

    test("Fires failure callback if request fails for request waiting after failed previous request", function() {
        var expectRequireFailure = sinon.mock().twice(); //fails because required resources doesn't exist
        var requireHandler = new RequireHandler();

        requireHandler.require("y").fail(expectRequireFailure);
        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();
        this.server.respond();

        expectRequireFailure.verify();
    });

    test("Fires failure callback if request fails for request waiting after successful previous request", function() {
        this.stubRequireXHR([createJs("blah1.js")], null, function (xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "xxx";
        });
        this.expectCurlInclude().once().callsArg(1);

        var expectRequireSuccess = sinon.mock().once().withExactArgs(window);
        var expectRequireFailure = sinon.mock().once(); //fails because required resource doesn't exist

        var requireHandler = new RequireHandler();

        requireHandler.require("wr!xxx").done(expectRequireSuccess);
        requireHandler.require("x").fail(expectRequireFailure);

        this.server.respond();
        expectRequireSuccess.verify();

        this.server.respond();
        expectRequireFailure.verify();

        this.curlMock.verify();
        equal(this.server.requests.length, 2);
    });

    test("Fires success callback if request was successful for request waiting after failed previous request", function() {
        this.stubRequireXHR([createJs("blah1.js")], null, function (xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "xxx";
        });
        this.expectCurlInclude().once().callsArg(1);
        var expectRequireSuccess = sinon.mock().once().withExactArgs(window);
        var expectRequireFailure = sinon.mock().once(); //fails because required resource doesn't exist
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        requireHandler.require("wr!xxx").done(expectRequireSuccess);

        this.server.respond();
        expectRequireFailure.verify();

        this.server.respond();
        expectRequireSuccess.verify();

        this.curlMock.verify();
        equal(this.server.requests.length, 2);
    });

    test("Cache of resource request for multiple invocations is removed after request failed", function() {
        var expectRequireFailure = sinon.mock().thrice(); //fails because required resources doesn't exist
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();
        this.server.respond();

        // Request was cached before server responded with error
        equal(this.server.requests.length, 1);

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();

        // Request was removed from cache on fail, so should be requested second time
        equal(this.server.requests.length, 2);
    });

    test("Failure callback is removed from cache if initial request fails", function() {
        var expectRequireFailure = sinon.mock().thrice(); //fails because required resources doesn't exist
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();
        this.server.respond();

        // Request was cached before server responded with error
        equal(this.server.requests.length, 1);

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();

        // Request was removed from cache on fail, so should be requested second time
        equal(this.server.requests.length, 2);
    });

    test("Fires failure callback if curljs request fails", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArgWith(2, "arg1", "arg2");
        var expectRequireFailure = sinon.mock().once().withExactArgs("arg1", "arg2");
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();

        this.curlMock.verify();
        expectRequireFailure.verify();
    });

    test("Failure callback is cached if curljs request fails", function() {
        this.stubRequireXHR([createJs("blah.js")]);
        this.expectCurlInclude().callsArgWith(2, "arg1", "arg2");
        var expectRequireFailure = sinon.mock().twice().withExactArgs("arg1", "arg2");
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();
        requireHandler.require("x").fail(expectRequireFailure);

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireFailure.verify();
    });

    test("CSS with media queries are not added via curl.js", function() {
        this.stubRequireXHR([createCss("test-blah.css", "print")]);
        this.expectCurlInclude().once().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();

        var c = this.log.callCount;
        this.invokeCurlHander();
        equal(this.log.callCount, c + 1);

        var resourceMediaQuery = null;
        $("head link").each(function() {
            if ($(this).attr("href") === "test-blah.css") {
                resourceMediaQuery = $(this).attr("media");
            }
        });
        equal(resourceMediaQuery, "print");
    });

    test("CSS with media queries are not added via curl.js", function() {
        this.stubRequireXHR([createCss("test-blah.css", "print")]);
        this.expectCurlInclude().once().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();

        var c = this.log.callCount;
        this.invokeCurlHander();
        equal(this.log.callCount, c + 1);

        var links = this.findTestLinks();
        equal(links.length, 1);
        equal(links.attr("href"), "test-blah.css");
        equal(links.attr("media"), "print");
    });

    test("CSS with media queries are discovered on subsequent calls", function() {
        this.stubRequireXHR([createCss("test-blah.css", "print")]);
        this.expectCurlInclude().twice().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();
        this.invokeCurlHander();
        requireHandler.require("y");
        this.server.respond();
        this.invokeCurlHander();

        this.curlMock.verify();

        var links = this.findTestLinks();
        equal(links.length, 1);
        equal(links.attr("href"), "test-blah.css");
        equal(links.attr("media"), "print");
    });

    test("CSS media query check respects media='all'", function() {
        this.stubRequireXHR([createCss("test-blah.css", "all")]);
        this.expectCurlInclude().once().withArgs(["css!test-blah.css"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
        equal(this.findTestLinks().length, 0);
    });

    test("JS that has provided data", function() {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        sinon.stub(WRM.data, 'claim', function(key) { return JSON.parse(WRM._unparsedData[key]) });

        this.stubRequireXHR([createJs("blah.js")], {"com.foo.x": JSON.stringify({x:"y"})});
        this.expectCurlInclude().once().withArgs(["js!blah.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
        var claim = WRM.data.claim("com.foo.x");
        equal(claim.x, "y");
    });

    test("JS that has provided errors", function() {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        sinon.stub(WRM.data, 'claim', function(key) { return JSON.parse(WRM._unparsedData[key]) });

        this.stubRequireXHR(
            [createJs("blah.js")],
            {},
            null,
            {"com.foo.x": JSON.stringify("")}
        );
        this.expectCurlInclude().once().withArgs(["js!blah.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();

        ok(WRM._unparsedErrors.hasOwnProperty("com.foo.x"));
    });

    // AG-1504, PLUGWEB-408
    test("Requiring resources works with hackish jQuery 1.4 version of jQuery.ajax function", function () {
        this.stubJqueryAjax();
        this.stubRequireXHR([createJs("blah1.js")], null, function(xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "x";
        });
        this.expectCurlInclude().callsArg(1);

        var expectRequireSuccess = sinon.mock().once();
        var expectRequireFailure = sinon.mock().once();
        var requireHandler = new RequireHandler();
        requireHandler.require("wr!x").done(expectRequireSuccess);
        this.server.respond();
        expectRequireSuccess.verify();

        requireHandler.require("wr!y").fail(expectRequireFailure);
        this.server.respond();
        expectRequireFailure.verify();
    });

    // PLUGWEB-437
    test("Require calls are queued while one is in flight and consolidated to reduce network utilisation", function () {
        // It doesn't matter what the server tells the browser to load;
        // all that matters is the number of requests the client makes to find out.
        this.stubRequireXHR([]);

        var requireHandler = new RequireHandler();

        requireHandler.require('1st');
        equal(this.server.requests.length, 1);

        requireHandler.require('2nd');
        requireHandler.require(['3rd', '4th']);

        equal(this.server.requests.length, 1, 'there should still be just one request made at this point because the first has not yet finished');

        // Resolve the first request, which should cause a second request to be queued
        this.server.respond();
        equal(this.server.requests.length, 2, 'the subsequent require calls should be compacted in to a single request');

        var firstRequestBody = JSON.parse(this.server.requests[0].requestBody);
        var secondRequestBody = JSON.parse(this.server.requests[1].requestBody);
        deepEqual(firstRequestBody.r, ['1st'], 'first request should be for a single resource');
        deepEqual(secondRequestBody.r, ['2nd', '3rd', '4th'], 'second request should be for remaining resources');
    });

    // PLUGWEB-437
    test("Nested require calls are queued and consolidated to reduce network utilisation", function () {
        // It doesn't matter what the server tells the browser to load;
        // all that matters is the number of requests the client makes to find out.
        this.stubRequireXHR([]);

        var requireHandler = new RequireHandler();

        requireHandler.require('main', function() {
            requireHandler.require('1st', function () {
                requireHandler.require('deep1');
            });
            requireHandler.require('2nd');
        });
        requireHandler.require('secondary', function() {
            requireHandler.require(['3rd', '4th'], function() {
                requireHandler.require('deep2');
            });
        });

        // Respond an unusual number of times.
        // todo: see PLUGWEB-438 for details; this test's expectations will need to change when that issue is addressed.
        this.server.respond();
        this.server.respond();
        this.server.respond();
        this.server.respond();

        var firstRequestBody = JSON.parse(this.server.requests[0].requestBody);
        var secondRequestBody = JSON.parse(this.server.requests[1].requestBody);
        var thirdRequestBody = JSON.parse(this.server.requests[2].requestBody);
        var fourthRequestBody = JSON.parse(this.server.requests[3].requestBody);
        deepEqual(firstRequestBody.r, ['main'], 'first request should be for a single resource');
        deepEqual(secondRequestBody.r, ['secondary', '1st', '2nd'], 'second request is for remaining sync resources plus calls discovered after first response');
        deepEqual(thirdRequestBody.r, ['3rd', '4th'], 'third request is for calls discovered after second response');
        deepEqual(fourthRequestBody.r, ['deep1', 'deep2'], 'should make a request for the deepest stacks');
    });

    // PLUGWEB-437
    test("Order for resolving require callbacks when resources loaded should align with require call order", function () {
        // It doesn't matter what the server tells the browser to load;
        // in this test we're only concerned with resolution order.
        this.stubRequireXHR([]);

        var step = sinon.spy();
        var verifySteps = function(correctOrder) {
            var actualOrder = [].concat(step.args).map(function(arg) { return arg[0]; });
            deepEqual(actualOrder, correctOrder, 'steps should have occurred in the correct order');
        };

        var requireHandler = new RequireHandler();

        // Make our first require which, when resolved, will make a nested require call.
        step('before requiring 1st');
        requireHandler.require(['1st']).done(function() {
            step('1st callback');

            // Make a nested require, which should only occur once the first response from the server comes back.
            step('before requiring bar');
            requireHandler.require('bar', function() {
                step('bar callback');
            });
            step('after requiring bar');
        });

        // Make a second and third require call in serial.
        step('before requiring 2nd');
        requireHandler.require(['2nd'], function() {
            step('2nd callback');
        });

        step('before requiring 3rd');
        requireHandler.require(['3rd'], function() {
            step('3rd callback');
        });

        step('after all top-level require calls');

        // Resolve the first request, which should cause a second request to be queued
        this.server.respond();
        // Resolve the second request, which should cause the remaining queued require
        // callbacks to resolve as well.
        this.server.respond();
        verifySteps([
            'before requiring 1st',
            'before requiring 2nd',
            'before requiring 3rd',
            'after all top-level require calls',
            '1st callback',
            'before requiring bar',
            'after requiring bar',
            '2nd callback',
            '3rd callback',
            'bar callback',
        ]);
    });

    // PLUGWEB-445
    // NOTE: This test is checking for the root cause of a problem, but not actually testing the behaviours a consumer would see.
    //       To do that, we would need a test that showed the callback for require call #3 executes before call #2.
    test("Multiple require calls waiting on a consolidated request do not result in multiple curl.js calls", function () {
        this.stubRequireXHR([]);
        var curlSpy = sinon.spy(WRM, 'curl');
        var requireHandler = new RequireHandler();

        // Start lining up some require calls.
        // Make the first require call. While the server has not responded, this blocks other requests.
        requireHandler.require(['blocking-request']);

        // Make the second and third require calls. These calls will get enqueued,
        // thus will get consolidated in to a single AJAX call.
        requireHandler.require(['a', 'b']);
        requireHandler.require(['c', 'd']);

        this.server.respond(); // Respond to the blocking request.
        this.server.respond(); // Respond to the second consolidated request.

        equal(curlSpy.callCount, 2, 'CURL should only have been invoked twice');
    });

    test("[deprecated behaviour] resources with any conditionalComment value are omitted", function() {
        var resources = [
            createResource({ resourceType: "JAVASCRIPT", url: "any-ie.js", conditionalComment: "IE" }),
            createResource({ resourceType: "JAVASCRIPT", url: "any.js" }),
            createResource({ resourceType: "JAVASCRIPT", url: "only-ie9.js", conditionalComment: "ie 9"}),
            createResource({ resourceType: "JAVASCRIPT", url: "after-ie8.js", conditionalComment: "ie gt 8"}),
            createResource({ resourceType: "JAVASCRIPT", url: "bad-config.js", conditionalComment: "something" })
        ];
        this.stubRequireXHR(resources);
        this.expectCurlInclude().once().withArgs(["js!any.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("[deprecated behaviour] resources with any ieOnly value are omitted", function() {
        var resources = [
            createResource({ resourceType: "JAVASCRIPT", url: "any-ie.js", ieOnly: true }),
            createResource({ resourceType: "JAVASCRIPT", url: "any.js" }),
            createResource({ resourceType: "JAVASCRIPT", url: "bad-ieonly.js", ieOnly: false }),
        ];
        this.stubRequireXHR(resources);
        this.expectCurlInclude().once().withArgs(["js!any.js!order", "js!bad-ieonly.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });
});
