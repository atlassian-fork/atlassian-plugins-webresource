// Assembling AMD modules for tests.
({
    optimize: "none",
    baseUrl: "../../../main/resources/js/modules",
    include: [
        "wrm",
        "wrm/require"
    ],
    paths: {
        wr: "../../../../../../atlassian-plugins-webresource-plugin/src/main/resources/js/web-resources/wr-loader",
        css: "../../../../../../atlassian-plugins-webresource-plugin/src/main/resources/js/web-resources/css-loader",
        less: "../../../../../../atlassian-plugins-webresource-plugin/src/main/resources/js/web-resources/less-loader",
        soy: "../../../../../../atlassian-plugins-webresource-plugin/src/main/resources/js/web-resources/soy-loader"
    },
    out: "../../../../target/modules-test-build.js"
})