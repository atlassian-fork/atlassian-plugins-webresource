package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestDiscoveryTask {

    private static final String JS_CONTENT_TYPE = "application/javascript";
    private static final String CSS_CONTENT_TYPE = "text/css";
    private static final String OCTET_STREAM_CONTENT_TYPE = "application/octet-stream";

    @Test
    public void testCollection() throws Exception {
        StubBundler bundler = new StubBundler();
        StubCollectorCrawler collectorCrawler = new StubCollectorCrawler(createDefaultResourceMap());

        DiscoveryTask discoveryTask = new DiscoveryTask(
                false,
                true,
                "",
                "",
                singletonList(collectorCrawler),
                collectorCrawler,
                bundler
        );

        discoveryTask.doRun();
        assertThat(bundler.getMappedCount(), equalTo(20));
        assertThat(bundler.getTaintedCount(), equalTo(0));

        // Checking that the resource content is the same as the resource URL
        for (Resource r : collectorCrawler.getResources()) {
            String url = r.getUrl();
            ResourceContent content = bundler.getResourceContent(url);
            assertThat(new String(content.getContent()), equalTo(url));
        }
    }

    @Test
    public void testTaintedResources() throws Exception {
        List<URI> taintedURIs = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            taintedURIs.add(new URI("taintedResource" + i + ".js"));
        }

        StubBundler bundler = new StubBundler();
        StubCollectorCrawler collectorCrawler = new StubCollectorCrawler(
                createDefaultResourceMap(),
                taintedURIs
        );

        DiscoveryTask discoveryTask = new DiscoveryTask(
                false,
                true,
                "",
                "",
                singletonList(collectorCrawler),
                collectorCrawler,
                bundler
        );

        discoveryTask.doRun();
        assertThat(bundler.getMappedCount(), equalTo(20));
        assertThat(bundler.getTaintedCount(), equalTo(10));
    }

    @Test
    public void testCSSProcessing() throws Exception {
        String cssUrl = "style.css";
        String css = ".relative { background:url(\"loading.gif\") center no-repeat;}" +
                ".relativeWithSlash { background:url(\"/image.svg\") center no-repeat;}" +
                ".relativeSingleQuotes { background:url('/image.svg') center no-repeat;}" +
                ".relativeNoQuotes { background:url(/image.svg) center no-repeat;}" +
                ".base64 { background:url(\"data:image/png;base64,iVBORw0KGgoAAAANSU\") center no-repeat;}" +
                ".absolute { background:url(\"http://test.com/loading.gif\") center no-repeat;}" +
                ".absoluteHttps { background:url(\"https://test.com/loading.gif\") center no-repeat;}";

        // We expect only the relative image to be
        String expectedProcessedCSS = ".relative { background:url(/b616fe17ec95b05bc355ee06177b7f3167d93cbf.gif) center no-repeat;}" +
                ".relativeWithSlash { background:url(/d9c59acf99d3917e9e3ef1d9cecf5875386217cc.svg) center no-repeat;}" +
                ".relativeSingleQuotes { background:url(/d9c59acf99d3917e9e3ef1d9cecf5875386217cc.svg) center no-repeat;}" +
                ".relativeNoQuotes { background:url(/d9c59acf99d3917e9e3ef1d9cecf5875386217cc.svg) center no-repeat;}" +
                ".base64 { background:url(\"data:image/png;base64,iVBORw0KGgoAAAANSU\") center no-repeat;}" +
                ".absolute { background:url(\"http://test.com/loading.gif\") center no-repeat;}" +
                ".absoluteHttps { background:url(\"https://test.com/loading.gif\") center no-repeat;}";

        Map<URI, ResourceContent> resources = new HashMap<>();
        URI uri = new URI(cssUrl);
        ResourceContent content = new ResourceContent(uri, CSS_CONTENT_TYPE, css.getBytes(UTF_8));
        resources.put(uri, content);
        uri = new URI("loading.gif");
        content = new ResourceContent(uri, OCTET_STREAM_CONTENT_TYPE, uri.toString().getBytes(UTF_8));
        resources.put(uri, content);
        uri = new URI("/image.svg");
        content = new ResourceContent(uri, OCTET_STREAM_CONTENT_TYPE, uri.toString().getBytes(UTF_8));
        resources.put(uri, content);

        StubBundler bundler = new StubBundler();
        StubCollectorCrawler collectorCrawler = new StubCollectorCrawler(resources);

        DiscoveryTask discoveryTask = new DiscoveryTask(
                false,
                true,
                "",
                "",
                singletonList(collectorCrawler),
                collectorCrawler,
                bundler
        );

        discoveryTask.doRun();
        assertThat(bundler.getMappedCount(), equalTo(3));
        assertThat(bundler.getTaintedCount(), equalTo(0));

        ResourceContent processedCSS = bundler.getResourceContent(cssUrl);
        assertThat(new String(processedCSS.getContent()), equalTo(expectedProcessedCSS));
    }

    @Test
    public void testCSSPrebakingDisabled() throws Exception {
        StubBundler bundler = new StubBundler();
        StubCollectorCrawler collectorCrawler = new StubCollectorCrawler(createDefaultResourceMap());

        DiscoveryTask discoveryTask = new DiscoveryTask(
                false,
                false,
                "",
                "",
                singletonList(collectorCrawler),
                collectorCrawler,
                bundler
        );

        discoveryTask.doRun();
        // We only expect 10 resources to be bundled, since CSS is ignored during this test
        assertThat(bundler.getMappedCount(), equalTo(10));
        assertThat(bundler.getTaintedCount(), equalTo(0));
    }

    /**
     * Creates a resource map with 10 JavaScript resources and 10 CSS resources.
     * The content of each resource corresponds to its URI
     */
    private Map<URI, ResourceContent> createDefaultResourceMap() throws URISyntaxException {
        int resourceCount = 10;
        Map<URI, ResourceContent> resources = new HashMap<>();
        for (int i = 0; i < resourceCount; i++) {
            String uriJS = "resource" + i + ".js";
            byte[] bytes = uriJS.getBytes(UTF_8);
            ResourceContent content = new ResourceContent(new URI(uriJS), JS_CONTENT_TYPE, bytes);
            resources.put(new URI(uriJS), content);

            String uriCSS = "resource" + i + ".css";
            bytes = uriCSS.getBytes(UTF_8);
            content = new ResourceContent(new URI(uriCSS), CSS_CONTENT_TYPE, bytes);
            resources.put(new URI(uriCSS), content);
        }

        return resources;
    }

}
