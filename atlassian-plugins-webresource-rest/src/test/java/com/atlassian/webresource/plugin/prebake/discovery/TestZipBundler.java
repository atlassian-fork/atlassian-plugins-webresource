package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.cdn.mapper.Mapping;
import com.atlassian.plugin.webresource.cdn.mapper.MappingParser;
import com.atlassian.plugin.webresource.cdn.mapper.MappingSet;
import com.atlassian.plugin.webresource.impl.PrebakeErrorFactory;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.plugin.prebake.resources.GenericUrlResource;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;
import com.atlassian.webresource.plugin.prebake.util.PreBakeUtil;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestZipBundler {

    private static final String GLOBAL_STATE_HASH = "global-state-hash";
    private static final String JS_CONTENT_TYPE = "application/javascript";

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void testResourceManagement() throws Exception {
        Path outputFolder = tempFolder.newFolder().toPath();
        ZipBundler bundler = new ZipBundler(GLOBAL_STATE_HASH, outputFolder);

        assertThat(bundler.getGlobalStateHash(), equalTo(GLOBAL_STATE_HASH));
        assertThat(bundler.getMappedCount(), equalTo(0));
        assertThat(bundler.getTaintedCount(), equalTo(0));

        Path bundleZipPath = bundler.getBundleZipPath();
        assertThat(bundleZipPath, equalTo(outputFolder.resolve("bundle.zip")));

        // Adding new resource
        String url = "test1.js";
        Resource resource = new GenericUrlResource(url);
        byte[] bytes = url.getBytes(UTF_8);
        String computedHash = PreBakeUtil.hash(bytes);
        ResourceContent content = new ResourceContent(new URI(url), JS_CONTENT_TYPE, bytes);
        Optional<String> hash = bundler.addResource(url, resource, content);
        assertTrue(hash.isPresent());
        assertThat(hash.get(), equalTo("/" + computedHash + ".js"));
        assertThat(bundler.getMappedCount(), equalTo(1));
        assertThat(bundler.getTaintedCount(), equalTo(0));
        assertTrue(bundler.isMapped(url));
        assertFalse(bundler.isTainted(url));

        // Adding the same resource another time
        Optional<String> hashRepeat = bundler.addResource(url, resource, content);
        assertThat(bundler.getMappedCount(), equalTo(1));
        assertThat(hashRepeat, equalTo(hash));

        // Adding new tainted resource
        url = "test2.js";
        PrebakeError error = PrebakeErrorFactory.from("Error processing resource " + url);
        resource = new GenericUrlResource(url, singletonList(error));
        bundler.addTaintedResource(url, resource);
        assertThat(bundler.getMappedCount(), equalTo(1));
        assertThat(bundler.getTaintedCount(), equalTo(1));
        assertFalse(bundler.isMapped(url));
        assertTrue(bundler.isTainted(url));

        // Adding the same tainted resource another time
        bundler.addTaintedResource(url, resource);
        assertThat(bundler.getTaintedCount(), equalTo(1));
    }

    @Test
    public void testBundleCreation() throws Exception {
        int resourceCount = 10;
        Path outputFolder = tempFolder.newFolder().toPath();
        ZipBundler bundler = new ZipBundler(GLOBAL_STATE_HASH, outputFolder);

        // Adding new resources
        for (int i = 0; i < resourceCount; i++) {
            String url = "resource" + i + ".js";
            addResource(url, bundler);
        }

        // Adding new tainted resources
        for (int i = 0; i < resourceCount; i++) {
            String url = "taintedResource" + i + ".js";
            addTaintedResource(url, bundler);
        }

        assertThat(bundler.getMappedCount(), equalTo(resourceCount));
        assertThat(bundler.getTaintedCount(), equalTo(resourceCount));

        // Check file creation
        bundler.createBundleZip();
        File bundleFile = bundler.getBundleZipPath().toFile();
        assertTrue(bundleFile.exists());

        // Check zip contents
        ZipFile bundleZipFile = new ZipFile(bundleFile);
        Enumeration<? extends ZipEntry> entries = bundleZipFile.entries();
        Map<String, ZipEntry> files = new HashMap<>();
        while (entries.hasMoreElements()) {
            ZipEntry e = entries.nextElement();
            files.put(e.getName(), e);
        }

        // Check global state hash
        ZipEntry stateTxt = files.get("/bundle/state.txt");
        assertNotNull(stateTxt);
        byte[] content = IOUtils.toByteArray(bundleZipFile.getInputStream(stateTxt));
        String readGlobalStateHash = new String(content);
        assertThat(readGlobalStateHash, equalTo(GLOBAL_STATE_HASH));

        // Check report
        ZipEntry reportTxt = files.get("/bundle/report.txt");
        assertNotNull(reportTxt);
        content = IOUtils.toByteArray(bundleZipFile.getInputStream(reportTxt));
        String report = new String(content);
        assertTrue(report.contains("Coverage: 50.00%"));
        assertTrue(report.contains("Baked resources: 10"));
        assertTrue(report.contains("Tainted resources: 10"));
        for (int i = 0; i < resourceCount; i++) {
            assertTrue(report.contains("taintedResource" + i + ".js"));
            assertFalse(report.contains("resource" + i + ".js"));
        }

        // Check mappings and resources
        ZipEntry mappingsJson = files.get("/bundle/mappings.json");
        MappingParser parser = new MappingParser();
        MappingSet mappings = parser.parse(new InputStreamReader(bundleZipFile.getInputStream(mappingsJson)));
        for (Mapping m : mappings.all()) {
            String originalUrl = m.originalResource();
            String hashedFilename = "/bundle/resources"  + m.mappedResources().get(0);
            ZipEntry res = files.get(hashedFilename);
            content = IOUtils.toByteArray(bundleZipFile.getInputStream(res));
            assertThat(new String(content), equalTo(originalUrl));
        }
    }

    /**
     * Adds a new resource whose content is the resource's URL
     */
    private void addResource(String url, Bundler bundler) throws Exception {
        Resource resource = new GenericUrlResource(url);
        byte[] bytes = url.getBytes(UTF_8);
        ResourceContent content = new ResourceContent(new URI(url), JS_CONTENT_TYPE, bytes);
        bundler.addResource(url, resource, content);
    }

    private void addTaintedResource(String url, Bundler bundler) {
        PrebakeError error = PrebakeErrorFactory.from(url);
        Resource resource = new GenericUrlResource(url, singletonList(error));
        bundler.addTaintedResource(url, resource);
    }

}
