package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.impl.PrebakeErrorFactory;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.plugin.prebake.exception.PreBakeIOException;
import com.atlassian.webresource.plugin.prebake.resources.GenericUrlResource;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceCollector;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * Stub {@link WebResourceCrawler} and {@link ResourceCollector} to be used in tests
 */
public class StubCollectorCrawler implements WebResourceCrawler, ResourceCollector {

    private final List<GenericUrlResource> resources;
    private final Map<URI, ResourceContent> contents;
    private final List<String> targets;

    public StubCollectorCrawler(Map<URI, ResourceContent> resources) {
        this(resources, emptyList());
    }

    public StubCollectorCrawler(Map<URI, ResourceContent> resources, List<URI> tainted) {
        List<GenericUrlResource> rs = new ArrayList<>();
        Map<URI, ResourceContent> cs = new HashMap<>();
        List<String> ts = new ArrayList<>();

        // Adding resources
        for (Map.Entry<URI, ResourceContent> e : resources.entrySet()) {
            URI uri = e.getKey();
            rs.add(new GenericUrlResource(uri.toString()));
            cs.put(uri, e.getValue());
            ts.add(uri.toString());
        }

        // Adding tainted resources
        for (URI uri : tainted) {
            if (cs.containsKey(uri)) {
                throw new IllegalArgumentException("URI " + uri + " cannot be both tainted and untainted");
            }
            String uriString = uri.toString();
            PrebakeError pe = PrebakeErrorFactory.from("Error processing resource " + uriString);
            rs.add(new GenericUrlResource(uriString, singletonList(pe)));
        }

        this.resources = Collections.unmodifiableList(rs);
        this.contents = Collections.unmodifiableMap(cs);
        this.targets = Collections.unmodifiableList(ts);
    }

    public List<GenericUrlResource> getResources() {
        return resources;
    }

    ////////////////////
    // Collector methods
    ////////////////////

    @Override
    public ResourceContent collect(URI uri) throws PreBakeIOException {
        return contents.get(uri);
    }

    //////////////////
    // Crawler methods
    //////////////////

    @Override
    public void crawl(BlockingQueue<Resource> queue) throws InterruptedException {
        for (GenericUrlResource r : resources) {
            queue.put(r);
        }
    }

    @Override
    public List<String> getTargets() {
        return targets;
    }

}
