package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;
import com.atlassian.webresource.plugin.prebake.util.PreBakeUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.Arrays.asList;

/**
 * Stub {@link Bundler} implementation to be used in unit tests
 */
public class StubBundler implements Bundler {

    private final Lock lock = new ReentrantLock();
    private final Map<String, String> mappings = new HashMap<>();
    private final Map<String, ResourceContent> contents = new HashMap<>();
    private final Map<String, TaintedResource> tainted = new HashMap<>();

    public ResourceContent getResourceContent(String url) {
        lock.lock();
        try {
            return contents.get(url);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Optional<String> addResource(String url, Resource resource, ResourceContent content) {
        lock.lock();
        try {
            String hash = PreBakeUtil.hash(content.getContent());
            String hashedUrl = "/" + hash + resource.getExtension();
            mappings.put(url, hashedUrl);
            contents.put(url, content);
            return Optional.of(hashedUrl);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void addTaintedResource(String url, Resource resource) {
        lock.lock();
        try {
            TaintedResource tr = new TaintedResource(url, resource.getName(), resource.getPrebakeErrors());
            tainted.put(url, tr);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void addTaintedResource(String url, String name, PrebakeError... errors) {
        lock.lock();
        try {
            TaintedResource tr = new TaintedResource(url, name, asList(errors));
            tainted.put(url, tr);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Optional<String> getMapping(String url) {
        lock.lock();
        try {
            return Optional.ofNullable(mappings.get(url));
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isMapped(String url) {
        lock.lock();
        try {
            return mappings.containsKey(url);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isTainted(String url) {
        lock.lock();
        try {
            return tainted.containsKey(url);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int getMappedCount() {
        lock.lock();
        try {
            return mappings.size();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int getTaintedCount() {
        lock.lock();
        try {
            return tainted.size();
        } finally {
            lock.unlock();
        }
    }

}
