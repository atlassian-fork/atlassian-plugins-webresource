/**
 * Implementation of some underscore utilities; used to avoid bringing in all of underscore.
 */
WRM.define("wrm/_", function() {
    var _ = {
        filter: function(list, fn) {
            var filtered = [];
            for (var i = 0; i < list.length; ++i) {
                var item = list[i];
                if (fn(item)) {
                    filtered.push(item);
                }
            }
            return filtered;
        },
        map: function(list, fn) {
            var map = [];
            for (var i = 0; i < list.length; ++i) {
                map.push(fn(list[i]));
            }
            return map;
        },
        each: function(obj, fn) {
            if (obj.length === +obj.length) {
                for (var i = 0; i < obj.length; ++i) {
                    fn(obj[i]);
                }
            } else {
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        fn(obj[prop], prop);
                    }
                }
            }
        },
        bind: function(fn, scope) {
            return function() {
                fn.apply(scope, Array.prototype.slice.call(arguments));
            }
        },
        isArray: function(maybe) {
            return Object.prototype.toString.call(maybe) == '[object Array]';
        },
        isUndefined: function(maybe) {
            return maybe === void 0;
        },
        isNull: function(maybe) {
            return maybe === null;
        }
    };

    return _;
});