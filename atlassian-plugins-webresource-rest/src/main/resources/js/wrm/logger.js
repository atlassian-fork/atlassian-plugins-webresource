/* eslint-disable no-console */
WRM.define("wrm/logger", function() {
    function wrmLog() {
        if (typeof console !== 'undefined' && console.log) {
            console.log.apply(console, arguments);
        }
    }

    return {
        log: wrmLog
    }
});
