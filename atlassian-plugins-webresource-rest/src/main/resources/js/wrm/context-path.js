// Wrapper for AJS.contextPath to include the instance context path value as a module
WRM.define("wrm/context-path", function() {
    return WRM.contextPath;
});
