package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.config.Config;
import io.atlassian.util.concurrent.ThreadFactories;
import com.atlassian.webresource.plugin.prebake.exception.PreBakeIOException;
import com.atlassian.webresource.plugin.prebake.resources.ResourceCollector;
import com.atlassian.webresource.plugin.prebake.util.ConcurrentUtil;
import com.atlassian.webresource.plugin.prebake.util.StopWatch;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static io.atlassian.util.concurrent.ThreadFactories.Type.DAEMON;
import static com.atlassian.webresource.plugin.prebake.discovery.PreBakeState.CANCELLED;
import static com.atlassian.webresource.plugin.prebake.discovery.PreBakeState.DONE;
import static com.atlassian.webresource.plugin.prebake.discovery.PreBakeState.NOTSTARTED;
import static com.atlassian.webresource.plugin.prebake.discovery.PreBakeState.RUNNING;
import static java.util.Arrays.asList;

/**
 * Pre-bake operation executor.
 *
 * @since v3.5.0
 */
public final class WebResourcePreBaker {

    private static final Logger log = LoggerFactory.getLogger(WebResourcePreBaker.class);

    private final ExecutorService pool;
    private final Lock readState;
    private final Lock writeState;
    private final Config config;
    private final WebResourceIntegration webResourceIntegration;
    private final ResourceCollector resourceCollector;

    private PreBakeState state;
    private ZipBundler bundler;
    private List<String> targets;

    public WebResourcePreBaker(
            Config config,
            WebResourceIntegration webResourceIntegration,
            ResourceCollector resourceCollector) {
        ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
        this.readState = rwl.readLock();
        this.writeState = rwl.writeLock();
        this.pool = Executors.newSingleThreadExecutor(ThreadFactories.named("prebaker").type(DAEMON).build());
        this.state = NOTSTARTED;
        this.config = config;
        this.webResourceIntegration = webResourceIntegration;
        this.resourceCollector = resourceCollector;
    }

    public PreBakeStatus start(WebResourceCrawler... crawlers) {
        return start(asList(crawlers));
    }

    /**
     * Starts web-resource discovery. This function must be called within a thread belonging to a valid
     * HttpServletRequest to ensure that we can correctly determine the context path and base URL of the product.
     *
     * @param crawlers - list of web-resource crawlers.
     */
    public PreBakeStatus start(List<WebResourceCrawler> crawlers) {
        writeState.lock();
        try {
            // Avoids starting multiple discovery tasks at the same time.
            if (state == RUNNING) {
                return getStatus();
            }
            state = RUNNING;
            doStart(crawlers);
            return getStatus();
        } catch (Exception e) {
            state = CANCELLED;
            log.error("An error occurred while starting the prebake process", e);
            return getStatus();
        } finally {
            writeState.unlock();
        }
    }

    private void doStart(List<WebResourceCrawler> crawlers) {
        targets = new ArrayList<>();
        for (WebResourceCrawler wrc : crawlers) {
            targets.addAll(wrc.getTargets());
        }

        bundler = createBundler();
        DiscoveryTask discoveryTask = new DiscoveryTask(
                config.ignorePrebakeWarnings(),
                config.isCSSPrebakingEnabled(),
                webResourceIntegration.getBaseUrl(UrlMode.RELATIVE),
                webResourceIntegration.getBaseUrl(UrlMode.ABSOLUTE),
                crawlers,
                resourceCollector,
                bundler
        );
        pool.execute(() -> {
            try {
                StopWatch stopwatch = new StopWatch();
                discoveryTask.doRun();
                bundler.createBundleZip();
                setState(DONE);
                log.info("Total prebaking time: {}s", stopwatch.getElapsedSecondsAndReset());
            } catch (Exception e) {
                log.error("Cancelling pre-bake due to ", e);
                setState(CANCELLED);
            }
        });
    }

    private ZipBundler createBundler() {
        String globalStateHash = config.computeGlobalStateHash();
        File tmp = webResourceIntegration.getTemporaryDirectory();
        Path outputFolder = Paths.get(tmp.getAbsolutePath(), globalStateHash);

        try {
            FileUtils.deleteDirectory(outputFolder.toFile());
            boolean created = outputFolder.toFile().mkdirs();
            if (!created) {
                String msg = String.format("Cannot create temporary folder '%s'", outputFolder.toString());
                throw new PreBakeIOException(msg);
            }
            return new ZipBundler(globalStateHash, outputFolder);
        } catch (IOException e) {
            String msg = String.format("Cannot clean temporary folder '%s'", outputFolder.toString());
            throw new PreBakeIOException(msg, e);
        }
    }

    /**
     * @return true if state was changed, otherwise false.
     */
    private boolean setState(PreBakeState newState) {
        writeState.lock();
        try {
            if (state == newState) {
                return false;
            } else {
                if (CANCELLED == newState) {
                    ConcurrentUtil.stopExecutor(pool, 200);
                }
                state = newState;
                return true;
            }
        } finally {
            writeState.unlock();
        }
    }

    public PreBakeStatus getStatus() {
        readState.lock();
        try {
            return new PreBakeStatus(
                    state,
                    bundler.getTaintedCount(),
                    bundler.getGlobalStateHash(),
                    bundler.getBundleZipPath(),
                    targets
            );
        } finally {
            readState.unlock();
        }
    }


    /**
     * Simple POJO that encapsulates the state of the prebaking process.
     *
     * @since v3.5.29
     */
    public static final class PreBakeStatus {

        private final PreBakeState state;
        private final int taintedResourceCount;
        private final String globalStateHash;
        private final Path bundlePath;
        private final List<String> targets;

        private PreBakeStatus(
                PreBakeState state,
                int taintedResourceCount,
                String globalStateHash,
                Path bundlePath,
                List<String> targets) {
            this.state = state;
            this.taintedResourceCount = taintedResourceCount;
            this.globalStateHash = globalStateHash;
            this.bundlePath = bundlePath;
            this.targets = Collections.unmodifiableList(new ArrayList<>(targets));
        }

        public PreBakeState getState() {
            return state;
        }

        public int getTaintedResourceCount() {
            return taintedResourceCount;
        }

        public String getGlobalStateHash() {
            return globalStateHash;
        }

        public Path getBundlePath() {
            return bundlePath;
        }

        public List<String> getTargets() {
            return targets;
        }

    }

}
