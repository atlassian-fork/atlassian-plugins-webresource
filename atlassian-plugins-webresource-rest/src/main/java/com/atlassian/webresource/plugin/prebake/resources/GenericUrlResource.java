package com.atlassian.webresource.plugin.prebake.resources;

import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Collections.emptyList;

/**
 * A generic {@link Resource} accessible through a URL
 */
public final class GenericUrlResource extends Resource {

    private static final Logger log = LoggerFactory.getLogger(GenericUrlResource.class);

    private static final Pattern extensionPattern = Pattern.compile(".*(?<extension>\\.[a-zA-Z-0-9]*).*$");

    private final String url;
    private final String extension;
    private final List<PrebakeError> errors;

    public GenericUrlResource(String url) {
        this(url, emptyList());
    }

    public GenericUrlResource(String url, List<PrebakeError> errors) {
        checkArgument(StringUtils.isNotEmpty(url), "Empty URL");
        this.url = url;
        this.errors = errors;

        Matcher m = extensionPattern.matcher(url);
        if (m.matches()) {
            extension = m.group("extension");
        } else {
            log.warn("Resource with URL '{}' does not contain a valid extension", url);
            extension = "";
        }
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isTainted() {
        return !errors.isEmpty();
    }

    @Override
    public List<PrebakeError> getPrebakeErrors() {
        return errors;
    }

    @Override
    public String getName() {
        return url;
    }

    @Override
    public String getExtension() {
        return extension;
    }

}
