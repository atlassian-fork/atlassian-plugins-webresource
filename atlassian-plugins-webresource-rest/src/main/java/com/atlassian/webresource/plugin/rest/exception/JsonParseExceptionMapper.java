package com.atlassian.webresource.plugin.rest.exception;

import com.google.gson.JsonObject;
import org.codehaus.jackson.JsonParseException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonParseExceptionMapper extends Throwable implements ExceptionMapper<JsonParseException> {
    @Override
    public Response toResponse(JsonParseException e) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("errorMessage", e.getMessage());
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(jsonObject.toString())
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
