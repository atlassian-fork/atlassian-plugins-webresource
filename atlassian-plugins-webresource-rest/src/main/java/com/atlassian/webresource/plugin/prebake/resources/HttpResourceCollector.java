package com.atlassian.webresource.plugin.prebake.resources;

import com.atlassian.webresource.plugin.prebake.exception.PreBakeIOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

import static java.lang.String.format;
import static java.lang.String.valueOf;

/**
 * Retrieves resources from the WebResourceManager using an HTTP connection.
 */
public final class HttpResourceCollector implements ResourceCollector {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpResourceCollector.class);
    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    @Override
    public ResourceContent collect(URI uri) throws PreBakeIOException {
        try (CloseableHttpResponse response = performGet(uri)) {
            if (response == null || response.getStatusLine().getStatusCode() != 200 || response.getEntity() == null) {
                String msg = format("Wrong HTTP response for %s: %s", uri, valueOf(response));
                LOGGER.error(msg);
                throw new PreBakeIOException(msg);
            }

            HttpEntity entity = response.getEntity();
            return new ResourceContent(
                    uri,
                    entity.getContentType().getValue(),
                    EntityUtils.toByteArray(entity)
            );
        } catch (IOException e) {
            String msg = format("Problem fetching resource: %s", uri);
            LOGGER.error(msg, e);
            throw new PreBakeIOException(msg, e);
        }
    }

    private CloseableHttpResponse performGet(URI uri) throws IOException {
        return httpClient.execute(
                new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme()),
                new HttpGet(uri)
        );
    }

}
