package com.atlassian.webresource.plugin.prebake.util;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * File handling helpers.
 *
 * @since v3.5.0
 */
public final class FileUtil {

    private static final Logger log = LoggerFactory.getLogger(FileUtil.class);

    public FileUtil() {
        // statics only
    }

    public static void write2File(byte[] content, File file) throws IOException {
        Files.write(file.toPath(), content);
    }

    public static void zipFiles(File directory, Collection<String> sources, File destination) throws IOException {
        try (
                FileOutputStream dest = new FileOutputStream(destination);
                CheckedOutputStream checksum = new CheckedOutputStream(dest, new Adler32());
                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(checksum))
        ) {
            for (String source : sources) {
                log.trace("Adding: {}", source);
                try (
                        FileInputStream fi = new FileInputStream(source);
                        BufferedInputStream origin = new BufferedInputStream(fi)
                ) {
                    ZipEntry entry = new ZipEntry(source.replaceFirst(directory.getPath(), ""));
                    out.putNextEntry(entry);
                    IOUtils.copy(origin, out);
                }
            }
        }
    }

}
