package com.atlassian.webresource.plugin.rest;

import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.prebake.DimensionUnawareOverride;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.atlassian.webresource.plugin.prebake.discovery.PreBakeState;
import com.atlassian.webresource.plugin.prebake.discovery.RootPageCrawler;
import com.atlassian.webresource.plugin.prebake.discovery.SuperBatchCrawler;
import com.atlassian.webresource.plugin.prebake.discovery.WebResourceCrawler;
import com.atlassian.webresource.plugin.prebake.discovery.WebResourcePreBaker;
import com.atlassian.webresource.plugin.prebake.resources.HttpResourceCollector;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.webresource.plugin.prebake.util.PreBakeUtil.BUNDLE_ZIP_FILE;
import static com.atlassian.webresource.plugin.prebake.util.PreBakeUtil.BUNDLE_ZIP_URI_PATH;
import static javax.ws.rs.core.Response.Status;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

import static com.atlassian.plugin.webresource.prebake.DimensionUnawareOverride.DimensionOverride;

/**
 * <p>
 * REST end-point responsible for web-resource pre-bake operation handling.
 * </p>
 * All resources will only be available when a system property named according to {@link Config#PREBAKE_FEATURE_ENABLED}
 * is set "true", otherwise {@link Status#FORBIDDEN} status will be returned.
 *
 * @since v3.5.0
 */
@AnonymousAllowed
@Path("prebake")
public final class PreBakeResource {

    private static final Logger log = LoggerFactory.getLogger(PreBakeResource.class);

    private final WebResourcePreBaker preBaker;
    private final PrebakeWebResourceAssemblerFactory webResourceAssemblerFactory;
    private final Config config;
    private final Globals globals;

    @VisibleForTesting
    @Context
    UriInfo info;

    @SuppressWarnings("deprecation")
    public PreBakeResource(
            final WebResourceIntegration webResourceIntegration,
            final PrebakeWebResourceAssemblerFactory webResourceAssemblerFactory,
            final PluginResourceLocator pluginResourceLocator) {
        this(
                pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt().getConfig(),
                pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt(),
                webResourceIntegration,
                webResourceAssemblerFactory
        );
    }

    @VisibleForTesting
    PreBakeResource(
            final Config config,
            final Globals globals,
            final WebResourceIntegration webResourceIntegration,
            final PrebakeWebResourceAssemblerFactory webResourceAssemblerFactory) {
        this.globals = globals;
        this.config = config;
        this.webResourceAssemblerFactory = webResourceAssemblerFactory;
        this.preBaker = new WebResourcePreBaker(
                config,
                webResourceIntegration,
                new HttpResourceCollector()
        );
    }

    /**
     * Initiates the superbatch pre-baking operation.
     * <p>In order to maintain backwards compatibility with the previous behaviour, this rest endpoint is hardwired to
     * prebake only the superbatch. It will be removed, or merged with the {@code prebakeRootPages} endpoint, once
     * there's at least one root-page declared in the JIRA XML descriptors (see APDEX-895 and APDEX-922)
     * <p>
     * <p> Accessible from rest/webResources/1.0/prebake/state, this REST endpoint accepts a JSON parameter with the
     * following format:
     * <p>
     * <pre>
     *      {
     *          "pagesToPrebake": [ "jira.root.pages:issue-view-page", "jira.root.pages:issues-nav-list"],
     *          "dimensionValueWhitelist": {
     *              "nps-not-opted-out": [ "true" ],
     *              "locale": [ "en-US", "en-UK" ]
     *              "jira.dev.mode": [ null ]
     *          }
     *      }
     * </pre>
     *
     * @param request Command for the prebaker. Contains the whitelist of dimension values that can be used to
     *                restrict the cartesian product of dimensions considered by the prebaker. An empty {@code
     *                root-page} list will result in all known root pages being prebaked. Similarly, an empty
     *                dimension value whitelist will result in all known dimensions to be considered for the
     *                cartesian product.
     * @return Outcome summary of the prebake operation
     *
     * @deprecated since v3.5.30 - use {@link #startRootPagePrebake(PreBakeRequest)} instead
     */
    @Path("/state")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public final Response put(PreBakeRequest request) {
        return whenPreBakeIsEnabled(() -> {
            WebResourceCrawler crawler = new SuperBatchCrawler(
                    webResourceAssemblerFactory,
                    toDimensions(request.dimensionValueWhitelist)
            );
            WebResourcePreBaker.PreBakeStatus status = preBaker.start(crawler);
            return ok(new PreBakeResult(status)).build();
        });
    }

    /**
     * REST endpoint for prebaking resoures declared as root-page dependencies. Prebake behaviour can be customized
     * passing a JSON request with the following format:
     *
     * <p> Accessible from rest/webResources/1.0/prebake/start, this REST endpoint accepts a JSON parameter with the
     * following format:
     *
     * <pre>
     * {
     *     "pagesToPrebake": ["jira.root.pages:issue-view-page", "jira.root.pages:issues-nav-list"],
     *     "dimensionValueWhitelist": {
     *         "nps-not-opted-out": ["true"],
     *         "locale": ["en-US", "en-UK"],
     *         "jira.dev.mode": [null]
     *      },
     *      "dimensionUnawareOverrides": {
     *          "com.atlassian.nps.plugin.NpsAcknowledgedUrlReadingCondition": {
     *              "key": "nps-acknowledged",
     *              "dimensions": ["true", null]
     *          },
     *          "com.atlassian.nps.plugin.NpsNotOptedOutUrlReadingCondition": {
     *              "key": "nps-not-opted-out",
     *              "dimensions": ["true", null]
     *          }
     *      }
     * }
     * </pre>
     *
     * <p> {@code pagesToPrebake} can be used to specify the names of the root-pages to consider while prebaking. An
     * empty list will result in all known root-pages being prebaked.
     *
     * <p> {@code dimensionValueWhitelist} allows to explicitly declare which dimension values are to be used when
     * prebaking. Whitelisted dimension values will be the only values to be considered when prebaking over the
     * dimension to which they are associated. An empty list will result in all known dimension values to be considered
     * in the cartesian product.
     *
     * <p> {@code dimensionUnawareOverrides} can be used to inject dimensionality into dimension-unaware conditions.
     * Each override is composed of the class name of the condition to override, the query string key associated with
     * the dimension, and the possible values that the dimension can assume. This parameter can be used to quickly
     * untaint web-resources that make use of dimension-unaware conditions. Dimension override declared for
     * dimension-aware conditions will simply be ignored. It is important to note that dimension overrides will not
     * work properly if the condition contributes to the resource hash instead of the query string. An empty {@code
     * dimensionUnawareOverrides} will result in no overrides being applied.
     *
     * @param request Prebaking options
     * @return Outcome summary of the prebake operation
     */
    @Path("/start")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public final Response prebakeRootPages(PreBakeRequest request) {
        return whenPreBakeIsEnabled(() -> startRootPagePrebake(request));
    }

    private Response startRootPagePrebake(PreBakeRequest request) {
        ValidatedRootPages validatedRootPages = getValidatedRootPages(request.pagesToPrebake,
                globals.getSnapshot().getAllRootPages());
        if (CollectionUtils.isNotEmpty(validatedRootPages.invalidRootPageNames)) {
            return status(Response.Status.BAD_REQUEST)
                    .entity(new ValidatedRootPagesREST(validatedRootPages))
                    .build();
        }

        setupDimensionUnawareOverrides(request.dimensionUnawareOverrides);

        WebResourceCrawler rootPageCrawler = new RootPageCrawler(
                webResourceAssemblerFactory,
                validatedRootPages.includedRootPages,
                toDimensions(request.dimensionValueWhitelist)
        );

        WebResourcePreBaker.PreBakeStatus status = preBaker.start(rootPageCrawler);
        return ok(new PreBakeResult(status)).build();
    }

    private void setupDimensionUnawareOverrides(Map<String, DimensionOverrideRequest> overrides) {
        if (overrides == null || overrides.isEmpty()) {
            DimensionUnawareOverride.reset();
            return;
        }

        Collection<DimensionOverride> os = new ArrayList<>();
        for (String className : overrides.keySet()) {
            DimensionOverrideRequest dor = overrides.get(className);
            os.add(new DimensionOverride(className, dor.key, dor.dimensions));
        }
        DimensionUnawareOverride.setup(os);
    }

    private Dimensions toDimensions(Map<String, List<String>> rawDimensions) {
        if (rawDimensions == null) {
            return Dimensions.empty();
        } else {
            return Dimensions.fromMap(rawDimensions);
        }
    }

    /**
     * REST endpoint for finding the dimensions that will be pre-baked for each root-page
     *
     * @param request Command to direct which root pages to determine dimensions for. Contains the names of
     *                {@code root-page}s to use, and a whitelist of dimension values that can be used to restrict the
     *                cartesian product of dimensions that would be considered by the prebaker. An empty
     *                {@code root-page} list will result in dimensions for all known root pages being returned.
     *                Similarly, an empty dimension value whitelist will result in all known dimensions to be considered
     *                for the cartesian product.
     * @return Dimensions computed for each root page.
     */
    @Path("/dimensions")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public final Response rootPageDimensions(PreBakeRequest request) {
        return whenPreBakeIsEnabled(() -> {
            ValidatedRootPages validatedRootPages = getValidatedRootPages(request.pagesToPrebake,
                    globals.getSnapshot().getAllRootPages());
            if (CollectionUtils.isNotEmpty(validatedRootPages.invalidRootPageNames)) {
                return status(Response.Status.BAD_REQUEST).entity(new ValidatedRootPagesREST(validatedRootPages)).build();
            } else {
                Map<String, String> rootPageDimensions = new LinkedHashMap<>();
                Dimensions whiteList = toDimensions(request.dimensionValueWhitelist);
                validatedRootPages.includedRootPages.stream().map(RootPage::getWebResource).forEach(wr -> {
                    Dimensions dims = webResourceAssemblerFactory
                            .computeBundleDimensions(wr)
                            .whitelistValues(whiteList);
                    rootPageDimensions.put(wr.getKey(),
                            dims.toString() + " Cartesian product: " + dims.cartesianProduct().count());
                });
                return ok(rootPageDimensions).build();
            }
        });
    }

    /**
     * Shows current state ({@link PreBakeState}) of pre-baking process.
     *
     * @return result of pre-bake operation.
     */
    @Path("/state")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public final Response get() {
        return whenPreBakeIsEnabled(() ->
                ok(new PreBakeResult(preBaker.getStatus())).build());
    }

    /**
     * Provides bundle.zip with product state, pre-baked web-resources and mappings.
     *
     * @return zipped bundle.
     */
    @Path("/" + BUNDLE_ZIP_FILE)
    @GET
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    public final Response bundle() {
        return whenPreBakeIsEnabled(() -> {
            WebResourcePreBaker.PreBakeStatus status = preBaker.getStatus();
            if (PreBakeState.DONE == status.getState()) {
                return
                        ok(status.getBundlePath().toFile())
                                .header("Content-Disposition", "attachment; filename=" + BUNDLE_ZIP_FILE)
                                .build();
            } else {
                log.debug("Bundle is not ready, current state is [{}]", status.getState());
                return status(Status.NOT_FOUND).build();
            }
        });
    }

    /**
     * Prebake request class. It encapsulates the different parameters that can be passed to the execution REST
     * endpoint to configure the prebake operation.
     */
    public static class PreBakeRequest {
        public Map<String, List<String>> dimensionValueWhitelist;
        public Map<String, DimensionOverrideRequest> dimensionUnawareOverrides;
        public List<String> pagesToPrebake;
    }

    public static class DimensionOverrideRequest {
        public String key;
        public List<String> dimensions;
    }

    /**
     * Response class returned by the prebake endpoint. It encapsulates the current state of the prebaking operation.
     */
    public class PreBakeResult {
        public PreBakeState state;
        public String bundle;
        public String productStateHash;
        public Collection<String> targets;
        public int taintedResourcesCount;

        public PreBakeResult() {
            this.state = null;
            this.bundle = null;
            this.productStateHash = null;
            this.targets = null;
            this.taintedResourcesCount = 0;
        }

        public PreBakeResult(WebResourcePreBaker.PreBakeStatus status) {
            state = status.getState();
            bundle = "" + URI.create(info.getBaseUri() + BUNDLE_ZIP_URI_PATH).normalize();
            productStateHash = status.getGlobalStateHash();
            targets = status.getTargets();
            taintedResourcesCount = status.getTaintedResourceCount();
        }
    }

    public static class ValidatedRootPages {
        public final Collection<String> invalidRootPageNames;
        public final Collection<String> validRootPageNames;
        public final Collection<RootPage> includedRootPages;

        public ValidatedRootPages(Iterable<RootPage> rootPages) {
            this.invalidRootPageNames = new ArrayList<>();
            this.validRootPageNames = StreamSupport.stream(rootPages.spliterator(), false)
                    .map(RootPage::getWebResource)
                    .map(WebResource::getKey)
                    .collect(Collectors.toList());
            this.includedRootPages = StreamSupport.stream(rootPages.spliterator(), false)
                    .collect(Collectors.toList());
        }

        public ValidatedRootPages(Collection<String> invalidRootPagesName, Collection<String> validRootPageNames,
                                  Collection<RootPage> includedRootPages) {
            this.invalidRootPageNames = invalidRootPagesName;
            this.validRootPageNames = validRootPageNames;
            this.includedRootPages = includedRootPages;
        }
    }

    public static class ValidatedRootPagesREST {
        public final Collection<String> validRootPageNames;
        public final Collection<String> invalidRootPageNames;

        public ValidatedRootPagesREST(ValidatedRootPages validatedRootPages) {
            this.validRootPageNames = validatedRootPages.validRootPageNames;
            this.invalidRootPageNames = validatedRootPages.invalidRootPageNames;
        }
    }

    private Response whenPreBakeIsEnabled(Supplier<Response> normalResponse) {
        if (config.isPreBakeEnabled()) {
            return normalResponse.get();
        } else {
            log.warn("Pre-baking called but feature is not enabled!");
            return status(Status.FORBIDDEN).build();
        }
    }

    /**
     * Given a list of root page names and an iterable containing all known {@link RootPage}, will return a
     * {@link ValidatedRootPages} that contains valid root pages and their names, along with any names that did not
     * correspond to a known root page.
     * <p>If the list of page names is null or empty then all root pages in the provided iterable rootPages will be
     * contained in the result.
     *
     * @param rootPageNames a list of root page names
     * @param rootPages     all known root pages
     * @return a {@link ValidatedRootPages} which contains a collection of root pages and a
     * collection of root page names that were not valid
     */
    @VisibleForTesting
    static ValidatedRootPages getValidatedRootPages(List<String> rootPageNames, Iterable<RootPage> rootPages) {
        if (CollectionUtils.isEmpty(rootPageNames)) {
            log.debug("Provided list of root pages is empty, will return all known root pages.");
            return new ValidatedRootPages(rootPages);
        }

        Set<RootPage> includedRootPages = new LinkedHashSet<>();
        Set<String> validRootPageNames = new LinkedHashSet<>();

        for (RootPage rp : rootPages) {
            if (rootPageNames.contains(rp.getWebResource().getKey())) {
                includedRootPages.add(rp);
                validRootPageNames.add(rp.getWebResource().getKey());
            }
        }

        Collection<String> invalidRootPageNames = CollectionUtils.<String>disjunction(rootPageNames, validRootPageNames);
        return new ValidatedRootPages(invalidRootPageNames, validRootPageNames, includedRootPages);
    }
}
