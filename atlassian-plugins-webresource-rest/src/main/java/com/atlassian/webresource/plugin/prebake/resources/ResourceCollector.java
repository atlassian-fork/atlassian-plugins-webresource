package com.atlassian.webresource.plugin.prebake.resources;

import com.atlassian.webresource.plugin.prebake.exception.PreBakeIOException;

import java.net.URI;

/**
 * Collects resources from the WebResourceManager.
 *
 * @since 3.5.13
 */
public interface ResourceCollector {

    /**
     * Fetches a resource using its {@link URI}
     *
     * @param uri resource uri
     * @return resource content and corollary information
     */
    ResourceContent collect(URI uri) throws PreBakeIOException;

}
