package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;

import java.util.Optional;

/**
 * An interface for bundling prebaked resources.
 *
 * @since v3.5.29
 */
public interface Bundler {

    /**
     * Adds a new resource to the bundle. If for whatever reason the resource could not be added, the {@link Bundler}
     * will save the resource as tainted and return an empty {@link Optional}.
     *
     * @param url original URL of the resource.
     * @param resource resource to be added.
     * @param content content of the resource.
     * @return hashed CT-CDN URL of the resource.
     */
    Optional<String> addResource(String url, Resource resource, ResourceContent content);

    /**
     * Adds the {@link Resource} to the list of tainted resources. This method is a no-op if the resource is not
     * tainted.
     *
     * @param url original URL of the resource.
     * @param resource tainted resource.
     */
    void addTaintedResource(String url, Resource resource);

    /**
     * Creates and adds a {@link TaintedResource} using the data passed as parameter.
     *
     * @param url original URL of the resource.
     * @param name resource name.
     * @param errors List of {@link PrebakeError}s to store in the {@link TaintedResource}.
     */
    void addTaintedResource(String url, String name, PrebakeError... errors);

    /**
     * Retrieves the hashed filename of a resource. Returns an empty {@link Optional} if the content of the original
     * URL is not in the bundle.
     *
     * @param url original URL of the resource.
     * @return hashed CT-CDN URL of the resource.
     */
    Optional<String> getMapping(String url);

    /**
     * Returns true if the resource identified by the specified URL has a corresponding mapping in the CT-CDN.
     *
     * @param url original URL of the resource.
     * @return true if the resource is mapped, false otherwise.
     */
    boolean isMapped(String url);

    /**
     * Returns true if the resource identified by the URL passed as parameter was added to the bundle as tainted.
     *
     * @param url original URL of the resource.
     * @return true if the resource was added to the bundle as tainted, false otherwise.
     */
    boolean isTainted(String url);

    /**
     * Returns the number of resources added to the bundle. This number indicates how many resources will be mapped
     * in the CT-CDN.
     *
     * @return number of resource mappings added to the bundle.
     */
    int getMappedCount();

    /**
     * Returns the number of tainted resources added to the bundle. This number indicates how many resources could
     * not be included in the CT-CDN due to tainting errors (i.e., dimension-unaware transformers and conditions,
     * missing files, etc).
     *
     * @return number of tainted resoures added to the bundle.
     */
    int getTaintedCount();

}
