package com.atlassian.webresource.plugin.prebake.exception;

/**
 * Occurs when pre-bake operation results in an error
 *
 * @since v3.5.27
 */
public class PreBakeException extends RuntimeException {
    public PreBakeException(String message, Throwable cause) {
        super(message, cause);
    }

    public PreBakeException(String message) {
        super(message);
    }
}
