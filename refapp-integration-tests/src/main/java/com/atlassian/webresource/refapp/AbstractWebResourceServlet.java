package com.atlassian.webresource.refapp;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SanitizedString;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Scanned
public abstract class AbstractWebResourceServlet extends HttpServlet {

    public static final Pattern REQ_TYPE_PATTERN = Pattern.compile("/([^/]*)/?.*");
    protected final Map<String, Consumer<HttpServletRequest>> handlers = new HashMap<>();
    private final Map<String, Object> context = new HashMap<>();

    protected final SoyTemplateRenderer renderer;
    protected final PageBuilderService pageBuilderService;
    public static final String PLUGIN_ID = "com.atlassian.plugins.refapp-integration-tests";

    @Inject
    public AbstractWebResourceServlet(@ComponentImport SoyTemplateRenderer renderer, @ComponentImport PageBuilderService pageBuilderService) {
        this.renderer = renderer;
        this.pageBuilderService = pageBuilderService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String actionPath = getActionPath(req);
        handlers.get(actionPath).accept(req);

        addToContext("htmlTags", new SanitizedString(drainResourcesToString()));

        renderer.render(
                resp.getWriter(),
                PLUGIN_ID + ":page-with-resources",
                "Atlassian.WebResource.ItTests." + actionPath,
                context);
    }

    protected void addToContext(String key, Object value) {
        context.put(key, value);
    }

    protected String drainResourcesToString() {
        StringWriter htmlTagsWriter = new StringWriter();
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(
                htmlTagsWriter, UrlMode.AUTO
        );
        return htmlTagsWriter.toString();
    }

    protected String getActionPath(HttpServletRequest req) {
        return Optional.ofNullable(req.getPathInfo())
                .map(REQ_TYPE_PATTERN::matcher)
                .filter(Matcher::matches)
                .map(matcher -> matcher.group(1))
                .flatMap(pathInfo ->
                        pathInfo.trim().isEmpty() ? Optional.empty() : Optional.of(pathInfo))
                .filter(handlers.keySet()::contains)
                .orElse("index");
    }
}
